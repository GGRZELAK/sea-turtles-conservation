<?php
namespace Framework;

use Controller\Authenticate\FilterController;
use Controller\Authenticate\IndexingController;
use Controller\Authenticate\LogController;
use Model\AuthenticateModel;
use Service\ServiceURL;

class Controller
{

    public $ici;

    public $ou;

    public function __construct()
    {
        $app = new App();

        $indexing = new IndexingController();
        $filter = new FilterController();
        $log = new LogController();

        $authenticateModel = new AuthenticateModel();

        // URL en cours
        $this->ici = $app->protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        // Emplacement de redirection
        if (! empty($_GET['redirect'])) {
            $this->ou = strip_tags($_GET['redirect']);
        } else {
            $this->ou = $app->protocol . $app->dns . '/';
        }

        // Déconnexion
        if (! empty($_SESSION)) {

            if (isset($_POST['logout']) || isset($_GET['logout']) || empty($authenticateModel->getSession($_SESSION["userId"]))) {

                $authenticateModel->deleteAutologin($_COOKIE["device"]);
                $authenticateModel->deleteSession(session_id());
                unset($_SESSION);
                session_destroy();
                if ($this->ou) {
                    header("Location: " . $this->ou);
                } else {
                    $serviceUrl = new ServiceURL();
                    header("Location: " . $app->protocol . $_SERVER["HTTP_HOST"] . $serviceUrl->changeParam("logout"));
                }
            }
        }

        // Indexation
        $agentId = $indexing->agent();
        $externalIpId = $indexing->ip();
        $deviceId = $indexing->device();
        $pageId = $indexing->page();

        // Logs
        $log->deviceAgent($deviceId, $agentId);
        $log->ipDevice($deviceId, $externalIpId);
        $log->ipAgent($agentId, $externalIpId);

        if (! empty($_SESSION["userId"])) {

            $log->userAgent($agentId);
            $log->userDevice($deviceId);
            $log->userIp($externalIpId);
        }

        $log->history($pageId, $deviceId, $agentId, $externalIpId);

        // Filtrage
        $filter->blacklist($pageId, $deviceId, $agentId, $externalIpId);

        // Autologin
        if (empty($_SESSION["userId"])) {

            $autoLogin = $authenticateModel->getAutologin($deviceId);

            if (! empty($autoLogin["autologin"])) {

                if (! $autoLogin["forbidden"]) {

                    $picture = null;
                    if (! empty($autoLogin["picture"])) {
                        $picture = $autoLogin["picture"];
                    } elseif (file_exists($app->getDocumentRoot() . $app->asset . $app->image . "users/" . $autoLogin["autologin"] . ".jpg")) {
                        $picture = '/' . $app->asset . $app->image . "users/" . $autoLogin["autologin"] . ".jpg";
                    }

                    $_SESSION["userId"] = $autoLogin["autologin"];
                    $_SESSION["firstName"] = $autoLogin["firstname"];
                    $_SESSION["lastName"] = $autoLogin["lastname"];
                    $_SESSION["email"] = $autoLogin["email"];
                    $_SESSION["birthdate"] = $autoLogin["birthdate"];
                    $_SESSION["registration"] = $autoLogin["registration"];
                    $_SESSION["picture"] = $picture;

                    $authenticateModel->deleteSessions(ini_get("session.gc_maxlifetime"));
                    $authenticateModel->setSession(session_id(), $_SESSION["userId"]);
                }
            }
        }
    }
}
