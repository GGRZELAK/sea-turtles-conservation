<?php
namespace Framework;

use PDO;
use PDOException;

class Model
{

    /**
     * Fonction permettant de se connecter à une base de donnée et de communiquer avec
     *
     * @param string $sql
     * @param array $data
     * @return \PDOStatement
     */
    function executerRequete($sql, $data = null, $error = false)
    {
        $conf = (file_exists("Framework/prod.ini")) ? parse_ini_file("Framework/prod.ini") : parse_ini_file("Framework/dev.ini");
        try {
            $bdd = new PDO('mysql:host=' . $conf["adress"] . ';dbname=' . $conf["name"] . ';charset=utf8', $conf["user"], $conf["password"]);
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $req = $bdd->prepare($sql);
            $req->execute($data);
            if (stripos($sql, "INSERT") !== false) {
                $lastInsertId = $bdd->lastInsertId();
                if ($lastInsertId) return $lastInsertId;
            }
            return $req;
        } catch (PDOException $e) {
            
            if ($error) {
                
                return $e->getMessage();
                
            } else {
                
                var_dump($sql);
                var_dump($data);
                echo "executerRequete : " . $e->getMessage();
                exit();
            }
        }
    }
}
