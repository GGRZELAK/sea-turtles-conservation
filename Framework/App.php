<?php
namespace Framework;

class App
{
    public $protocol;
    public $dns;
    private $documentRoot;
    public $asset;
    public $image;
    public $script;
    public $style;
    public $file;
    public $language;
    
    public function __construct()
    {
        $this->protocol = (!empty($_SERVER["HTTPS"]) && $_SERVER['HTTPS'] != "off") ? "https://" : "http://";
        $this->dns = $GLOBALS["dns"];
        $this->documentRoot = $_SERVER["DOCUMENT_ROOT"] . '/';
        $this->asset = "Assets/";
        $this->image = "img/";
        $this->script = "js/";
        $this->style = "css/";
        $this->file = "files/";
        $this->language = (!empty($GLOBALS["language"])) ? $GLOBALS["language"] : "en";
    }
    
    public function getDocumentRoot()
    {
        return $this->documentRoot;
    }
}
