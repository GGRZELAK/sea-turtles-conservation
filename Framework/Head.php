<?php
namespace Framework;

class Head
{
    private $app;
    private $favicon;
    private $title;
    private $colorTheme;
    private $icons = array();
    private $googleFonts = array();
    private $fonts = array();
    private $styles = array();
    private $externalStyles = array();
    private $script = array();
    private $externalScript = array();
    private $dependencies = array();
    
    public function __construct()
    {
        $this->app = new App();
    }
    
    /**
     * Ajout du header dans le DOM
     */
    public function generateHead()
    {
        echo '<head>', "\n";
        echo '<meta charset="utf-8" />', "\n"; // Encodage
        echo '<meta name="HandheldFriendly" content="True" />', "\n"; // Affichage responsif
        $this->getColorTheme();
        $this->getFavicon();
        $this->getTitle();
        $this->getIcons();
        $this->getGoogleFonts();
        $this->getDependencies();
        $this->getScript();
        $this->getStyles();
        echo '</head>', "\n";
    }
    
    /**
     * Injecte le thème de couleur
     */
    private function getColorTheme()
    {
        if (!empty($this->colorTheme))
        {
            echo '<meta name="theme-color" content="', $this->colorTheme, '" />', "\n";
        }
    }
    
    /**
     * Injecte le favicon
     */
    private function getFavicon()
    {
        if (!empty($this->favicon))
        {
            echo '<link rel="icon" type="image/png" href="', $this->favicon, '" />', "\n";
        }
    }
    
    /**
     * Injecte le titre
     */
    private function getTitle()
    {
        if (!empty($this->title))
        {
            echo '<title>', $this->title, '</title>', "\n";
        }
    }
    
    /**
     * Injecte les packs d'icones
     */
    private function getIcons()
    {
        foreach ($this->icons as $icon)
        {
            echo $icon, "\n";
        }
    }
    
    /**
     * Injecte les polices Google
     */
    private function getGoogleFonts()
    {
        foreach ($this->googleFonts as $googleFont)
        {
            echo '<link href="https://fonts.googleapis.com/css?family=', $googleFont, '" rel="stylesheet" />', "\n";
        }
    }
    
    /**
     * Injecte les polices
     */
    private function getFonts()
    {
        foreach ($this->fonts as $font)
        {
            echo $font, "\n";
        }
    }
    
    /**
     * Injecte les styles
     */
    private function getStyles()
    {
        // CDN Style
        foreach($this->externalStyles as $externalStyle)
        {
            echo '<link href="', $externalStyle["file"], '" rel="stylesheet" type="text/css" ';
            if ($externalStyle["async"]) echo "async";
            echo ' />', "\n";
        }
        
        // Style local
        foreach ($this->styles as $style)
        {
            echo '<link href="/', $this->app->asset, $this->app->style, $style["file"], '?', filemtime('./' . $this->app->asset . $this->app->style . $style["file"]), '" rel="stylesheet" type="text/css" ';
            if ($style["async"]) echo "async";
            echo ' />', "\n";
        }
    }
    
    /**
     * Injecte les script
     */
    private function getScript()
    {
        // CDN Script
        for ($currScript = 0; $currScript < count($this->externalScript); $currScript++)
        {
            echo '<script src="', $this->externalScript[$currScript]["file"], '" type="text/javascript"';
            if ($this->externalScript[$currScript]["async"]) echo " async";
            echo '></script>', "\n";
        }
        
        // Script local
        for ($currScript = 0; $currScript < count($this->script); $currScript++)
        {
            echo '<script src="/' . $this->app->asset . $this->app->script, $this->script[$currScript]["file"], '?', filemtime("./" . $this->app->asset . $this->app->script . $this->script[$currScript]["file"]), '" type="text/javascript"';
            if ($this->script[$currScript]["async"]) echo " async";
            echo '></script>', "\n";
        }
    }
    
    /**
     * Injecte les autres dépendances
     */
    private function getDependencies()
    {
        for ($currDependency = 0; $currDependency < count($this->dependencies); $currDependency++)
        {
            echo $this->dependencies[$currDependency], "\n";
        }
    }
    
    /**
     * Surcharge le thème de couleur du navigateur
     * @param string $colorTheme
     */
    public function setColorTheme($colorTheme)
    {
        $this->colorTheme = $colorTheme;
    }
    
    /**
     * Définit le Favicon du site
     * @param string $href
     */
    public function setFavicon($href)
    {
        $this->favicon = $href;
    }
    
    /**
     * Définit le titre de la page
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Ajoute un pack d'icones
     * @param string $link
     */
    public function setIcon($link)
    {
        $this->setExternalStyle($link, false);
    }
    
    /**
     * Ajoute une police
     * @param string $link
     */
    public function setFont($link)
    {
        $this->setExternalStyle($link, false);
    }
    
    /**
     * Ajoute un style local
     * @param string $style
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setStyle($style, $async = true)
    {
        array_push($this->styles, array("file" => $style, "async" => $async));
    }
    
    /**
     * Ajoute le style depuis un CDN
     * @param string $style
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setExternalStyle($style, $async = true)
    {
        array_push($this->externalStyles, array("file" => $style, "async" => $async));
    }
    
    /**
     * Ajoute un script local dans le header
     * @param string $script
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setScript($script, $async = true)
    {
        array_push($this->script, array("file" => $script, "async" => $async));
    }
    
    /**
     * Ajoute un script depuis un CDN dans le header
     * @param string $script
     * @param bool $async Chargement asynchrone, actif par défaut.
     */
    public function setExternalScript($script, $async = true)
    {
        array_push($this->externalScript, array("file" => $script, "async" => $async));
    }
    
    /**
     * Injection direct d'autres balises dans le header
     * @param string $dependency
     */
    public function setDependency($dependency)
    {
        array_push($this->dependencies, $dependency);
    }
}

