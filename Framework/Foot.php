<?php
namespace Framework;

class Foot
{
    private $script = array();
    private $externalScript = array();
    private $app;
    
    public function __construct()
    {
        $this->app = new App();
    }
    
    /**
     * Ajoute des script dans le DOM
     */
    public function generateFoot()
    {
        $this->getScript();
    }
    
    /**
     * Injecte les scripts
     */
    private function getScript()
    {
        // CDN Script
        for ($currScript = 0; $currScript < count($this->externalScript); $currScript++)
        {
            echo '<script src="', $this->externalScript[$currScript], '" type="text/javascript"></script>', "\n";
        }
        
        // Script local
        for ($currScript = 0; $currScript < count($this->script); $currScript++)
        {
            echo '<script src="/' . $this->app->asset . $this->app->script, $this->script[$currScript], '" type="text/javascript"></script>', "\n";
        }
    }
    
    /**
     * Ajoute un script local à la fin du body
     * @param string $script
     */
    public function setScript($script)
    {
        array_push($this->script, $script);
    }
    
    /**
     * Ajoute un script depuis un CDN à la fin du body
     * @param string $script
     */
    public function setExternalScript($script)
    {
        array_push($this->externalScript, $script);
    }
}
