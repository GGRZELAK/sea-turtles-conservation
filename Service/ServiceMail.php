<?php
namespace Service;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ServiceMail
{
    public function send($subject, $content, $receiver_mail, $sender_mail = "contact@gaelmedias.fr", $sender_name = false, $receiver_name = false)
    {
        $mail = new PHPMailer(true);
        try {
            
            $conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);
            
            //Server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $conf["MAIL"]["host"];
            $mail->SMTPAuth = true;
            $mail->Username = $conf["MAIL"]["username"];
            $mail->Password = $conf["MAIL"]["password"];
            $mail->SMTPSecure = $conf["MAIL"]["security"];
            $mail->Port = $conf["MAIL"]["port"];
            
            //Recipients
            $mail->setFrom(utf8_decode($sender_mail), utf8_decode($sender_name));
            $mail->addAddress(utf8_decode($receiver_mail), utf8_decode($receiver_name));
            
            //Content
            $mail->isHTML(true);
            $mail->Subject = utf8_decode($subject);
            $mail->Body    = utf8_decode($content);
            
            $mail->send();
            
            return "success";
        }
        catch (Exception $e)
        {
            return $mail->ErrorInfo;
        }
    }
}
