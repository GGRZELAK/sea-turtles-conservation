<?php
namespace Service;

class ServiceURL
{

    public function changeParam($askParam, $askValue = false, $url = false)
    {
        if ($url) {
            $param = array();
            $start = stripos($url, '?') + 1;
            $end = stripos($url, '#');
            if ($end !== false) {
                $lenght = $end - $start;
            } else {
                $lenght = strlen($url) - $start;
            }
            $paramString = substr($url, $start, $lenght);
            $params = explode('&', $paramString);
            foreach ($params as $value) {
                $temp = explode('=', $value);
                $param[$temp[0]] = $temp[1];
            }
        } else {
            $param = $_GET;
        }

        if (is_array($askParam)) {
            foreach ($askParam as $id => $currentParam) {
                if (! empty($askValue[$id])) {
                    $param[$currentParam] = $askValue[$id];
                } else {
                    unset($param[$currentParam]);
                }
            }
        } elseif ($askValue) {
            $param[$askParam] = $askValue;
        } elseif (array_key_exists($askParam, $param)) {
            unset($param[$askParam]);
        }

        unset($currentParam);
        $newUrl = null;
        if ($param) {
            $newUrl = "?";
            foreach ($param as $currentParam => $currentValue) {
                if ($newUrl != '?') {
                    $newUrl .= '&';
                }
                $newUrl .= urlencode($currentParam) . '=' . urlencode($currentValue);
            }
        }

        if ($end !== false) {
            $newUrl .= substr($url, $end);
        }

        return $newUrl;
    }
}
