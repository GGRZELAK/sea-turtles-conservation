<?php
namespace Controller\Api;

use Model\AuthenticateModel;
use Model\UsersModel;
use Service\ServiceMail;
use Framework\Controller;
use Template\HeaderTemplate;
use Framework\App;
use View\RegisterView;
use View\LoginView;

class AuthenticateController extends Controller
{

    public function login($language = "en")
    {
        $response = array();
        $response["success"] = false;
        $response["error"] = false;
        
        if (!empty($_POST)) {
        
            $match = false;
    
            if (! empty($_POST["email"]) && ! empty($_POST["password"])) {
    
                $email = strip_tags($_POST["email"]);
                $password = strip_tags($_POST["password"]);
    
                $usersModel = new UsersModel();
                
                $users = $usersModel->searchUsers(array(
                    "email" => $email
                ));
                
                $users = array_column($users, null, "id");
    
                if (! empty($users)) {
    
                    foreach ($users as $user) {
    
                        if (! empty($user['password']) && $user['password'] == $password) {
                            
                            $match = $user["id"];
                        }
                    }
                }
                    
                if ($match) {
                    
                    $user = $users[$match];
                    
                    if (! $user["forbidden"]) {
                        
                        $authenticateModel = new AuthenticateModel();
                    
                        $app = new App();
                        
                        $picture = null;
                        if (! empty($user["picture"])) {
                            $picture = $user["picture"];
                        } elseif (file_exists($app->getDocumentRoot() . $app->asset . $app->image . "users/" . $user["id"] . ".jpg")) {
                            $picture = '/' . $app->asset . $app->image . "users/" . $user["id"] . ".jpg";
                        }
                        
                        $_SESSION["userId"] = $user["id"];
                        $_SESSION["firstName"] = $user["firstname"];
                        $_SESSION["lastName"] = $user["lastname"];
                        $_SESSION["email"] = $email;
                        $_SESSION["birthdate"] = $user["birthdate"];
                        $_SESSION["registration"] = $user["registration"];
                        $_SESSION["language"] = $user["language"];
                        $_SESSION["picture"] = $picture;
                        
                        $response["success"]["id"] = $user["id"];
                        $response["success"]["firstName"] = $user["firstname"];
                        $response["success"]["lastName"] = $user["lastname"];
                        $response["success"]["email"] = $email;
                        $response["success"]["registration"] = $user["registration"];
                        $response["success"]["language"] = $user["language"];
                        $response["success"]["picture"] = $picture;
                        
                        $authenticateModel->deleteSessions(ini_get("session.gc_maxlifetime"));
                        $authenticateModel->setSession(session_id(), $user['id']);
                        
                        if (! empty($_POST['autologin'])) {
                            
                            $authenticateModel->setAutologin($_SESSION["userId"], $_COOKIE["device"]);
                        }
                        
                        $response["success"]["message"] = "Welcome " . $_SESSION["firstName"];
                        if (!empty($_SESSION["lastName"])) {
                            $response["success"]["message"] .= ' ' . $_SESSION["lastName"];
                        } else {
                            $response["success"]["message"] .= ' (' . $_SESSION["registration"] . ')';
                        }
                        $response["success"]["message"] .= '.';
                        
                        if (! empty($_POST["output"]) && strip_tags($_POST["output"]) == "html") {
                            
                            $headerTemplate = new HeaderTemplate();
                            $response["success"]["html"] = $headerTemplate->profilTools();
                        }
                    } else {
                        
                        $response["error"] = "You are not allowed to sign in.";
                    }
                } else {
                    
                    $response["error"] = "Email or password entered is incorrect.";
                }
                    
            } else {
    
                $response["error"] = "Please complete all fields.";
            }
        }
        
        if ($GLOBALS["subDns"] == "api")
        {
            header('Content-Type: application/json');
            echo json_encode($response, JSON_NUMERIC_CHECK);
            
        } else {
            
            new LoginView($response);
        }
    }

    public function logout()
    {
        $message = array();
        
        $authenticateModel = new AuthenticateModel();
        $authenticateModel->deleteAutologin($_COOKIE["device"]);
        $authenticateModel->deleteSession(session_id());

        unset($_SESSION);
        session_destroy();

        $headerTemplate = new HeaderTemplate();
        $message["success"]["html"] =  $headerTemplate->loginTools();
        
        $message["success"]["message"] = "You are disconnected.";
        
        header('Content-Type: application/json');
        echo json_encode($message, JSON_NUMERIC_CHECK);
    }

    public function forgot()
    {
        $message = array();
        $message["error"] = false;
        $message["success"] = false;

        if (! empty($_POST["forgot"])) {

            $forgot = strip_tags($_POST["forgot"]);

            $usersModel = new UsersModel();
            $users = $usersModel->searchUsers(array(
                "email" => $forgot
            ));

            if (! empty($users)) {
                
                $conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

                $content = null;

                foreach ($users as $user) {

                    $content .= "<p>Hi " . $user["firstname"] . ",</p>";
                    $content .= "<p>Here is your password: <strong>" . $user["password"] . "</strong></p>";
                }

                $content .= "<p>A bientot sur " . $conf["APP"]["name"] ."</p>";

                $mail = new ServiceMail();

                $mail->send("Mot de passe " . $conf["APP"]["name"], $content, $forgot);

                $message["success"] = "Your password has just been sent to you by email. Remember to check your spam.";
            }
        }

        header('Content-Type: application/json');
        echo json_encode($message, JSON_NUMERIC_CHECK);
    }

    public function update()
    {
        $message = array();
        $message["error"] = false;
        $message["success"] = false;
        
        if (!empty($_SESSION["userId"])) {

            if (! empty($_POST["email"]) || ! empty($_POST["password"])) {
    
                $usersModel = new UsersModel();
    
                $update = array();
                $update["email"] = false;
                $update["password"] = false;
    
                if (! empty($_POST["email"])) {
    
                    $email = strip_tags($_POST["email"]);
    
                    $update["email"] = $usersModel->updateUserEmail(array(
                        "id" => $_SESSION["userId"],
                        "email" => $email
                    ));
    
                    $_SESSION["email"] = $email;
    
                    if (! empty($_POST["output"]) && strip_tags($_POST["output"]) == "html") {
    
                        $headerTemplate = new HeaderTemplate();
                        $message["success"]["html"] = $headerTemplate->profilTools();
                    }
                }
    
                if (! empty($_POST["password"])) {
    
                    $password = strip_tags($_POST["password"]);
    
                    $update["password"] = $usersModel->updateUserPassword(array(
                        "id" => $_SESSION["userId"],
                        "password" => $password
                    ));
                }
    
                if ($update["email"] && $update["password"])
                    $message["success"]["message"] = "Your email address and password have been changed.";
                elseif ($update["email"])
                    $message["success"]["message"] = "Your email address has been changed.";
                elseif ($update["password"])
                    $message["success"]["message"] = "Your password has been changed.";
                else
                    $message["error"] = "No changes have been made.";
            } else {
    
                $message["error"] = "Please fill in at least one field.";
            }
        } else {
            
            $message["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($message, JSON_NUMERIC_CHECK);
    }
    
    public function register($languaga = "en")
    {
        $response = array();
        $response["success"] = false;
        $response["error"] = false;
        
        if (! empty($_POST)) {
            
            if (!empty($_SESSION["userId"])) {
                
                if (! empty($_POST["firstName"]) && !empty($_POST["language"])) {
                    
                    $usersModel = new UsersModel();
                    
                    $firstName = strip_tags($_POST["firstName"]);
                    $language = strip_tags($_POST["language"]);
                    
                    $email = (!empty($_POST["email"])) ? strip_tags($_POST["email"]) : null;
                    $password = (!empty($_POST["password"])) ? strip_tags($_POST["password"]) : null;
                    
                    $alreadyExist = false;
                    
                    if ($email && $password) {
                        
                        $users = $usersModel->searchUsers(array(
                            "email" => $email,
                            "password" => $password
                        ));
                        
                        if (!empty($users)) {
                            
                            $alreadyExist = true;
                        }
                        
                    } else {
                        
                        $users = $usersModel->searchUsers(array(
                            "firstname" => $firstName,
                            "registration" => date('Y')
                        ));
                        
                        if (!empty($users)) {
                            
                            $alreadyExist = true;
                        }
                    }
                    
                    if ($alreadyExist) {
                        
                        $response["error"] = "This user already exists. Please choose another email and password.";
                    }
                    
                    if (empty($response["error"])) {
                        
                        $data = array();
                        
                        $data["email"] = $email;
                        $data["password"] = $password;
                        $data["firstname"] = $firstName;
                        $data["lastname"] = (! empty($_POST["lastName"])) ? strip_tags($_POST["lastName"]) : null;
                        $data["birthdate"] = (! empty($_POST["birthdate"])) ? strip_tags($_POST["birthdate"]) : null;
                        $data["registration"] = date('Y');
                        $data["language"] = $language;
                        $data["picture"] = (! empty($_POST["picture"])) ? strip_tags($_POST["picture"]) : null;
                        
                        $userId = $usersModel->setUser($data);
                        
                        if (! empty($userId)) {
                            
                            $response["success"]["json"]["userid"] = $userId;
                            
                            $response["success"]["html"] = '<option value="' . $userId . '">' . $data["firstname"] . ' ' . $data["lastname"] . ' (' . $data["registration"] . ')</option>';
                            
                            $response["success"]["message"] = "User was successfully created.";
                        } else {
                            
                            $response["error"] = "Error during the creating process of the user.";
                        }
                    }
                    
                } else {
                    
                    $response["error"] = "Please complete all required fields.";
                }
            } else {
                
                $response["error"] = "You are not connected.";
            }
        }
        
        if ($GLOBALS["subDns"] == "api")
        {
            header('Content-Type: application/json');
            echo json_encode($response, JSON_NUMERIC_CHECK);
            
        } else {
            
            new RegisterView($response);
        }
    }

    public function updateInfosClient()
    {
        
        if (! empty($_POST["localIp"]) && ! empty($_POST["userAgent"])) {

            $_SESSION["localIp"] = strip_tags($_POST["localIp"]);
            $_SESSION["userAgent"] = strip_tags($_POST["userAgent"]);
        } else {

            echo "strange think";
        }
    }
    
    public function getUsers()
    {
        $response = array();
        
        if (empty($_SESSION["userId"])) {
            
            $response["error"] = "You are not connected.";
            
        } else {
        
            $usersModel = new UsersModel();
            
            if (!empty($_POST["id"]) && !empty($_GET["id"])) {
                
                $id = (!empty($_POST["id"])) ? strip_tags($_POST["id"]) : strip_tags($_GET["id"]);
                
                $response = $usersModel->getUser($id);
                
            } else {
                
                $users = $usersModel->getUsers();
                
                if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
                    !empty($_GET["output"]) && $_GET["output"] == "select") {
                    
                    foreach ($users as $user) {
                        
                        $response[] = array("value" => $user["id"], "text" => $user["firstname"] . ' ' . $user["lastname"] . ' (' . $user["registration"] . ')');
                    }
                    
                } else {
                    
                    $response = $users;
                }
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}
