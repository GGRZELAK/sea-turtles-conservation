<?php
namespace Controller\Api;

use Framework\Controller;
use Model\GenericModel;
use DateTime;

class EntityController extends Controller
{

    public function delete($table, $pk)
    {
        $table = strip_tags($table);
        $pk = strip_tags($pk);

        $response = array();

        if (! empty($_SESSION["userId"])) {

            $genericModel = new GenericModel();
            $return = $genericModel->delData($table, $pk);

            if (strpos($return, "SQLSTATE") === false) {

                $response["success"] = $return . " deleted line.";
            } else {

                $response["error"] = $return;
            }
        } else {

            $response["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }

    public function update($table, $pk)
    {
        $table = strip_tags($table);
        $pk = strip_tags($pk);

        $response = array();

        if (! empty($_SESSION["userId"])) {

            if (! empty($_POST)) {

                if (! empty($_POST["column"]) && isset($_POST["value"])) {

                    $column = strip_tags($_POST["column"]);
                    $value = strip_tags($_POST["value"]);

                    if (DateTime::createFromFormat("d/m/Y H:i", $value) !== FALSE) {

                        $value = DateTime::createFromFormat('d/m/Y H:i', strip_tags($value))->format('Y-m-d H:i:s');
                    }

                    $values = array(
                        $column => $value
                    );

                    $genericModel = new GenericModel();
                    $return = $genericModel->updData($table, $values, array(
                        "id" => $pk
                    ));

                    if (strpos($return, "SQLSTATE") === false) {

                        $response["success"] = $return . " affected line.";
                    } else {

                        $response["error"] = $return;
                    }
                } else {

                    $response["error"] = "Please complete all required fields.";
                }
            } else {

                $response["error"] = "This page only accepts post parameters.";
            }
        } else {

            $response["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}

