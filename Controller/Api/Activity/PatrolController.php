<?php
namespace Controller\Api\Activity;

use Model\HatcheryModel;
use Model\Activity\PatrolModel;
use Model\TurtleModel;
use DateTime;
use Framework\Controller;

class PatrolController extends Controller
{

    public function set()
    {
        $response = array();

        if (!empty($_SESSION["userId"])) {

            $hatcheryModel = new HatcheryModel();
            $activityModel = new PatrolModel();
            $turtleModel = new TurtleModel();

            // Prepare data and check if they are logical

            if (!empty($_POST)) {

                if (!empty($_POST["date"]) && !empty($_POST["station"]) &&
                    !empty($_POST["leader"]) && !empty($_POST["beachlocation"]) &&
                    !empty($_POST["step"]) && !empty($_POST["laid"])) {

                        $dateTimeActivity = DateTime::createFromFormat('d/m/Y H:i', strip_tags($_POST["date"]));

                        $patrolActivityData = array();

                        $patrolActivityData["stationid"] = strip_tags($_POST["station"]);
                        $patrolActivityData["date"] = $dateTimeActivity->format('Y-m-d H:i:s');
                        $patrolActivityData["leader"] = strip_tags($_POST["leader"]);
                        $patrolActivityData["beachlocation"] = strip_tags($_POST["beachlocation"]);
                        $patrolActivityData["step"] = strip_tags($_POST["step"]);
                        $patrolActivityData["laid"] = strip_tags($_POST["laid"]);

                        $null = NULL;

                        $patrolActivityData["turtleid"] = $null;
                        $patrolActivityData["startlaying"] = $null;
                        $patrolActivityData["endlaying"] = $null;
                        $patrolActivityData["oldtide"] = (!empty($_POST["oldtide"])) ? strip_tags($_POST["oldtide"]) : $null;
                        $patrolActivityData["tide"] = $null;
                        $patrolActivityData["assistant"] = (!empty($_POST["assistant"])) ? strip_tags($_POST["assistant"]) : $null;
                        $patrolActivityData["beachplace"] = $null;
                        $patrolActivityData["widthreturntrace"] = (!empty($_POST["widthreturntrace"])) ? strip_tags($_POST["widthreturntrace"]): $null;
                        $patrolActivityData["fire"] = (!empty($_POST["fire"])) ? 1 : $null;
                        $patrolActivityData["residues"] = (!empty($_POST["residues"])) ? 1 : $null;
                        $patrolActivityData["destination"] = $null;
                        $patrolActivityData["hatcheryid"] = $null;
                        $patrolActivityData["nestid"] = $null;
                        $patrolActivityData["comments"] = (!empty($_POST["comments"])) ? strip_tags($_POST["comments"]) : $null;

                        if (!empty($_POST["destination"]) && !empty($_POST["builder"]) &&
                            !empty($_POST["eggs"]) && !empty($_POST["nestdate"])) {

                            $nestTime = DateTime::createFromFormat('H:i', strip_tags($_POST["nestdate"]));

                            $nestData = array();

                            $patrolActivityData["destination"] = (is_numeric($_POST["destination"])) ? "hatchery" : strip_tags($_POST["destination"]);

                            $nestData["builder"] = strip_tags($_POST["builder"]);
                            $nestData["buildingdate"] = $dateTimeActivity->format("Y-m-d") . $nestTime->format(" H:i:s");
                            $nestData["eggs"] = strip_tags($_POST["eggs"]);

                            $nestData["code"] = $null;
                            $nestData["hatcheryid"] = $null;
                            $nestData["hatcheryleader"] = (!empty($_POST["hatcheryleader"])) ? strip_tags($_POST["hatcheryleader"]) : $null;

                            if ($patrolActivityData["destination"] == "natural") {

                                if (!empty($_POST["beachplace"])) {

                                    $patrolActivityData["beachplace"] = strip_tags($_POST["beachplace"]);

                                } else {

                                    $response["error"] = "Please enter the Height on the beach.";
                                }

                            } elseif ($patrolActivityData["destination"] == "relocated") {

                                if (!empty($_POST["code"])) {

                                    $nestData["code"] = strip_tags($_POST["code"]);

                                } else {

                                    $response["error"] = "Please enter the nest Code (location).";
                                }

                            } else {

                                if (!empty($_POST["code"]) && !empty($_POST["beachplace"])) {

                                    $nestData["code"] = strip_tags($_POST["code"]);
                                    $nestData["hatcheryid"] = strip_tags($_POST["destination"]);

                                    $patrolActivityData["beachplace"] = strip_tags($_POST["beachplace"]);
                                    $patrolActivityData["hatcheryid"] = $nestData["hatcheryid"];

                                } else {

                                    $response["error"] = "Please enter the Height on the beach and the nest Code (location).";
                                }
                            }

                            if (empty($response["error"])) {

                                if (is_numeric($_POST["destination"])) {

                                    if (!empty($_POST["startlaying"])) {

                                        $startLaying = DateTime::createFromFormat('H:i', strip_tags($_POST["startlaying"]));
                                        $patrolActivityData["startlaying"] = $startLaying->format("H:i:s");

                                        if ($patrolActivityData["step"] > 4) {

                                            $response["error"] = "You cannot have the Start laying time at this step.";
                                        }

                                    }

                                    if (!empty($_POST["endlaying"])) {

                                        $endLaying = DateTime::createFromFormat('H:i', strip_tags($_POST["endlaying"]));
                                        $patrolActivityData["endlaying"] = $endLaying->format("H:i:s");

                                        if ($patrolActivityData["step"] > 5) {

                                            $response["error"] = "You cannot have the End laying time at this step.";
                                        }
                                    }
                                }
                            }

                        } else if (!empty($_POST["widthreturntrace"])) {

                            $patrolActivityData["widthreturntrace"] = strip_tags($_POST["widthreturntrace"]);

                        } else {

                            $response["error"] = "Please enter at least the Width of the return trace.";
                        }

                        if (empty($response["error"])) {

                            if (!empty($_POST["speciesid"]) || !empty($_POST["motherid"]) || !empty($_POST["fatherid"]) || !empty($_POST["lefttag"]) || !empty($_POST["righttag"]) || !empty($_POST["length"]) || !empty($_POST["width"]) || !empty($_POST["lefthurt"]) || !empty($_POST["righthurt"]) || !empty($_POST["birthdate"]) || !empty($_POST["manager"]) || !empty($_POST["turtlecomments"])) {

                                $turtleData = array();

                                $turtleData["id"] = uniqid();
                                $turtleData["speciesid"] = (!empty($_POST["species"])) ? strip_tags($_POST["species"]) : $null;
                                $turtleData["motherid"] = (!empty($_POST["mother"])) ? strip_tags($_POST["mother"]) : $null;
                                $turtleData["fatherid"] = (!empty($_POST["father"])) ? strip_tags($_POST["father"]) : $null;
                                $turtleData["lefttag"] = (!empty($_POST["lefttag"])) ? strip_tags($_POST["lefttag"]) : $null;
                                $turtleData["righttag"] = (!empty($_POST["righttag"])) ? strip_tags($_POST["righttag"]) : $null;
                                $turtleData["length"] = (!empty($_POST["length"])) ? strip_tags($_POST["length"]) : $null;
                                $turtleData["width"] = (!empty($_POST["width"])) ? strip_tags($_POST["width"]) : $null;
                                $turtleData["lefthurt"] = (!empty($_POST["lefthurt"])) ? strip_tags($_POST["lefthurt"]) : $null;
                                $turtleData["righthurt"] = (!empty($_POST["righthurt"])) ? strip_tags($_POST["righthurt"]) : $null;
                                $turtleData["referencingdate"] = $patrolActivityData["date"];
                                $turtleData["birthdate"] = (!empty($_POST["birthdate"])) ? strip_tags($_POST["birthdate"]) : $null;
                                $turtleData["manager"] = (!empty($_POST["manager"])) ? strip_tags($_POST["manager"]) : $null;
                                $turtleData["comments"] = (!empty($_POST["turtlecomments"])) ? strip_tags($_POST["turtlecomments"]) : $null;

                            } elseif ($patrolActivityData["step"] < 5) {

                                $response["warning"] = "No data about the turtle.";
                            }
                        }

                } else {

                    $response["error"] = "Please complete all required fields.";
                }

            } else {

                $response["error"] = "This page accepts only post parameters.";
            }

            // If we don't have any data after this, that mean the user try to do something strange

            if (empty($nestData) && empty($turtleData) && empty($patrolActivityData)) {

                $response["error"] = 'Process faillure. Please contact Gaël GRZELAK at <a href="mailto:gael.grzelak@gmail.com">gael.grzelak@gmail.com</a>';
            }

            // Case of edition, getting of previous data

            if (!empty($_POST["id"])) {

                $activity = $activityModel->getPatrol(strip_tags($_POST["id"]));
            }

            // Creating nest

            if (empty($response["error"])) {

                if (!empty($nestData)) {

                    if (!empty($activity["nestid"])) {

                        $nestData["id"] = $activity["nestid"];

                        $hatcheryModel->updNest($nestData);

                        $patrolActivityData["nestid"] = $activity["nestid"];

                    } else {

                        if (!empty($nestData["hatcheryid"])) {

                            $lastNest = $hatcheryModel->getLastNest($nestData["hatcheryid"]);
                        }

                        if (!empty($nestData["code"]) && !empty($lastNest["code"]) && $nestData["code"] == $lastNest["code"]) {

                            $response["error"] = "This nest Code (location) is already used.";

                        } else {

                            $patrolActivityData["nestid"] = $hatcheryModel->setNest($nestData);

                        }
                    }

                } elseif (!empty($activity["nestid"])) {

                    $hatcheryModel->delNest($activity["nestid"]);
                }
            }

            // Creating turtle

            if (empty($response["error"])) {

                if (!empty($turtleData)) {

                    if (!empty($activity["turtleid"])) {

                        $turtleData["id"] = $activity["turtleid"];

                        $turtleModel->updTurtle($turtleData);

                        $patrolActivityData["turtleid"] = $activity["turtleid"];

                    } else {

                        $turtle = $turtleModel->getTurtleByTag($turtleData["speciesid"], $turtleData["lefttag"], $turtleData["righttag"]);

                        if (!empty($turtle)) {

                            $patrolActivityData["turtleid"] = $turtle["id"];

                        } else {

                            $request = $turtleModel->setTurtle($turtleData);

                            if (!empty($request)) {

                                $patrolActivityData["turtleid"] = $turtleData["id"];
                            }
                        }
                    }

                } elseif (!empty($activity["turtleid"])) {

                    $turtleModel->delTurtle($activity["turtleid"]);
                }
            }

            // Creating patrol activity

            if (empty($response["error"])) {

                if (!empty($patrolActivityData)) {

                    if (!empty($activity["id"])) {

                        $patrolActivityData["id"] = $activity["id"];

                        $activityModel->updPatrol($patrolActivityData);

                        $response["success"]["upd"] = "Patrol activity " . $activity["id"] . " has been updated successfully.";

                    } else {

                        $request = $activityModel->setPatrol($patrolActivityData);

                        if (!empty($request)) {

                            $response["success"]["set"] = "Patrol activity added with id :" . $request;
                        }
                    }
                }
            }

        } else {

            $response["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }

    public function get()
    {
        $response = array();

        $activityModel = new PatrolModel();

        if (!empty($_GET["id"])) {

            $id = strip_tags($_GET["id"]);

            $response = $activityModel->getPatrol($id);

        } else if (!empty($_GET["year"])) {

            $year = strip_tags($_GET["year"]);

            $response = $activityModel->getPatrols($year);
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}

