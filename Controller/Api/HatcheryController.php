<?php
namespace Controller\Api;

use Framework\Controller;
use Model\HatcheryModel;

class HatcheryController extends Controller
{
    private $hatcheryModel;
    
    public function __construct()
    {
        $this->hatcheryModel = new HatcheryModel();
    }

    public function set()
    {
        $response = array();
        $response["success"] = false;
        $response["error"] = false;
        
        if (!empty($_SESSION["userId"])) {

            if (! empty($_POST)) {
    
                if (! empty($_POST["station"]) && ! empty($_POST["hatcheryname"]) && ! empty($_POST["width"]) && ! empty($_POST["height"]) && ! empty($_POST["numberoflocations"])) {
                    
                    $data = array();
    
                    $stationiId = strip_tags($_POST["station"]);
                    $name = strip_tags($_POST["hatcheryname"]);
                    $width = strip_tags($_POST["width"]);
                    $height = strip_tags($_POST["height"]);
                    $numberOfLocations = strip_tags($_POST["numberoflocations"]);
    
                    $problem = $this->hatcheryModel->getHatcheryByName($name, $stationiId);
    
                    if (empty($problem)) {
    
                        $data = array();
    
                        $data["stationid"] = $stationiId;
                        $data["name"] = $name;
                        $data["width"] = $width;
                        $data["height"] = $height;
                        $data["numberoflocations"] = $numberOfLocations;
                        $data["latgps"] = (! empty($_POST["hatcherylatgps"])) ? strip_tags($_POST["hatcherylatgps"]) : null;
                        $data["longps"] = (! empty($_POST["hatcherylongps"])) ? strip_tags($_POST["hatcherylongps"]) : null;
                        $data["comments"] = (! empty($_POST["hatcherycomments"])) ? strip_tags($_POST["hatcherycomments"]) : null;
    
                        $hatcheryId = $this->hatcheryModel->setHatchery($data);
    
                        if (! empty($hatcheryId)) {
                            
                            $response["success"]["hatcheryid"] = $hatcheryId;
    
                            $response["success"]["html"] = '<option value="' . $hatcheryId . '" selected>' . $name . '</option>';
    
                            $response["success"]["message"] = "Hatchery was successfully created.";
                        } else {
    
                            $response["error"] = "Error during the creating process of the hatchery.";
                        }
                    } else {
    
                        $response["error"] = "This hatchery name is already used for this station.";
                    }
                } else {
    
                    $response["error"] = "Please complete all required fields.";
                }
            } else {
    
                $response["error"] = "This page only accepts post parameters.";
            }
        } else {
            
            $response["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
    
    public function get() {
        
        $stationId = null;
        
        if (!empty($_POST["stationId"])) {
            
            $stationId = strip_tags($_POST["stationId"]);
            
        } else if (!empty($_GET["stationId"])) {
            
            $stationId = strip_tags($_GET["stationId"]);
        }
        
        $response = array();
        
        if ($stationId) {
        
            $hatcheries = $this->hatcheryModel->getHatcheries($stationId);
        
        } else {
            
            $hatcheries = $this->hatcheryModel->getAllHatcheries();
        }
        
        if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
            !empty($_GET["output"]) && $_GET["output"] == "select") {
            
            foreach($hatcheries as $hatchery) {
                
                $response[] = array("value" => $hatchery["id"], "text" => $hatchery["name"]);
            }
            
        } else {
            
            $response = $hatcheries;
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
    
    public function getNests() {
        
        $response = array();
        
        if (!empty($_POST["id"]) || !empty($_GET["id"])) {
            
            $id = (!empty($_POST["id"])) ? strip_tags($_POST["id"]) : strip_tags($_GET["id"]);
            
            $response = $this->hatcheryModel->getNest($id);
            
        } else {
            
            $nests = $this->hatcheryModel->getAllNests();
            
            if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
                !empty($_GET["output"]) && $_GET["output"] == "select") {
                    
                foreach($nests as $nest) {
                    
                    $response[] = array("value" => $nest["id"], "text" => $nest["code"] . ' - ' . $nest["eggs"] . " eggs");
                }
            } else{
                $response = $nests;
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}

