<?php
namespace Controller\Api;

use Framework\Controller;
use Model\TurtleModel;

class TurtleController extends Controller
{
    public function get()
    {
        $response = array();
        
        $turtleModel = new TurtleModel();
        
        if (!empty($_POST["id"]) || !empty($_GET["id"])) {
            
            $id = (!empty($_POST["id"])) ? strip_tags($_POST["id"]) : strip_tags($_GET["id"]);
            
            $response = $turtleModel->getTurtle($id);
            
        } else {
            
            $turtles = $turtleModel->getTurtles();
            
            if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
                !empty($_GET["output"]) && $_GET["output"] == "select") {
                
                foreach ($turtles as $turtle) {
                    
                    $text = '';
                    if (!empty($turtle["lefttag"]) || !empty($turtle["righttag"])) {
                        if (!empty($turtle["lefttag"])) {
                            $text .= $turtle["lefttag"];
                        }
                        $text .= ':';
                        if (!empty($turtle["righttag"])) {
                            $text .= $turtle["righttag"];
                        }
                    }
                    
                    $response[] = array("value" => $turtle["id"], "text" => $text);
                }
                
            } else {
                
                $response = $turtles;
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
    
    public function setSpecies()
    {
        $response = array();
        $response["success"] = false;
        $response["error"] = false;
        
        if (!empty($_SESSION["userId"])) {
        
            if (! empty($_POST)) {
                
                $turtleModel = new TurtleModel();
                
                if (! empty($_POST["scientificname"]) && !empty($_POST["englishname"])) {
                    
                    $scientificName = strip_tags($_POST["scientificname"]);
                    $englishName = strip_tags($_POST["englishname"]);
                    
                    $problem = $turtleModel->getSpeciesByNames($scientificName, $englishName);
                    
                    if (empty($problem)) {
                        
                        $data = array();
                        
                        $data["scientificname"] = $scientificName;
                        $data["englishname"] = $englishName;
                        
                        $speciesId = $turtleModel->setSpecies($data);
                        
                        if (! empty($speciesId)) {
                            
                            $response["success"]["html"] = '<option value="' . $speciesId . '" selected>' . $englishName . '</option>';
                            
                            $response["success"]["message"] = "Species was successfully created.";
                        } else {
                            
                            $response["error"] = "Error during the creating process of the species.";
                        }
                    } else {
                        
                        $response["error"] = "This species is already registered.";
                    }
                } else {
                    
                    $response["error"] = "Please complete all required fields.";
                }
            } else {
                
                $response["error"] = "This page only accepts post parameters.";
            }
        } else {
            
            $response["error"] = "You are not connected.";
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
    
    function getSpecies()
    {
        $response = array();
        
        $turtleModel = new TurtleModel();
        
        $id = null;
        
        if (!empty($_POST["id"])) {
            
            $id = strip_tags($_POST["id"]);
            
        } else if (!empty($_GET["id"])) {
            
            $id = strip_tags($_GET["id"]);
        }
            
        $species = $turtleModel->getSpecies($id);
        
        if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
            !empty($_GET["output"]) && $_GET["output"] == "select") {
            
            foreach ($species as $specie) {
            
                $response[] = array("value" => $specie["id"], "text" => $specie["englishname"]);
            }
            
        } else {
            
            $response = $species;
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}

