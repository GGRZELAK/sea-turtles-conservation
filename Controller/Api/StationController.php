<?php
namespace Controller\Api;

use Framework\Controller;
use Model\StationModel;

class StationController extends Controller
{

    public function set()
    {
        $response = array();
        $response["success"] = false;
        $response["error"] = false;
        
        if (!empty($_SESSION["userId"])) {

            if (! empty($_POST)) {
    
                $stationModel = new StationModel();
    
                if (! empty($_POST["name"])) {
                    
                    $name = strip_tags($_POST["name"]);
                    
                    $problem = $stationModel->getStationByName($name);
                    
                    if (empty($problem)) {
    
                        $data = array();
        
                        $data["name"] = strip_tags($_POST["name"]);
                        $data["address"] = (! empty($_POST["address"])) ? strip_tags($_POST["address"]) : null;
                        $data["contact"] = (! empty($_POST["contact"])) ? strip_tags($_POST["contact"]) : null;
                        $data["latgps"] = (! empty($_POST["latgps"])) ? strip_tags($_POST["latgps"]) : null;
                        $data["longps"] = (! empty($_POST["longps"])) ? strip_tags($_POST["longps"]) : null;
                        $data["comments"] = (! empty($_POST["comments"])) ? strip_tags($_POST["comments"]) : null;
        
                        $stationId = $stationModel->setStation($data);
        
                        if (! empty($stationId)) {
        
                            $response["success"]["html"] = '<option value="' . $stationId . '" selected>' . $data["name"] . '</option>';
        
                            $response["success"]["message"] = "Station was successfully created.";
                        } else {
        
                            $response["error"] = "Error during the creating process of the station.";
                        }
                    } else {
                        
                        $response["error"] = "This station name is already used.";
                    }
                } else {
    
                    $response["error"] = "Please complete all required fields.";
                }
            } else {
    
                $response["error"] = "This page only accepts post parameters.";
            }
        } else {
            
            $response["error"] = "You are not connected.";
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
    
    public function get()
    {
        $response = array();
        
        $stationModel = new StationModel();
        
        if (!empty($_POST["id"]) || !empty($_GET["id"])) {
            
            $id = (!empty($_POST["id"])) ? strip_tags($_POST["id"]) : strip_tags($_GET["id"]);
            
            $response = $stationModel->getStation($id);
            
        } else {
        
            $stations = $stationModel->getStations();
            
            if (!empty($_POST["output"]) && $_POST["output"] == "select" ||
                !empty($_GET["output"]) && $_GET["output"] == "select") {
                
                foreach($stations as $station) {
                    
                    $response[] = array("value" => $station["id"], "text" => $station["name"]);
                }
            } else {
                
                $response = $stations;
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($response, JSON_NUMERIC_CHECK);
    }
}

