<?php
namespace Controller;

use Framework\Controller;
use View\NotFoundView;

class NotFoundController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        
        new NotFoundView();
    }
}
