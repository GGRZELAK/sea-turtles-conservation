<?php
namespace Controller;

use Framework\Controller;
use View\DataView;
use DateTime;
use Framework\App;
use Model\StationModel;
use Model\HatcheryModel;
use Model\TurtleModel;
use Model\Activity\PatrolModel;
use Model\UsersModel;

class DataController extends Controller
{
    // public function __construct() {}
    
    public function import()
    {
        
        if (empty($_SESSION["userId"])) {
            
            $app = new App();
            header("Location: /" . $app->language . "/patrol/activities");
        }
        
        $message = array();
        $message["warning"] = array();
        
        $stationModel = new StationModel();
        $stations = $stationModel->getStations();
        
        $activityModel = new PatrolModel();
        
        if (! empty($_FILES["file"])) {
            
            if (!empty($_POST["station"])) {
                
                $stationid = strip_tags($_POST["station"]);
                
                $hatcheryModel = new HatcheryModel();
                $hatcheries = $hatcheryModel->getHatcheries($stationid);
                
            } else {
                
                $message["error"] = "Please select the station for which data will be injected.";
            }
            
            // Uploading file

            if ($_FILES["file"]["error"] != UPLOAD_ERR_OK) {

                $message["error"] = "Upload error.";
            }
            
            if (empty($message["error"])) {
            
                $app = new App();
                $uploads_dir = $app->getDocumentRoot() . $app->asset . $app->file;
                $tmp_name = $_FILES["file"]["tmp_name"];
                
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mimetype = finfo_file($finfo, $tmp_name);
                finfo_close($finfo);
                
                if ($mimetype != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                    
                    $message["error"] = "This file is not a valid excel file.";
                }
            }
            
            if (empty($message["error"])) {
                
                $name = "data" . time() . ".xlsx";
                $filename = $uploads_dir . '/' . $name;
                
                move_uploaded_file($tmp_name, $filename);
            }
            
            // Opening file
            
            if (empty($message["error"])) {
    
                $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
                @$spreadsheet = $reader->load($filename);
                
                if ($spreadsheet->getSheetCount() > 1) {
                    
                    $message["error"] = "This file contains more than only one sheet.";
                }
            }
    
            // Preparing data
            
            if (empty($message["error"])) {
                
                $worksheet = $spreadsheet->getActiveSheet();
                
                $pos = array();
                
                $null = NULL;
                
                $count = 0;
                
                function hurt($hurt) {
                    
                    global $null;
                    
                    switch (strtolower($hurt)) {
                        
                        case "cicatriz":
                            $return = "scar";
                            break;
                            
                        case "desgarre":
                            $return = "tear";
                            break;
                            
                        case "hueco":
                            $return = "hole";
                            break;
                            
                        default:
                            $return = $null;
                            break;
                    }
                    
                    return $return;
                }
                
                foreach ($worksheet->getRowIterator() as $row) {
                    
                    $patrolData = array();
                    $nestData = array();
                    $turtleData = array();
                    
                    if (empty($pos)) {
                        
                        // Searching and getting table header
                        
                        $cellIterator = $row->getCellIterator();
                        $cellIterator->setIterateOnlyExistingCells(FALSE);
                        
                        foreach ($cellIterator as $cell) {
                            
                            $value = $cell->getValue();
                            
                            if ($value == "Fecha" || !empty($pos)) {
                            
                                $pos[$cellIterator->getCurrentColumnIndex()] = $value;
                            }
                        }
                        
                        // Prevent empty row
                    } elseif ($worksheet->getCellByColumnAndRow(array_search("Enc", $pos), $row->getRowIndex())->getValue()) {
                        
                        // Fecha (date only)
                        
                        $fechaIndex = array_search("Fecha", $pos);
                        
                        $badFecha = $worksheet->getCellByColumnAndRow($fechaIndex, $row->getRowIndex())->getValue();
                        
                        if (is_float($badFecha)) {
                            
                            $fechaTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($badFecha);
                            $fecha = date("Y-m-d", $fechaTimestamp);
                            
                        } else {
                            
                            $fechaObject = DateTime::createFromFormat('d/m/y', $badFecha);
                            $fecha = $fechaObject->format("Y-m-d");
                            
                        }
                        
                        // Enc (time only)
                        
                        $encIndex = array_search("Enc", $pos);
                        
                        $badEnc = $worksheet->getCellByColumnAndRow($encIndex, $row->getRowIndex())->getValue();
                        
                        $encTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($badEnc);
                        $enc = date("H:i:s", $encTimestamp);
                        
                        // date (Fecha + Enc)
                        
                        $patrolData["date"] = $fecha . ' ' . $enc;
                        
                        // Check if an activity is not already recorded
                        
                        $activities = $activityModel->getPatrolActivityByDate($stationid, $patrolData["date"]);
                        
                        if (!empty($activities)) {
                            
                            array_push($message["warning"], "Patrol activity " . $patrolData["date"] . " is already recorded.");
                            continue;
                        }
                        
                        // Lider (leader)
                        
                        $patrolData["leader"] = 1;
                        
                        $liderIndex = array_search("Lider", $pos);
                        
                        if (!empty($liderIndex)) {
                            
                            $badLider = $worksheet->getCellByColumnAndRow($liderIndex, $row->getRowIndex())->getValue();
                            
                            $badUser = explode(' ', $badLider);
                            
                            if (!empty($badUser[0]) && !empty($badUser[1])) {
                                
                                $userModel = new UsersModel();
                                
                                $user = $userModel->searchUsers(array(
                                    "firstname" => $badUser[0],
                                    "lastname" => $badUser[1]
                                ));
                                
                                if (!empty($user)) {
                                    
                                    $patrolData["leader"] = $user[0]["id"];
                                    
                                } else {
                                    
                                    $data = array();
                                    $data["firstname"] = $badUser[0];
                                    $data["lastname"] = $badUser[1];
                                    $data["email"] = $null;
                                    $data["password"] = $null;
                                    $data["forbidden"] = $null;
                                    $data["birthdate"] = $null;
                                    $data["registration"] = date('Y', $fecha);
                                    $data["language"] = "en";
                                    $data["picture"] = $null;
                                    
                                    $patrolData["leader"] = $userModel->setUser($data);
                                }
                            }
                        }
                        
                        // I. P. (startLaying)
                        
                        $ipIndex = array_search("I. P.", $pos);
                        
                        $badIp = $worksheet->getCellByColumnAndRow($ipIndex, $row->getRowIndex())->getValue();
                        
                        $ipTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($badIp);
                        
                        $ip = ($ipTimestamp) ? date("H:i:s", $ipTimestamp) : $null;
                        
                        $patrolData["startlaying"] = $ip;
                        
                        // F. P. (endLaying)
                        
                        $fpIndex = array_search("F. P.", $pos);
                        
                        $badFp = $worksheet->getCellByColumnAndRow($fpIndex, $row->getRowIndex())->getValue();
                        
                        $fpTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($badFp);
                        
                        $fp = ($fpTimestamp) ? date("H:i:s", $fpTimestamp) : $null;
                        
                        $patrolData["endlaying"] = $fp;
                        
                        // Siembra (buildingdate)
                        
                        $siembraIndex = array_search("Siembra", $pos);
                        
                        $badSiembra = $worksheet->getCellByColumnAndRow($siembraIndex, $row->getRowIndex())->getValue();
                        
                        $siembraTimestamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($badSiembra);
                        
                        $siembra = ($siembraTimestamp) ? date("H:i:s", $siembraTimestamp) : $null;
                        
                        $nestData["buildingdate"] = $fecha . ' ' . $siembra;
                        
                        // Moj (beachLocation)
                        
                        $mojIndex = array_search("Moj", $pos);
                        
                        $moj = $worksheet->getCellByColumnAndRow($mojIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$moj) {
                            
                            $moj = $worksheet->getCellByColumnAndRow($mojIndex, $row->getRowIndex())->getValue();
                        }
                        
                        $patrolData["beachlocation"] = ($moj) ? $moj : $null;
                        
                        // Marea (oldTide)
                        
                        $mareaIndex = array_search("Marea", $pos);
                        
                        $badMarea = $worksheet->getCellByColumnAndRow($mareaIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$badMarea) {
                            
                            $badMarea = $worksheet->getCellByColumnAndRow($mareaIndex, $row->getRowIndex())->getValue();
                        }
                        
                        switch (preg_replace('/\s+/', '', $badMarea)) {
                            
                            case "1/2↓":
                                $patrolData["oldtide"] = "down";
                                break;
                                
                            case '↓':
                                $patrolData["oldtide"] = "low";
                                break;
                                
                            case "1/2↑":
                                $patrolData["oldtide"] = "up";
                                break;
                                
                            case '↑';
                                $patrolData["oldtide"] = "high";
                                break;
                                
                            default:
                                $patrolData["oldtide"] = $null;
                                break;
                        }
                        
                        // Etapa (step)
                        
                        $etapaIndex = array_search("Etapa", $pos);
                        
                        $etapa = $worksheet->getCellByColumnAndRow($etapaIndex, $row->getRowIndex())->getValue();
                        
                        $patrolData["step"] = ($etapa) ? $etapa : 8;
                        
                        // Espc (species)
                        
                        $espcIndex = array_search("Espc", $pos);
                        
                        $badEspc = $worksheet->getCellByColumnAndRow($espcIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$badEspc) {
                            
                            $badEspc = $worksheet->getCellByColumnAndRow($espcIndex, $row->getRowIndex())->getValue();
                        }
                        
                        switch (preg_replace('/\s+/', '', $badEspc)) {
                            
                            case "L.o.":
                                $turtleData["speciesid"] = 1;
                                break;
                                
                            case "E.i.":
                                $turtleData["speciesid"] = 2;
                                break;
                                
                            case "D.c.":
                                $turtleData["speciesid"] = 3;
                                break;
                                
                            case "C.m.";
                                $turtleData["speciesid"] = 4;
                                break;
                            
                            default:
                                $turtleData["speciesid"] = $null;
                                break;
                        }
                        
                        // Anid (laid)
                        
                        $anidIndex = array_search("Anid", $pos);
                        
                        $badAnid = $worksheet->getCellByColumnAndRow($anidIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$badAnid) {
                            
                            $badAnid = $worksheet->getCellByColumnAndRow($anidIndex, $row->getRowIndex())->getValue();
                        }
                        
                        switch (strtolower($badAnid)) {
                            
                            case "si":
                                $patrolData["laid"] = "yes";
                                break;
                                
                            case "no":
                                $patrolData["laid"] = "no";
                                break;
                            
                            default:
                                $patrolData["laid"] = "dontknow";
                                break;
                        }
                        
                        // Dest (destination)
                        
                        $destIndex = array_search("Dest", $pos);
                        
                        $badDest = $worksheet->getCellByColumnAndRow($destIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$badDest) {
                            
                            $badDest = $worksheet->getCellByColumnAndRow($destIndex, $row->getRowIndex())->getValue();
                        }
                        
                        $badDest = trim($badDest);
                        
                        switch (strtolower($badDest)) {
                            
                            case "natural":
                                $patrolData["destination"] = "natural";
                                break;
                                
                            case "reubicado":
                                $patrolData["destination"] = "relocated";
                                break;
                                
                            default:
                                $patrolData["destination"] = "hatchery";
                                break;
                        }
                        
                        // hatcheryId
                        
                        $patrolData["hatcheryid"] = $null;
                        $nestData["hatcheryid"] = $null;
                        
                        if ($patrolData["destination"] == "hatchery") {
                            
                            foreach ($hatcheries as $hatchery) {
                                
                                if ($badDest == $hatchery["name"]) {
                                    
                                    $patrolData["hatcheryid"] = $hatchery["id"];
                                    $nestData["hatcheryid"] = $hatchery["id"];
                                }
                            }
                        }
                        
                        // Cod (code)
                        
                        $CodIndex = array_search("Cod", $pos);
                        
                        $cod = $worksheet->getCellByColumnAndRow($CodIndex, $row->getRowIndex())->getValue();
                        
                        if (strlen($cod) > 4) {
                            
                            $cod = $null;
                        }
                        
                        $nestData["code"] = ($cod) ? $cod : $null;
                        
                        // Z ps (beachPlace)
                        
                        $zpsIndex = array_search("Z Ps", $pos);
                        
                        $badZps = $worksheet->getCellByColumnAndRow($zpsIndex, $row->getRowIndex())->getCalculatedValue();
                        
                        if (!$badZps) {
                            
                            $badZps = $worksheet->getCellByColumnAndRow($zpsIndex, $row->getRowIndex())->getValue();
                        }
                        
                        $badZps = trim($badZps);
                        
                        switch (strtolower($badZps)) {
                            
                            case "veg":
                                $patrolData["beachplace"] = "vegetation";
                                break;
                                
                            case "abierto":
                                $patrolData["beachplace"] = "walkingarea";
                                break;
                                
                            case "berma":
                                $patrolData["beachplace"] = "berm";
                                break;
                                
                            default:
                                $patrolData["beachplace"] = $null;
                                break;
                        }
                        
                        // T. Hu (eggs)
                        
                        $thuIndex = array_search("T. Hu", $pos);
                        
                        $thu = $worksheet->getCellByColumnAndRow($thuIndex, $row->getRowIndex())->getValue();
                        
                        $nestData["eggs"] = ($thu) ? $thu : 0;
                        
                        // Ancho Rastro (widthReturnTrace)
                        
                        $anchoRastroIndex = array_search("Ancho Rastro", $pos);
                        
                        $anchoRastro = $worksheet->getCellByColumnAndRow($anchoRastroIndex, $row->getRowIndex())->getValue();
                        
                        if (!is_float($anchoRastro)) {
                            
                            $anchoRastro = 0;
                        }
                        
                        $patrolData["widthreturntrace"] = ($anchoRastro) ? $anchoRastro : $null;
                        
                        // Observaciones (comments)
                        
                        $observacionesIndex = array_search("Observaciones", $pos);
                        
                        $observaciones = $worksheet->getCellByColumnAndRow($observacionesIndex, $row->getRowIndex())->getValue();
                        
                        $patrolData["comments"] = ($observaciones) ? $observaciones : $null;
                        
                        // Izq (leftTag)
                        
                        $izqIndex = array_search("Izq", $pos);
                        
                        $izq = $worksheet->getCellByColumnAndRow($izqIndex, $row->getRowIndex())->getValue();
                        
                        $turtleData["lefttag"] = ($izq) ? $izq : $null;
                        
                        // Der (rightTag)
                        
                        $derIndex = array_search("Der", $pos);
                        
                        $der = $worksheet->getCellByColumnAndRow($derIndex, $row->getRowIndex())->getValue();
                        
                        $turtleData["righttag"] = ($der) ? $der : $null;
                        
                        // turtleId
                        
                        $patrolData["turtleid"] = $null;
                        
                        $turtleModel = new TurtleModel();
                        
                        if (!empty($turtleData["speciesid"]) && (!empty($turtleData["lefttag"]) || !empty($turtleData["righttag"]))) {
                        
                            $patrolData["turtleid"] = $turtleModel->getTurtleByTag($turtleData["speciesid"], $turtleData["lefttag"], $turtleData["righttag"])["id"];
                        }
                        
                        if (empty($patrolData["turtleid"])) {
                            
                            // Evidencia (leftHurt, rightHurt)
                            
                            $evidenciaIndex = array_keys($pos, "Evidencia");
                            
                            $leftHurt = $worksheet->getCellByColumnAndRow($evidenciaIndex[0], $row->getRowIndex())->getCalculatedValue();
                            
                            if (!$leftHurt) {
                                
                                $leftHurt = $worksheet->getCellByColumnAndRow($evidenciaIndex[0], $row->getRowIndex())->getValue();
                            }
                            
                            $rightHurt = $worksheet->getCellByColumnAndRow($evidenciaIndex[1], $row->getRowIndex())->getCalculatedValue();
                            
                            if (!$rightHurt) {
                                
                                $rightHurt = $worksheet->getCellByColumnAndRow($evidenciaIndex[1], $row->getRowIndex())->getValue();
                            }
                            
                            $turtleData["lefthurt"] = ($leftHurt) ? hurt(trim($leftHurt)) : $null;
                            $turtleData["righthurt"] = ($rightHurt) ? hurt(trim($rightHurt)) : $null;
                            
                            // Prom LCC (length)
                            
                            $promLccIndex = array_search("Prom LCC", $pos);
                            
                            $promLcc = $worksheet->getCellByColumnAndRow($promLccIndex, $row->getRowIndex())->getValue();
                            
                            $turtleData["length"] = ($promLcc) ? $promLcc : $null;
                            
                            // Prom ACC (width)
                            
                            $promAccIndex = array_search("Prom ACC", $pos);
                            
                            $promAcc = $worksheet->getCellByColumnAndRow($promAccIndex, $row->getRowIndex())->getValue();
                            
                            $turtleData["width"] = ($promAcc) ? $promAcc : $null;
                            
                            // Creating Turtle
                            
                            if ((!empty($turtleData["speciesid"]) && $patrolData["step"] < 7) ||
                                !empty($turtleData["righttag"]) ||
                                !empty($turtleData["lefttag"]) ||
                                !empty($turtleData["righttag"]) ||
                                !empty($turtleData["lefthurt"]) ||
                                !empty($turtleData["righthurt"]) ||
                                !empty($turtleData["length"]) ||
                                !empty($turtleData["width"])) {
                                    
                                $turtleData["id"] = uniqid();
                                $turtleData["referencingdate"] = $patrolData["date"];
                                $turtleData["motherid"] = $null;
                                $turtleData["fatherid"] = $null;
                                $turtleData["birthdate"] = $null;
                                $turtleData["manager"] = $null;
                                $turtleData["comments"] = $null;
                                
                                $turtleModel->setTurtle($turtleData);
                                
                                $patrolData["turtleid"] = $turtleData["id"];
                            }
                        }
                        
                        // Creating Nest
                        
                        $patrolData["nestid"] = $null;
                        
                        if (!empty($nestData["hatcheryid"]) ||
                            !empty($nestData["code"]) ||
                            !empty($nestData["eggs"])) {
                                
                            $nestData["builder"] = $patrolData["leader"];
                            $nestData["hatcheryleader"] = $null;
                                
                            $patrolData["nestid"] = $hatcheryModel->setNest($nestData);
                        }
                        
                        // Creating Patrol Activity
                        
                        $patrolData["stationid"] = $stationid;
                        $patrolData["tide"] = $null;
                        $patrolData["assistant"] = $null;
                        $patrolData["fire"]= $null;
                        $patrolData["residues"] = $null;
                        
                        $activityModel->setPatrol($patrolData);
                        
                        $count++;
                    }
                }
                
                $message["success"] = $count . " Patrol activity added !";
            }
        }

        $view = new DataView(null);
        $view->import($stations, $message);
    }
}

