<?php
namespace Controller;

use Model\GenericModel;
use View\EntityView;
use Model\UsersModel;
use Framework\Controller;

class EntityController extends Controller
{

    public function get($language, $table)
    {
        $language = strip_tags($language);
        $table = strip_tags($table);

        $userModel = new UsersModel();
        $leaders = array_column($userModel->getLeaders(), null, "userid");

        if ($table == "users" && (empty($_SESSION["userId"]) || (! empty($_SESSION["userId"]) && ! in_array($_SESSION["userId"], array_keys($leaders))))) {

            header("Location: /");
        }

        $authorizedTable = array(
            "turtles",
            "species",
            "stations",
            "hatcheries",
            "users",
            "leaders",
            "patrolactivities",
            "nests"
        );
        
        $responses = array();

        if (in_array($table, $authorizedTable)) {
            
            // @todo Affichage du tableau par ajax
            // @todo Trie

            $genericModel = new GenericModel();
            
            if (!empty($_POST) && !empty($_SESSION["userId"])) {
                
                $count = 0;
                
                foreach (array_keys($_POST) as $id) {
                    
                    $id = strip_tags($id);
                    
                    $response = $genericModel->delData($table, $id);
                    
                    if (is_int($response)) {
                        
                        $count += $response;
                        
                    } else {
                        
                        $responses["error"][] = $response;
                    }
                }
                
                if ($count) {
                    
                    $responses["success"][] = $count . " deleted lines.";
                }
            }

            $struct = array_column($genericModel->getStruct($table), null, "Field");
            $foreignKey = array_column($genericModel->getForeignKey($table), null, "COLUMN_NAME");
            $struct = array_merge_recursive($struct, $foreignKey);

            foreach ($struct as $field => $settings) {

                $matches = array();
                preg_match('/\((.*)\)/', $settings["Type"], $matches);

                if (! empty($matches)) {

                    $struct[$field]["Type"] = str_replace($matches[0], null, $settings["Type"]);
                    $typeValue = $matches[1];

                    if ($struct[$field]["Type"] == "set") {

                        $struct[$field]["Type"] = "select";
                        $struct[$field]["set"] = str_replace("'", null, $typeValue);
                    }
                }
            }

            $title = ucfirst($table);

            if ($table == "patrolactivities") {

                $title = "Patrol activities";
            }

            $orderColumn = "date";

            switch ($table) {
                case "turtles":
                    $orderColumn = "referencingdate";
                    break;

                case "users":
                    $orderColumn = "registration";
                    break;

                case "nests":
                    $orderColumn = "buildingdate";
                    break;
            }

            $rows = $genericModel->getData($table, [], [], [], array(
                $orderColumn => "DESC"
            ));

            new EntityView($title, $language, $table, $struct, $rows, $responses);
        } else {

            header("Location: /" . $language . '/');
        }
    }
}

