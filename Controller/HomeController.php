<?php
namespace Controller;

use Framework\Controller;
use Model\Activity\PatrolModel;
use Model\HatcheryModel;
use Model\TurtleModel;
use View\HomeView;
use View\LanguageView;
use Model\UsersModel;
use Model\StationModel;

class HomeController extends Controller
{
    public function language()
    {
        header("Location: " . $this->ici . $GLOBALS["language"] . '/');
        
        new LanguageView();
    }
    
    public function index($language)
    {
        $usersModel = new UsersModel();
        $activityModel = new PatrolModel();
        $nestModel = new HatcheryModel();
        $stationModel = new StationModel();
        $turtleModel = new TurtleModel();

        $users = $usersModel->getUsers();
        $leaders = array_column($usersModel->getLeaders(), null, "userid");
        $lastActivities = $activityModel->getLastPatrols();
        $stations = array_column($stationModel->getStations(), null, "id");
        $nests = array_column($nestModel->getAllNests(), null, "id");
        $lastTurtles = $turtleModel->getLastTurtles();

        $year = date('Y');
        $yearBefore = 2;
        $years = array($year);
        for ($i = 1; $i <= $yearBefore; $i++) {
            array_push($years, (string)($year-$i));
        }
        $patrolsByYear = $activityModel->getPatrolsByYear($years);
        setlocale(LC_TIME, $language . '_' . strtoupper($language));
        $meetingTurtlesStat[0] = $years;
        array_unshift($meetingTurtlesStat[0], "Months");
        foreach($patrolsByYear as $patrol) {
            $year = date('Y', strtotime($patrol["date"]));
            if (in_array($year, $years)) {
                $month = date('n', strtotime($patrol["date"]));
                $monthName = ucfirst(strftime("%B", mktime(0, 0, 0, $month, 1)));
                if (empty($meetingTurtlesStat[$month])) {
                    $meetingTurtlesStat[$month][0] = $monthName;
                }
                foreach ($years as $key => $curYear) {
                    if ($year == $curYear) {
                        if (empty($meetingTurtlesStat[$month][$key+1])) {
                            $meetingTurtlesStat[$month][$key+1] = 1;
                        } else {
                            $meetingTurtlesStat[$month][$key+1] += 1;
                        }
                    } else if (empty($meetingTurtlesStat[$month][$key+1])) {
                        $meetingTurtlesStat[$month][$key+1] = 0;
                    }
                }
            }
        }

        new HomeView($users, $leaders, $stations, $language, array_values($meetingTurtlesStat), $lastActivities, $nests, $lastTurtles);
    }
}
