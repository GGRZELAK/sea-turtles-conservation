<?php
namespace Controller\Authenticate;

use Model\LogModel;
use PDORow;
use PDOStatement;
use Framework\App;

class LogController
{

    private $model;

    private $app;

    public function __construct()
    {
        $this->model = new LogModel();
        $this->app = new App();
    }
    
    /**
     * Enregistrement du couple Périphériques / userAgent
     * 
     * @param int $deviceId L'id du périphérique
     * @param int $agentId L'id de l'userAgent
     * @return PDORow $deviceAgent La jointure entre les tables
     */
    public function deviceAgent($deviceId, $agentId)
    {
        $data = array();
        $data["deviceId"] = $deviceId;
        $data["agentId"] = $agentId;
        
        $deviceAgent = $this->model->getDeviceAgent($data);
        
        if (empty($deviceAgent)) {
            
            $rowCount = $this->model->setDeviceAgent($data);
            
            if ($rowCount) {
                
                $deviceAgent = $this->model->getDeviceAgent($data);
            }
        }
        
        return $deviceAgent;
    }
    
    /**
     * Enregistrement du couple Périphériques / IP
     *
     * @param string $device L'ID unique du périphérique
     * @param int $externalIpId L'ID de l'IP publique
     * @param int $localIpId L'ID de l'IP locale
     * @return PDORow $ipDevice La jointure entre les tables
     */
    public function ipDevice($device, $externalIpId, $localIpId = null)
    {
        $data = array();
        $data["deviceId"] = $device;
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        
        $ipDevice = $this->model->getIpDevices($data);
        
        if (empty($ipDevice)) {
            
            $rowCount = $this->model->setIpDevices($data);
            
            if ($rowCount) {
                
                $ipDevice = $this->model->getIpDevices($data);
            }
        }
        
        return $ipDevice;
    }
    
    /**
     * Enregistrement du coupke userAgent / IP
     * 
     * @param int $agentId L'ID de l'userAgent 
     * @param int $externalIpId L'ID de l'IP publique
     * @param int $localIpId L'ID de l'IP locale
     * @return PDORow $ipAgent La jointure entre les tables
     */
    public function ipAgent($agentId, $externalIpId, $localIpId = null)
    {
        $data = array();
        $data["agentId"] = $agentId;
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        
        $ipAgent = $this->model->getIpAgent($data);
        
        if (empty($ipAgent)) {
            
            $rowCount = $this->model->setIpAgent($data);
            
            if ($rowCount) {
                
                $ipAgent = $this->model->getIpAgent($data);
            }
        }
        
        return $ipAgent;
    }
    
    /**
     * Enregistrement du couple Utilisateur / userAgent
     * 
     * @param int $agentId L'ID de l'userAgent
     * @return PDORow $userAgent La jointure entre les tables
     */
    public function userAgent($agentId)
    {
        $data = array();
        $data["userId"] = $_SESSION["userId"];
        $data["agentId"] = $agentId;
        
        $userAgent = $this->model->getUserAgent($data);
        
        if (empty($userAgent)) {
            
            $rowCount = $this->model->setUserAgent($data);
            
            if ($rowCount) {
                
                $userAgent = $this->model->getUserAgent($data);
            }
        }
        
        return $userAgent;
    }
    
    /**
     * Enregistrement du couple Utilisateur / Périphérique
     * 
     * @param int $deviceId L'ID du périphérique
     * @return PDORow $userDevice La jointure entre les tables
     */
    public function userDevice($deviceId)
    {
        $data = array();
        $data["userId"] = $_SESSION["userId"];
        $data["deviceId"] = $deviceId;
        
        $userDevice = $this->model->getUserDevice($data);
        
        if (empty($userDevice)) {
            
            $rowCount = $this->model->setUserDevice($data);
            
            if ($rowCount) {
                
                $userDevice = $this->model->getUserDevice($data);
            }
        }
        
        return $userDevice;
    }
    
    /**
     * Enregistrement du couple Utilisateur / IP
     *
     * @param int $externalIpId L'ID de l'IP publique
     * @param int $localIpId L'ID de l'IP locale
     * @return PDORow $userDevice La jointure entre les tables
     */
    public function userIp($externalIpId, $localIpId = null)
    {
        $data = array();
        $data["userId"] = $_SESSION["userId"];
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        
        $userIp = $this->model->getUserIp($data);
        
        if (empty($userIp)) {
            
            $rowCount = $this->model->setUserIp($data);
            
            if ($rowCount) {
                
                $userIp = $this->model->getUserIp($data);
            }
        }
        
        return $userIp;
    }
    
    /**
     * Enregistrement de la page visitée
     * 
     * @param int $pageId L'ID de la page visitée
     * @param int $deviceId L'ID du périphérique
     * @param int  $agentId L'ID de l'userAgent
     * @param int $externalIpId L'ID de l'IP publique
     * @param int $localIpId L'ID de l'IP locale
     * @return PDOStatement 1 si une ligne a été enregistrée, 0 si non
     */
    public function history($pageId, $deviceId, $agentId, $externalIpId, $localIpId = null)
    {
        $data = array();
        $data["pageId"] = $pageId;
        $data["deviceId"] = $deviceId;
        $data["userId"] = (!empty($_SESSION["userId"])) ? $_SESSION["userId"] : null;
        $data["agentId"] = $agentId;
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        
        return $this->model->history($data);
    }
}

