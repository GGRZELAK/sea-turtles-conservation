<?php
namespace Controller\Authenticate;

use Model\FilterModel;
use Framework\App;

class FilterController
{

    private $model;

    private $app;

    public function __construct()
    {
        $this->model = new FilterModel();
        $this->app = new App();
    }

    public function blacklist($pageId, $deviceId, $agentId, $externalIpId, $localIpId = null)
    {
        $data = array();
        $data["pageId"] = $pageId;
        $data["deviceId"] = $deviceId;
        $data["userId"] = (! empty($_SESSION["userId"])) ? $_SESSION["userId"] : null;
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        $data["agentId"] = $agentId;
        if ($this->model->getBlacklist($data) || ! filter_var($_SERVER["REMOTE_ADDR"], FILTER_VALIDATE_IP)) {
            header("Location: https://www.facebook.com/");
        }
    }
    
    public function addToBlacklist($deviceId, $pageId = null, $userId = null, $agentId = null, $externalIpId = null, $localIpId = null)
    {
        $data = array();
        $data["pageId"] = $pageId;
        $data["deviceId"] = $deviceId;
        $data["userId"] = $userId;
        $data["externalIpId"] = $externalIpId;
        $data["localIpId"] = $localIpId;
        $data["agentId"] = $agentId;
        
        return $this->model->setBlacklist($data);
    }
}
