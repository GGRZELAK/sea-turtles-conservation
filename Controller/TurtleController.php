<?php
namespace Controller;

use Framework\Controller;
use View\TurtleView;
use Model\TurtleModel;
use Model\Activity\PatrolModel;
use Model\StationModel;
use Model\UsersModel;
use Model\HatcheryModel;

class TurtleController extends Controller
{

    public function get($language, $id)
    {
        $id = strip_tags($id);

        $dic = array();
        $dic["turtle"] = array(
            "en" => "turtle",
            "es" => "tortuga",
            "fr" => "tortue",
            "de" => "schildkröte"
        );
        
        $turtleModel = new TurtleModel();
        $activityModel = new PatrolModel();
        $stationModel = new StationModel();
        $usersModel = new UsersModel();
        $hatcheryModel = new HatcheryModel();
                
        $stations = array_column($stationModel->getStations(), null, "id");
        $users = array_column($usersModel->getUsers(), null, "id");
        $hatcheries = array_column($hatcheryModel->getAllHatcheries(), null, "id");
        $nests = array_column($hatcheryModel->getAllNests(), null, "id");
        
        $turtle = $turtleModel->getTurtle($id);
        $activities = $activityModel->getPatrolsByTurtle($id);

        new TurtleView($turtle, $activities, $stations, $users, $hatcheries, $nests, $language, $dic);
    }
}

