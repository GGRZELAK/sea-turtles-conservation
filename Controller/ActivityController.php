<?php
namespace Controller;

use Framework\Controller;
use Model\Activity\PatrolModel;
use Model\HatcheryModel;
use Model\StationModel;
use Model\TurtleModel;
use Model\UsersModel;
use View\Activity\GetPatrolView;
use View\Activity\SetPatrolView;

class ActivityController extends Controller
{
    private $turtleModel;
    private $usersModel;
    private $stationModel;
    
    public function __construct() {
        
        parent::__construct();
        
        $this->usersModel = new UsersModel();
        $this->stationModel = new StationModel();
        $this->turtleModel = new TurtleModel();
    }

    public function setPatrol($language) {
        
        $language = strip_tags($language);
        
        $stations = array_column($this->stationModel->getStations(), null, "id");
        $users = array_column($this->usersModel->getUsers(), null, "id");
        $species = $this->turtleModel->getSpecies();
        
        $activity = null;
        $turtles = null;
        $hatcheries = null;
        $nests = null;
        
        if (!empty($_GET["id"])) {
            
            $id = strip_tags($_GET["id"]);
            
            $activityModel = new PatrolModel();
            $hatcheryModel = new HatcheryModel();
            
            $activity = $activityModel->getPatrol($id);
            $turtles = array_column($this->turtleModel->getTurtles(), null, "id");
            $hatcheries = array_column($hatcheryModel->getAllHatcheries(), null, "id");
            $nests = array_column($hatcheryModel->getAllNests(), null, "id");
            
        } elseif (empty($_SESSION["userId"])) {
            
            header("Location: /" . $language . "/patrol/activities");
        }
        
        new SetPatrolView($stations, $users, $species, $language, $activity, $turtles, $hatcheries, $nests);
    }
    
    public function getPatrol($language, $year) {
        
        $language = strip_tags($language);
        
        $activityModel = new PatrolModel();
        $hatcheryModel = new HatcheryModel();
        
        $activities = $activityModel->getPatrols(strip_tags($year));
        
        $seasonsSQL = $activityModel->getSeasons();
        $seasons = array_column($seasonsSQL, "count", "year");
        
        $stations = array_column($this->stationModel->getStations(), null, "id");
        $users = array_column($this->usersModel->getUsers(), null, "id");
        $turtles = array_column($this->turtleModel->getTurtles(), null, "id");
        $hatcheries = array_column($hatcheryModel->getAllHatcheries(), null, "id");
        $nests = array_column($hatcheryModel->getAllNests(), null, "id");
        
        new GetPatrolView($activities, $seasons, $year, $stations, $users, $turtles, $hatcheries, $nests, $language);
    }
    
    public function addYear() {
        
        header("Location: " . $this->ici . '/' . date('Y'));
    }
}

