$(function () {
	
	// Post on "Add user" modal
	$('#adduser form').submit(function() {
		
		$.ajax({

			type : "POST",
			data : $(this).serialize(),
			url : protocol + "api." + dns + "/set/user",
			timeout: 5000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {

					$.growl.error({ message: response["error"] });

				} else if (response["success"]) {

					$('#adduser').modal('hide');
					
					$("#leader option:first-child").after(response["success"]["html"]);
					$("#assistant option:first-child").after(response["success"]["html"]);
					$("#hatcheryleader option:first-child").after(response["success"]["html"]);
					$("#builder option:first-child").after(response["success"]["html"]);
					$("#contact option:first-child").after(response["success"]["html"]);
					
					$(fieldIdAddUser + ' option[value="' + response["success"]["json"]["userid"] + '"]').attr("selected", "selected");

					$.growl.notice({ message: response["success"]["message"] });
				}
				
				return false;
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});
});
