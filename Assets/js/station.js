$(function () {
	
	// Post on "Add station" modal
	$('#addstation form').submit(function() {
		
		$.ajax({

			type : "POST",
			data : $(this).serialize(),
			url : protocol + "api." + dns + "/set/station",
			timeout: 5000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {

					$.growl.error({ message: response["error"] });

				} else if (response["success"]) {

					$('#addstation').modal('hide');
					
					$("#station option:first-child").after(response["success"]["html"]);

					$.growl.notice({ message: response["success"]["message"] });
				}
				
				return false;
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});
});
