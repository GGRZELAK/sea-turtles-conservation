$(function () {
	
	// Post on "Add hatchery" modal
	$('#addhatchery form').submit(function() {
		
		$.ajax({

			type : "POST",
			data : $(this).serialize(),
			url : protocol + "api." + dns + "/set/hatchery",
			timeout: 5000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {

					$.growl.error({ message: response["error"] });

				} else if (response["success"]) {

					$('#addhatchery').modal('hide');

					if (typeof(getHatcheries) == "function") {

						getHatcheries(response["success"]["hatcheryid"]);
					}

					$.growl.notice({ message: response["success"]["message"] });
				}
				
				return false;
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});
});
