/**
 * array_column() retourne les valeurs d'une colonne de input, identifiée par la clé column_key.
 * Optionnellement, vous pouvez fournir un paramètre index_key pour indexer les valeurs dans le tableau retourné par les valeurs de la colonne index_key du tableau d'entrée.
 * @param input Un tableau multi-dimensionnel ou un tableau d'objets à partir duquel on extrait une colonne de valeur.
 * @param column_key La colonne de valeurs à retourner. Cette valeur peut être la clé entière de la colonne que vous souhaitez récupérer, ou bien le nom de la clé pour un tableau associatif. Il peut aussi valoir NULL pour retourner le tableau complet ou des objets (ceci peut être utile en conjonction du paramètre index_key pour ré-indexer le tableau).
 * @param index_key La colonne à utiliser comme index/clé pour le tableau retourné. Cette valeur peut être la clé entière de la colonne, ou le nom de la clé.
 * @return Retourne un tableau de valeurs représentant une seule colonne depuis le tableau d'entrée.
 * @link http://php.net/manual/fr/function.array-column.php Pour voir des exemples d'utilisations
 * @author Gaël GRZELAK
 */
function array_column(input, column_key = null, index_key = false) {
	
	array = {};
	
	for (firstKey in input) {
		
		if (index_key && input[firstKey][index_key] != "undefined") {
			
			key = input[firstKey][index_key];
			
		} else {
			
			key = firstKey;
		}

		if (column_key && input[firstKey][column_key] != "undefined") {
			
			array[key] = input[firstKey][column_key];
		}
		else if (input[firstKey] != "undefined") {
			
			array[key] = input[firstKey];
		}
	}
	
	return array;
}

function tooltipEvent() {

	$('[data-toggle="tooltip"]').tooltip();
}

function datetimepickerEvent() {
	
    $('.datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        locale: language
    });
}

function datepickerEvent() {

    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY',
    	//format: 'L',
        locale: language
    });
}

function timepickerEvent() {

    $('.timepicker').datetimepicker({
        format: 'HH:mm',
    	//format: 'LT',
    	locale: language
    });
}

function yearpickerEvent() {

    $('.yearpicker').datetimepicker({
    	viewMode: 'years',
        format: 'YYYY',
    	//format: 'LT',
    	locale: language
    });
}

$(function () {
	
	tooltipEvent();
	
	setTimeout(function() {
		$(".auto-dismiss").alert("close");
	}, 10000);
	
	datetimepickerEvent();
	datepickerEvent();
	timepickerEvent();
	yearpickerEvent();
	
	// Before open modal
	$(".modal").on("show.bs.modal", function() {
		
		// Close other modal
		$(".modal:not(#"+this.id+")").modal("hide");
	});

	// After open modal
	$(".modal").on("shown.bs.modal", function() {
		
		//if it has an autofocus element, focus on it.
		$(this).find("[autofocus]").focus();
	});
});
