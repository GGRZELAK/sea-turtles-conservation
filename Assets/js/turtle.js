$(function () {
	
	// Post on "Add station" modal
	$('#addspecies form').submit(function() {
		
		$.ajax({

			type : "POST",
			data : $(this).serialize(),
			url : protocol + "api." + dns + "/set/species",
			timeout: 5000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {

					$.growl.error({ message: response["error"] });

				} else if (response["success"]) {

					$('#addspecies').modal('hide');
					
					$("#species").prepend(response["success"]["html"]);

					$.growl.notice({ message: response["success"]["message"] });
				}
				
				return false;
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});
});
