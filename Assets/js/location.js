$(function () {

	$(".get-position").click(function() {

		if (navigator.geolocation) {
			
			navigator.geolocation.getCurrentPosition(showPosition);
			
		} else {
			
			$.growl.error({ message: "Geolocation is not supported by this browser." });
		}
	});
	
	function showPosition(position) {
		
	  $(".get-position").parent().parents(".latgps").val(position.coords.latitude);
	  $(".get-position").parent().parents(".longps").val(position.coords.longitude);
	  
	}
});
