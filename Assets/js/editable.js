var editableType;
var editablePost = '';
var editableGet;
var editablePk;
var editableId;
var editableName;
var editableValue;
var editableColumn;
var editableHtml = '';
var editableSource;

function reset() {

	html = '<span\
		class="editable"';
	html += ' id="' + editableId + '"';
	if (editableType) {
		html += ' data-type="' + editableType + '"';
	}
	if (editablePost) {
		html += ' data-post="' + editablePost + '"';
	}
	if (editableGet) {
		html += ' data-get="' + editableGet + '"';
	}
	if (editablePk) {
		html += ' data-pk="' + editablePk + '"';
	}
	if (editableName) {
		html += ' data-name="' + editableName + '"';
	}
	if (editableSource) {
		html += ' data-source="' + editableSource + '"';
	}
	html += '>';
	html += (editableHtml) ? editableHtml : null;
	html += '</span>';

	$('#' + editableId).parent().replaceWith(html);

	editableType = '';
	editablePost = '';
	editableGet = '';
	editablePk = '';
	editableId = '';
	editableName = '';
	editableValue = '';
	editableColumn = '';
	editableHtml = '';
	editableSource = '';

	editable();
}

//Change content with a text area
function save() {

	if (editableType == "tinyint") {

		value = ($('#' + editableId).prop("checked")) ? "yes" : "no";

	} else {

		value = $('#' + editableId).val();
	}

	if (value != editableHtml) {

		if (editableType == "tinyint") {

			updateValue = (value == "yes") ? 1 : 0;

		} else {
			
			updateValue = value;
		}

		$.ajax({

			type : "POST",
			data : 'column=' + editableColumn + '&value=' + updateValue,
			url : editablePost,
			timeout: 3000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {

					reset();

					$.growl.error({ message: response["error"] });

				} else if (response["success"]) {

					editableHtml = value;

					reset();

					$.growl.notice({ message: response["success"] });
				}
			},
			error: function() {

				reset();

				$.growl.error({ message: "Check your Internet connection." });
			}
		});

	} else {

		reset();
	}
}

function get() {

	$.ajax({

		type : "POST",
		data : "output=select",
		url : editableGet,
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {

			if (response["error"]) {

				$.growl.error({ message: response["error"] });

			} else if (response) {

				html = '<option value=""></option>';
				for(element in response) {

					html += '<option value="' + response[element]["value"] + '">' + response[element]["value"] + ' - ' + response[element]["text"] + '</option>';
				}

				$("#" + editableId).html(html);
			}
		},
		error: function() {

			$.growl.error({ message: "Check your Internet connection." });
		}
	});
}

//Change content with an edit area
function edit(that) {

	editableId = that.id;

	editableType = ($(that).attr("data-type")) ? $(that).attr("data-type") : null;
	if ($(that).attr("data-url")) {

		editablePost = $(that).attr("data-url"); 

	} else if ($(that).attr("data-post")) {

		editablePost = $(that).attr("data-post");
	}
	editablePost = ($(that).attr("data-post")) ? $(that).attr("data-post") : null;
	editableGet = ($(that).attr("data-get")) ? $(that).attr("data-get") : null;
	editablePk = ($(that).attr("data-pk")) ? $(that).attr("data-pk") : null;
	editableName = ($(that).attr("data-name")) ? $(that).attr("data-name") : null;
	editableSource = ($(that).attr("data-source")) ? $(that).attr("data-source") : null;
	editableValue = ($(that).val()) ? $(that).val() : null;
	editableColumn = (editableName) ? editableName : editableId;
	if ($(that).html() && $(that).html() != "null") {

		editableHtml = $(that).html();
	}

	html = '<div style="position: relative;">';

	switch (editableType) {

	case "datetime":
		html += '<input name="' + editableColumn + '" id="' + editableId + '" class="datetimepicker form-control datetimepicker-input" type="text" value="';
		html += editableHtml;
		html += '" />';
		break;

	case "date":
		html += '<input name="' + editableColumn +'" id="' + editableId + '" class="datepicker form-control datetimepicker-input" data-toggle="datetimepicker" type="text" value="';
		html += editableHtml;
		html += '" />';
		break;

	case "time":
		html += '<input name="' + editableColumn +'" id="' + editableId + '" class="timepicker form-control datetimepicker-input" type="text" value="';
		html += editableHtml;
		html += '" />';
		break;

	case "year":
		html += '<input name="' + editableColumn +'" id="' + editableId + '" class="yearpicker form-control datetimepicker-input" data-toggle="datetimepicker" type="text" value="';
		html += editableHtml;
		html += '" />';
		break;

	case "int":
		html += '<input name="' + editableColumn +'" id="' + editableId + '" type="number" min="1" class="form-control" value="';
		html += editableHtml;
		html += '" />';
		break;

	case "float":
		html += '<input name="' + editableColumn +'" id="' + editableId + '" type="number" step="0.1" class="form-control" value="';    
		html += editableHtml;
		html += '" />';
		break;

	case "select":
		html += '<select name="' + editableColumn +'" id="' + editableId +'" class="custom-select">';
		if (editableGet) {

			get();

		} else if (editableSource) {

			data = editableSource.split(',');

			html += '<option value=""></option>';
			for(element in data) {

				html += '<option value="' + data[element] + '">' + data[element] + '</option>';
			}
		}
		html += '</select>';
		break;

	case "longtext":
		html += '<textarea name="' + editableColumn +'" id="' + editableId +'" rows="1" class="form-control">';
		html += editableHtml;
		html += '</textarea>';
		break;

	case "tinyint":
		html += '<div class="custom-control custom-checkbox">';
		html += '<input name="' + editableColumn +'" id="' + editableId +'" type="checkbox" class="custom-control-input"';
		if (editableHtml == "yes") {
			html += " checked";
		}
		html += ' />';
		html += '<label class="custom-control-label" for="' + editableId +'"></label>';
		html += '</div>';
		break;

	default:
		html += '<input name="' + editableColumn +'" id="' + editableId +'" type="text" class="form-control" value="';
	html += editableHtml;
	html += '" />';
	break;
	}

	html += '</div>';

	$(that).replaceWith(html);

	switch (editableType) {

	case "datetime":
		datetimepickerEvent();
		break;

	case "date":
		datepickerEvent();
		break;

	case "time":
		timepickerEvent();
		break;

	case "year":
		yearpickerEvent();
		break;
	}

	if (editableType == "date" || editableType == "time" || editableType == "datetime" || editableType == "year") {

		$('#' + editableId).datetimepicker("show");
	}

	if (editableType != "tinyint") {

		value = $('#' + editableId).val();
		$('#' + editableId).val('');
		$('#' + editableId).val(value);
		$('#' + editableId).focus();
	}
}

//Add a click listener on editable element
function editable() {

	$(".editable").click(function() {

		if (!editableId) {

			edit(this);
		}
	});
}

$(document).keyup(function(e) {

	if (e.key == "Escape") {

		reset();

	} else if (e.key == "Enter") {

		save();
	}
});

$(document).click(function(e) {

	if (editableId) {

		element = '#' + editableId;
		label = null;

		if (editableType == "tinyint") {

			label = 'label[for="' + $(element).labels().attr("for") + '"]';
		}

		if (!$(e.target).is(element) && !$(e.target).is(label)) {

			save();

			if ($(e.target).is(".editable")) {

				edit(e.target);
			}
		}
	}
});

editable();