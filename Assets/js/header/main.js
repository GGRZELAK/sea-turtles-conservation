function login(idLoginPopover, that) {

	$.ajax({

		type : "POST",
		data : $(that).serialize() + "&output=html",
		url : protocol + "api." + dns + "/login",
		timeout: 3000,
		xhrFields: {
			withCredentials: true 
		},
		success : function(response) {

			if (response["error"]) {

				$.growl.error({ message: response["error"] });

			} else if (response["success"]) {

				$(idLoginPopover).popover("dispose");

				$.growl.notice({ message: response["success"]["message"] });

				content = '<!-- Bouton Profil -->\
					<p id="profil">\
					<a role="button" data-toggle="popover-profil" data-placement="bottom" href="#">';

				if (response["success"]["picture"]) {

					content += '<img src="' + response["success"]["picture"] + '" alt="Photo de profil ' + response["success"]["firstName"] + ' ' + response["success"]["lastName"] + '" />';

				} else {

					content += '<i class="material-icons">account_circle</i>';
				}

				content += '<span>' + response["success"]["firstName"] + "<br />";
				content += (response["success"]["lastName"]) ? response["success"]["lastName"] : '(' + response["success"]["registration"] + ')';
				content += '</span></a></p>';

				$("#login").replaceWith(content);

				$("#headerTools").html(response["success"]["html"]);

				addPopEvent("popover-profil", profilEvent);

				if (typeof(reloadPrivateContent) == "function") {

					reloadPrivateContent();
				}
			}
		},
		error: function() {

			$.growl.error({ message: "Check your Internet connection." });
		}
	});
}

function logout(idProfilPopover, that) {

	$.ajax({

		type : "POST",
		url : protocol + "api." + dns + '/logout',
		timeout: 3000,
		xhrFields: {
			withCredentials: true 
		},
		success : function(response) {

			$(idProfilPopover).popover("dispose");

			$.growl.notice({ message: response["success"]["message"] });

			content = '<!-- Bouton Login -->\
				<p id="login">\
				<a role="button" data-toggle="popover-login" data-placement="bottom" href="#" class="btn btn-primary">\
				Sign in\
				</a>\
				</p>';

			$("#profil").replaceWith(content);

			$("#headerTools").html(response["success"]["html"]);

			addPopEvent("popover-login", loginEvent);

			if (typeof(reloadPrivateContent) == "function") {

				reloadPrivateContent();
			}
		},
		error: function() {

			$.growl.error({ message: "You are not logged out. Check your Internet connection." });
		}
	});
}

function forgot(that) {

	$('#forgotPassword button[type="submit"]').html('<span class="lds-ring"><span></span><span></span><span></span><span></span></span><strong>Envoi en cours...</strong>');

	$.ajax({

		type : 'POST',
		data : $(that).serialize(),
		url : protocol+"api." + dns + '/password/forgot',
		timeout: 20000,
		xhrFields: {
			withCredentials: true
		},
		beforeSend : function() {

			$('#forgotPassword input[type="email"]').attr("disabled", '');
			$('#forgotPassword button[type="submit"]').attr("disabled", '');
		},
		success : function(response) {

			if (response["error"]) {

				$.growl.error({ message: response["error"] });

				$('#forgotPassword input[type="email"]').removeAttr("disabled");
				$('#forgotPassword button[type="submit"]').removeAttr("disabled");
				$('#forgotPassword button[type="submit"]').html('Envoyer mon mot de passe');

			} else if (response["success"]) {

				$('#forgotPassword').modal("hide");

				$.growl.notice({ message: response["success"] });

				setTimeout(function() {

					$('#forgotPassword .modal-footer').remove();

					content = '<div class="alert alert-success" role="alert">';
					content += response["success"];
					content += '</div>';

					$('#forgotPassword .input-group').replaceWith(content);
				}, 1000);
			}
		},
		error: function() {

			$.growl.error({ message: "Votre mot de passe n'a pas été envoyé. Vérifiez votre connexion Internet." });

			$('#forgotPassword input[type="email"]').removeAttr("disabled");
			$('#forgotPassword button[type="submit"]').removeAttr("disabled");
			$('#forgotPassword button[type="submit"]').html('Envoyer mon mot de passe');

		}
	});
}

function scrollAction(that) {

	var scrollBar = $(that).scrollTop();

	if (scrollBar == 0) {

		$("header").css("background", "none");
		$("header").css("box-shadow", "none");

	} else {

		$("header").css("background", "white");
		$("header").css("box-shadow", "0px 1px 10px rgba(0, 0, 0, 0.5)");
	}
}

function updateProfil(that) {

	$.ajax({

		type : 'POST',
		data : $(that).serialize() + "&output=html",
		url : protocol+"api."+dns+'/user/update',
		timeout: 3000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {

			if (response["error"]) {

				$.growl.error({ message: response["error"] });

			} else if (response["success"]) {

				$('#updateProfil').modal("hide");

				if (response["success"]["html"]) {

					setTimeout(function() {

						$("#headerTools").html(response["success"]["html"]);

					}, 1000);
				}

				$.growl.notice({ message: response["success"]["message"] });
			}
		},
		error: function() {

			$.growl.error({ message: "Aucune modification n'a été apporté. Vérifiez votre connexion Internet." });

		}
	});
}
