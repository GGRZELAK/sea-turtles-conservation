function addPopEvent(id, callback = false) {
	
	if ($('[data-toggle="'+id+'"]').length) {
		
		var idPopover;

		var p = $('[data-toggle="'+id+'"]').on("click", function(e) {
			
			e.preventDefault();
			
		}).popover({

			html: true,
			sanitize: false,
			container: 'body',
			content: function() {
				
				popoverContent = $('#'+id).parent();
				el = popoverContent.detach();
				return popoverContent.html();
			}
		}).on("shown.bs.popover", function () {

			idPopover = '#'+$(this).data("bs.popover").tip.id;
			$(idPopover).css("z-index", "1000");

			if (callback) callback(idPopover, this);

		}).on("hide.bs.popover", function () {
			
			$("body").append(el);
		});

		$(document).click(function(event) {
			
			if (!$(event.target).closest(idPopover).length) {
				
				$(idPopover).popover("hide");
			}
		});
	}
}

function loginEvent(idLoginPopover, that) {
	
	$(idLoginPopover + ' input[type="email"]').focus();

	$(idLoginPopover + ' form').submit(function() {

		login(idLoginPopover, this);
		return false;
	});
	
	$('#forgotPassword').on('shown.bs.modal', function () {
		
		$('#forgotPassword input[type="email"]').focus();
		mail = $('#popover-login input[type="email"]').val();
		$('#forgotPassword input[type="email"]').val(mail);
	});
	
	tooltipEvent();

	$('#forgotPassword form').submit(function() {

		forgot(this);
		return false;
	});
}

function profilEvent(idProfilPopover, that) {
	
	//$(idProfilPopover).css("max-width", "max-content");
	$(idProfilPopover + " .popover-body").css("padding", 0);
	
	$('#logout').click(function() {

		logout(idProfilPopover, that);
	});
	
	$('#updateProfil form').submit(function() {

		updateProfil(this);
		return false;
	});
}

addPopEvent("popover-login", loginEvent);
addPopEvent("popover-profil", profilEvent);
addPopEvent("popover-navigation");

$(function() {

	$(window).scroll(function () {
		
		scrollAction(this);
	});
});
