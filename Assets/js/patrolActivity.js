var fieldIdAddUser;
var stationId;
var hatcheries;

function getHatcheries(selectedValue = null) {

	$.ajax({

		type : "POST",
		data : "stationId=" + stationId,
		url : protocol + "api." + dns + "/hatcheries" ,
		timeout: 5000,
		xhrFields: {
			withCredentials: true
		},
		success : function(response) {

			if (response["error"]) {

				$.growl.error({ message: response["error"] });

			} else if (response) {
				
				hatcheries = array_column(response, null, "id");
				
				html = '';
				for(hatchery in hatcheries) {
					
					html += '<option value="' + hatcheries[hatchery]["id"] + '">' + hatcheries[hatchery]["name"] + '</option>'
				}
				
				html += '<option value="relocated">Relocated</option>\
					<option vlaue="natural">Natural</option>';
				
				$("#destination").html(html);

				if (selectedValue) {

					$('#destination option:selected').removeAttr("selected");
					$('#destination option[value="'+selectedValue+'"]').attr("selected", '');
					
					$("#beachlocation").attr("max", hatcheries[selectedValue]["numberoflocations"]);
				}

				if ($("#laid").val() != "yes") {

					$('#destination').prepend('<option value="" selected></option>');
				}
			}
		},
		error: function() {

			$.growl.error({ message: "Check your Internet connection." });
		}
	});
}

function reloadPrivateContent() {
	
	location.reload();
}

$(function () {
	
	stationId = $("#station").val();
	
	if (stationId) {
		
		getHatcheries();
	}

	// What field call the modal #adduser
	$('#adduser').on('show.bs.modal', function (event) {
		
		var button = $(event.relatedTarget);
		fieldIdAddUser = button.data('id');
		
	});

	// Update field when station changed
	$('#station').on('change', function() {

		stationId = this.value;
		
		getHatcheries();
		
		$('#stationid option:selected').removeAttr("selected");
		$('#stationid option[value="'+stationId+'"]').attr("selected", '');
		
	});

	$('#destination').on('change', function() {
		
		switch (this.value) {
		
			case "relocated":
				
				$('label[for="beachplace"] strong').remove();
				$('select[name="beachplace"]').removeAttr("required");
				$("#beachplace option:selected").removeAttr('selected');
				$('#beachplace').prepend('<option value="" selected></option>');

				$('label[for="code"]').append(' <strong class="text-danger">*</strong>');
				$('input[name="code"]').attr("required", '');
				
				break;
				
			case "natural":
				
				$('label[for="beachplace"]').append(' <strong class="text-danger">*</strong>');
				$('select[name="beachplace"]').attr("required", '');
				$('#beachplace option[value=""]').remove();
				$("#beachplace option:selected").removeAttr('selected');
				$('#beachplace option[value="berm"]').attr("selected", '');

				$('label[for="code"] strong').remove();
				$('input[name="code"]').removeAttr("required");
				$('input[name="code"]').val('');
				
				break;
				
			default:
				
				$('label[for="beachplace"]').append(' <strong class="text-danger">*</strong>');
				$('select[name="beachplace"]').attr("required", '');
				$('#beachplace option[value=""]').remove();
				$("#beachplace option:selected").removeAttr('selected');
				$('#beachplace option[value="berm"]').attr("selected", '');

				$('label[for="code"]').append(' <strong class="text-danger">*</strong>');
				$('input[name="code"]').attr("required", '');
				
				$("#beachlocation").attr("max", hatcheries[this.value]["numberoflocations"]);
				
				break;
		}
	});

	$('#laid').on('change', function() {
		
		if (this.value == "yes") {
			
			$('label[for="nestdate"]').append(' <strong class="text-danger">*</strong>');
			$('input[name="nestdate"]').attr("required", '');
			
			$('label[for="beachplace"]').append(' <strong class="text-danger">*</strong>');
			$('select[name="beachplace"]').attr("required", '');
			$('#beachplace option[value=""]').remove();
			$("#beachplace option:selected").removeAttr('selected');
			$('#beachplace option[value="berm"]').attr("selected", '');
			
			$('label[for="destination"]').append(' <strong class="text-danger">*</strong>');
			$('select[name="destination"]').attr("required", '');
			$('#destination option[value=""]').remove();
			$("#destination option:selected").removeAttr('selected');
			$("#destination option:first-child").attr("selected", '');
				
			$('label[for="code"]').append(' <strong class="text-danger">*</strong>');
			$('input[name="code"]').attr("required", '');
			
			$('label[for="builder"]').append(' <strong class="text-danger">*</strong>');
			$('select[name="builder"]').attr("required", '');
			$('#builder option[value=""]').remove();
			$("#builder option:selected").removeAttr('selected');
			$('#builder option[value="1"]').attr("selected", '');
			
			$('label[for="eggs"]').append(' <strong class="text-danger">*</strong>');
			$('input[name="eggs"]').attr("required", '');

		} else {
			
			$('label[for="nestdate"] strong').remove();
			$('input[name="nestdate"]').removeAttr("required");
			$('input[name="nestdate"]').val('');
			
			$('label[for="beachplace"] strong').remove();
			$('select[name="beachplace"]').removeAttr("required");
			$("#beachplace option:selected").removeAttr('selected');
			$('#beachplace').prepend('<option value="" selected></option>');
			
			$('label[for="destination"] strong').remove();
			$('select[name="destination"]').removeAttr("required");
			$("#destination option:selected").removeAttr('selected');
			$('#destination').prepend('<option value="" selected></option>');
			
			$('label[for="code"] strong').remove();
			$('input[name="code"]').removeAttr("required");
			$('input[name="code"]').val('');
			
			$('label[for="builder"] strong').remove();
			$('select[name="builder"]').removeAttr("required");
			$("#builder option:selected").removeAttr('selected');
			$('#builder').prepend('<option value="" selected>Nobody</option>');
			
			$('label[for="eggs"] strong').remove();
			$('input[name="eggs"]').removeAttr("required");
			$('input[name="eggs"]').val('');

		}
	});
	

	$('#patrol').submit(function() {
		
		$.ajax({

			type : "POST",
			data : $(this).serialize(),
			url : protocol + "api." + dns + "/set/activity/patrol",
			timeout: 5000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["error"]) {
				
					$.growl.error({ message: response["error"] });
				
				} else {
					
					if (response["warning"]) {

						$.growl.warning({ message: response["warning"] });
					}
						
					if (response["success"]["upd"]) {
						
						$.growl.notice({ message: response["success"]["upd"] });
				
					} else if (response["success"]["set"]) {
						
						$.growl.notice({ message: response["success"]["set"] });
						
						setTimeout(function() {
							
							$(location).attr("href", '/' + language + "/activities/patrol");
							
						}, 3000);
					}
				}

				return false;
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});

});