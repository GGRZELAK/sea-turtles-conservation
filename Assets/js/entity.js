function reloadPrivateContent() {

	location.reload();
}

$(function () {
	$("#selectAll").click(function () {
		$('input:checkbox').not(this).prop('checked', this.checked);
	});
	
	$(".delete").click(function () {
		
		entity = $(this).attr("data-entity");
		pk = $(this).attr("data-pk");
		
		$.ajax({
			
			type : "POST",
			url : protocol + "api." + dns + "/delete/" + entity + '/' + pk,
			timeout: 3000,
			xhrFields: {
				withCredentials: true
			},
			success : function(response) {

				if (response["success"]) {

					$.growl.notice({ message: response["success"] });
					
					$("#pk" + pk).remove();
					
					return false;

				} else if (response["error"]) {

					$.growl.error({ message: response["error"] });
					
					return false;
				}
			},
			error: function() {

				$.growl.error({ message: "Check your Internet connection." });
				
				return false;
			}
		});
		
		return false;
	});
});
