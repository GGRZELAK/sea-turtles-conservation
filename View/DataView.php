<?php
namespace View;

use Framework\View;
use Template\HeaderTemplate;
use Framework\App;

class DataView extends View
{

    public function __construct()
    {
        parent::__construct("Data manipulation");
    }
    
    public function import($stations, $message = null)
    {
        $app = new App();
        ?>
<style>
thead td {
	white-space: nowrap;
	padding: 0 5px;
}
</style>
<?php
        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();
        ?>
<div class="container">
	<?php
	echo '<p><a href="/', $app->language, '/activities/patrol" class="btn btn-light"><i class="material-icons">keyboard_arrow_left</i> Back</a></p>';
	?>
	<h1>Import data</h1>
	<div class="alert alert-info" role="alert">
		<p>Please select a .xlsx file with only one sheet. (Datos de Campos)</p>
		<hr>
		<div class="mb-0">
			Use this table header :
			<div class="overflow-auto">
				<table>
					<thead>
						<tr>
							<td>Fecha</td>
							<td>Enc</td>
							<td>Lider</td>
							<td>I. P.</td>
							<td>F. P.</td>
							<td>Siembra</td>
							<td>Moj</td>
							<td>Marea</td>
							<td>Etapa</td>
							<td>Espc</td>
							<td>Anid</td>
							<td>Dest</td>
							<td>Cod</td>
							<td>Z Ps</td>
							<td>T. Hu</td>
							<td>Izq</td>
							<td>Evidencia</td>
							<td>Der</td>
							<td>Evidencia</td>
							<td>Prom LCC</td>
							<td>Prom ACC</td>
							<td>Ancho Rastro</td>
							<td>Observaciones</td>
						</tr>
					</thead>
					<tbody class="d-none">
						<tr>
							<td>Meeting date</td>
							<td>Meeting time</td>
							<td>[Firstanme]</td>
							<td>[Start laying]</td>
							<td>[End laying]</td>
							<td>Nest time</td>
							<td>Beach location</td>
							<td>[Tide]</td>
							<td>Step</td>
							<td>Species</td>
							<td>Laid</td>
							<td>Destination</td>
							<td>[Nest code]</td>
							<td>[Height on the beach]</td>
							<td>Number of eggs</td>
							<td>[Left tag]</td>
							<td>[Left hurt]</td>
							<td>[Right tag]</td>
							<td>[Right hurt]</td>
							<td>[Length]</td>
							<td>[Width]</td>
							<td>[Width of return trace]</td>
							<td>[Comments]</td>
						</tr>
					</tbody>
					<tfoot class="d-none">
						<tr>
							<td>date</td>
							<td>time</td>
							<td>string(50)</td>
							<td>time</td>
							<td>time</td>
							<td>time</td>
							<td>int</td>
							<td>set(1/2 ↑, ↑, 1/2↓, ↓)</td>
							<td>int</td>
							<td>set(L.o., E.i., D.c., C.m.)</td>
							<td>set(Si, No, N/S)</td>
							<td>set(Vivero, Vivero2, Vivero[...], Reubicado, Natural)</td>
							<td>string(4)</td>
							<td>set(Veg, Berma, Abierto)</td>
							<td>int</td>
							<td>string(25)</td>
							<td>set(Cicatriz, Desgarre, Hueco)</td>
							<td>string(25)</td>
							<td>set(Cicatriz, Desgarre, Hueco)</td>
							<td>float</td>
							<td>float</td>
							<td>float</td>
							<td>text</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<p>If Lider is not specified, then Ricardo will be selected.</p>
		</div>
	</div>
	
	<form method="post" enctype="multipart/form-data">
	<?php
        if (! empty($message["error"])) {

            echo '<div class="alert alert-danger" role="alert">', $message["error"], '</div>';
        }
        
        if (! empty($message["success"])) {
            
            echo '<div class="alert alert-success" role="alert">', $message["success"], '</div>';
        }
        
        if (! empty($message["warning"])) {
            
            foreach ($message["warning"] as $warning) {
            
                echo '<div class="alert alert-warning" role="alert">', $warning, '</div>';
            
            }
        }
        
        echo '<div class="input-group mb-3">
            <select name="station" id="station" class="custom-select" required>';
            
                foreach($stations as $station) {
                    
                    echo '<option value ="' , $station["id"], '"';
                    if ($station["id"] == 1) echo ' selected';
                    echo '>', $station["name"], '</option>';
                }
            
            echo '</select>
            <div class="input-group-append">
                <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#addstation" disabled>Add station</button>
            </div>
        </div>';
        ?>
		<p><input type="file" name="file"></p>
		<p><button type="submit" class="btn btn-secondary">Import</button></p>
	
	</form>
</div>
<?php
    }
}

