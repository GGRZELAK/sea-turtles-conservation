<?php
namespace View;

use Framework\View;
use Template\HeaderTemplate;
use Framework\Head;
use Template\FooterTemplate;

class EntityView extends View
{

    private function alert($message, $type = "danger")
    {
        echo '<div class="alert alert-', $type, ' alert-dismissible auto-dismiss fade show" role="alert">', $message, '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>';
    }

    public function __construct($title, $language, $table, $struct, $rows, $responses = [])
    {
        $this->head = new Head();

        if (! empty($_SESSION["userId"])) {
            $this->head->setScript("entity.js");
            $this->head->setScript("editable.js");
        }

        $this->head->setStyle("entity.css", false);

        parent::__construct($title);

        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();

        $cellId = 1;
        $rowId = 1;

        echo '<div class="container-fluid">
        <h1 class="text-center">', $title, '</h1>';

        if (! empty($responses)) {
            
            echo '<div class="alert-container">';

            foreach ($responses as $type => $messages) {

                $type = ($type == "error") ? "danger" : "success";

                foreach ($messages as $message) {
                    
                    $this->alert($message, $type);
                }
            }
            echo "</div>";
        }

        if (! empty($_SESSION["userId"])) {
            echo '<form action="" method="post">
            <div>
                <img src="/', $this->app->asset, $this->app->image, 'arrow-rtb.svg" alt="v" height="22px" class="px-3" />
                <div class="custom-control custom-checkbox selectAllInput">
                    <input type="checkbox" class="custom-control-input" id="selectAll">
                    <label class="custom-control-label" for="selectAll">Select all</label>
                </div>
                <div class="selectAllSubmit">
                    <button type="submit" class="btn btn-sm btn-danger">
                    <i class="material-icons">delete_forever</i> Delete selection
                    </button>
                </div>
            </div>';
        }
        echo '<table>
           <thead class="font-weight-bold">';

        echo '<tr>';
        if (! empty($_SESSION["userId"])) {
            echo '<td class="px-3 py-2"></td>';
        }
        foreach (array_keys($struct) as $field) {
            echo '<td class="px-3 py-2">
            <a href="#" id="', $field, '">
            <img src="/', $this->app->asset, $this->app->image, 'up-and-down.svg" alt="orderBy" height="16px" />', $field, '</a></td>';
        }
        if (! empty($_SESSION["userId"])) {
            echo '<td class="px-3 py-2"></td>';
        }
        echo '</tr>
        </thead>
        <tbody>';

        foreach ($rows as $row) {
            echo '<tr class="even" id="pk', $row["id"], '">';
            if (! empty($_SESSION["userId"])) {
                echo '<td class="px-3 py-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="row', $rowId, '" name="', $row["id"], '">
                        <label class="custom-control-label" for="row', $rowId, '"></label>
                    </div>
                </td>';
            }

            foreach ($row as $key => $cell) {
                echo '<td class="px-3 py-2">
                    <span class="';
                if (! empty($_SESSION["userId"])) {
                    echo "editable";
                }
                echo '" id="', $cellId, '"
                    data-post="', $this->app->protocol, "api.", $this->app->dns, '/update/', $table, '/', $row["id"], '"
                    data-name="', $key, '" ';
                if (! empty($struct[$key]["set"])) {

                    echo 'data-source="', $struct[$key]["set"], '" ';
                }

                if (! empty($struct[$key]["REFERENCED_TABLE_NAME"])) {

                    echo 'data-get="', $this->app->protocol, "api.", $this->app->dns, '/', $struct[$key]["REFERENCED_TABLE_NAME"], '"
                            data-type="select" ';
                } else {

                    echo 'data-type="', $struct[$key]["Type"], '" ';
                }
                echo '>';

                switch ($struct[$key]["Type"]) {
                    case "datetime":
                        if (! empty($cell)) {

                            echo date("d/m/Y H:i", strtotime($cell));
                        } else {

                            echo "null";
                        }
                        break;

                    case "date":
                        if (! empty($cell)) {

                            echo date("d/m/Y", strtotime($cell));
                        } else {

                            echo "null";
                        }
                        break;

                    case "time":
                        if (! empty($cell)) {

                            echo date("H:i", strtotime($cell));
                        } else {

                            echo "null";
                        }
                        break;

                    case "year":
                        if (! empty($cell)) {

                            echo date("Y", strtotime($cell));
                        } else {

                            echo "null";
                        }
                        break;

                    case "tinyint":
                        if (! empty($cell)) {

                            echo 'yes';
                        } else {
                            echo 'no';
                        }
                        break;

                    default:
                        if (! empty($cell)) {

                            echo $cell;
                        } else {

                            echo "null";
                        }
                        break;
                }

                echo '</span>';

                $cellId ++;
            }

            if (! empty($_SESSION["userId"])) {
                echo '</td>
                    <td class="px-3 py-2">
                        <button class="btn btn-sm btn-danger delete" data-entity="', $table, '" data-pk="', $row["id"], '">
                        <i class="material-icons">delete_forever</i>
                        </button>
                    </td>
                </tr>';
            }

            $rowId ++;
        }

        echo '</tbody>
        </table>
        </form>
        </div>';

        new FooterTemplate();
    }
}

