<?php
namespace View;

use Framework\View;
use Template\HeaderTemplate;

class NotFoundView extends View
{

    public function __construct()
    {
        parent::__construct("Erreur 404");
        
        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();

        echo '<div class="container"><h1>Erreur 404</h1></div>';
    }
}
