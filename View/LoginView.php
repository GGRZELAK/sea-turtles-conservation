<?php
namespace View;

use Framework\View;
use Framework\Head;
use Template\HeaderTemplate;

class LoginView extends View
{

    public function __construct($response)
    {
        $this->head = new Head();
        parent::__construct("Sign in");

        ?>
        <style>
        .container-popover-login {
        	display: block;
        }
        
        #popover-login {
        	pointer-events: auto;
        	background: rgba(255, 255, 255, 0.5);
        	padding: 20px 40px;
        	max-width: 500px;
        	margin: auto;
        }
        </style>
        <?php

        $headerTemplate = new HeaderTemplate();
        echo '<header>',
        $headerTemplate->logo(),
        $headerTemplate->navigation(),
        $headerTemplate->navigationTools(),
        '</header>';

        echo '<div class="container">';
        
        if (! empty($response["success"])) {
            
            echo '<p class="alert alert-success" role="alert">',
            $response["success"],
            '</p>';
        }
        
        if (! empty($response["error"])) {
            
            echo '<p class="alert alert-danger" role="alert">',
            $response["error"],
            '</p>';
        }
        
        echo $headerTemplate->loginTools(),
        '</div>';
    }
}

