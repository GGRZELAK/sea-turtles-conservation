<?php
namespace View;

use Framework\Head;
use Framework\View;
use Template\HeaderTemplate;
use Template\RegisterTemplate;
use Framework\App;

class RegisterView extends View
{

    public function __construct($response)
    {
        $this->head = new Head();
        parent::__construct("Sign up");
        
        ?>
        <style>
        .modal-dialog {
        	pointer-events: auto;
        	background: rgba(255, 255, 255, 0.5);
        	padding: 20px 40px;
        }
        </style>
        <?php

        $headerTemplate = new HeaderTemplate();
        echo '<header>', $headerTemplate->logo(), '</header>';
        ?>
        <div class="container">
        <?php
        
        if (! empty($response["success"]["message"])) {
            
            echo '<p class="alert alert-success" role="alert">',
            $response["success"]["message"],
            '</p>';
        }
        
        if (! empty($response["error"])) {
            
            echo '<p class="alert alert-danger" role="alert">',
            $response["error"],
            '</p>';
        }
        ?>
        	<form action="" method="post" class="modal-dialog">
        	   <!-- Registration form -->
        		<h2>Sign up</h2>
    			<?php
    			$registerTemplate = new RegisterTemplate();
    			echo $registerTemplate->registerTools();
    			$app = new App();
    			?>
                <p class="text-center">
                	<button type="submit" class="btn btn-primary text-center">Register</button>
                </p>
                <div class="dropdown-divider"></div>
                <p class="clearfix">
                <?php echo '<a class="btn btn-sm btn-light float-right" href="' . $app->protocol . $app->dns . '/' . $GLOBALS["language"] . '">Sign in</a>'; ?>
                </p>
        	</form>
        </div>
        <?php
    }
}

