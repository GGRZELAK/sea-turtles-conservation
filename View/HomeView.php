<?php

namespace View;

use Framework\View;
use Template\HeaderTemplate;
use Template\FooterTemplate;
use Template\ModalTemplate;
use Framework\Head;

class HomeView extends View
{

    public function __construct($users, $leaders, $stations, $language, $meetingTurtlesStat, $lastActivities, $nests, $lastTurtles)
    {
        $this->head = new Head();

        $this->head->setScript("location.js");
        $this->head->setScript("station.js");
        $this->head->setScript("hatchery.js");
        $this->head->setScript("user.js");
        $this->head->setScript("turtle.js");
        $this->head->setExternalScript("https://www.gstatic.com/charts/loader.js");

        $conf = (file_exists("Framework/prod.ini")) ? parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

        parent::__construct($conf["APP"]["name"]);

        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();

        echo '<div class="container">
            <h1 class="mb-3">', $conf["APP"]["name"], '</h1>

            <p class="lead mb-4">
                Our job, the protection of sea turtles! This website provides tools, data and news about the sea turtle population.
                Enjoy your visit!
            </p>

            <p class="mb-4">
                <a href="http://www.asvocr.org/" class="btn btn-primary">Help us</a>
                <a href="mailto:info@asvocr.org" class="btn btn-warning">Contact us</a>
            </p>

            <h2>Manage data</h2>

            <div class="mb-2">

                                <div class="btn-group mb-2">
                                    <a class="btn btn-secondary" href="/', $language, '/activities/patrol">Patrol activities</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addPatrolActivty" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="addPatrolActivty">
                                            <a href="/', $language, '/activity/patrol" class="dropdown-item">Add patrol activity</a>
                                        </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                    <a class="btn btn-secondary disabled" href="/', $language, '/activities/birth">Birth activities</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addBirthActivty" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="addBirthActivty">
                                        <a href="/', $language, '/activity/birth" class="dropdown-item disabled">Add birth activity</a>
                                    </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                    <a class="btn btn-secondary disabled" href="/', $language, '/activities/exhumation">Exhumation activities</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addExhumationActivty" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="addExhumationActivty">
                                        <a href="/', $language, '/activity/exhumation" class="dropdown-item disabled">Add exhumation activity</a>
                                    </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                    <a class="btn btn-secondary disabled" href="/', $language, '/activities/public">Public activities</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addPublicActivty" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="addPublicActivty">
                                        <a href="/', $language, '/activity/public" class="dropdown-item disabled">Add public activity</a>
                                    </div>';
        }
        echo '</div>
            </div>

            <div class="mb-3">
            
                            <div class="btn-group mb-2">
                                <a class="btn btn-secondary" href="/', $language, '/entity/turtles">Turtles</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addTurtle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="addTurtle">
                                    <a href="#" class="dropdown-item disabled">Add turtle</a>
                                </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                <a class="btn btn-secondary" href="/', $language, '/entity/species">Species</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addSpecies" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="addSpecies">
                                    <a href="#" class="dropdown-item" type="button" data-toggle="modal" data-target="#addspecies">Add species</a>
                                </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                <a class="btn btn-secondary" href="/', $language, '/entity/stations">Stations</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addStation" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="addStation">
                                    <a href="#" class="dropdown-item" type="button" data-toggle="modal" data-target="#addstation">Add station</a>
                                </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                                <a class="btn btn-secondary" href="/', $language, '/entity/hatcheries">Hatcheries</a>';
        if (!empty($_SESSION["userId"])) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addHatchery" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="addHatchery">
                                    <a href="#" class="dropdown-item" type="button" data-toggle="modal" data-target="#addhatchery">Add hatchery</a>
                                </div>';
        }
        echo '</div>
        
        <div class="btn-group mb-2">
                        <a class="btn btn-secondary" href="/', $language, '/entity/users">Users</a>';
        if (!empty($_SESSION["userId"]) && in_array($_SESSION["userId"], array_keys($leaders))) {
            echo '<button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" id="addUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="addUser">
                            <a href="#" class="dropdown-item" type="button" data-toggle="modal" data-id="#leader" data-target="#adduser">Add user</a>
                        </div>';
        }
        echo '</div>
        </div>

            <div class="row">
                <div class="col-sm">
                    <h2>Statistics</h2>
                    <div class="row p-3 mb-3" style="background: rgba(255,255,255,0.5);">
                    <div class="col p-3" style="background: rgba(255,255,255,0.5);">
                    <div id="chart_div1"></div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm">
                    <h2>Last activities</h2>';
        foreach ($lastActivities as $lastActivity) {
            if (!empty($lastActivity["nestid"])) {
                echo '<p>
                <a href="/', $language, '/activity/patrol?id=', $lastActivity["id"], '">',
                $nests[$lastActivity["nestid"]]["eggs"], " eggs collected at ",
                $stations[$lastActivity["stationid"]]["name"], " the ",
                date("d/m/Y \a\\t H:i", strtotime($lastActivity["date"])),
                '</a>
                </p>';
            }
        }
        echo '<p><a href="/', $language, '/activities/patrol">More...</a></p>';
        echo '</div>

                <div class="col-sm">
                    <h2>Last tagged turtles</h2>';
        foreach ($lastTurtles as $lastTurtle) {
            echo '<p><a href="/', $language, '/turtle/', $lastTurtle["id"], '">',
            $lastTurtle["englishname"], ' ';
            if (!empty($lastTurtle["lefttag"])) {

                echo '<span class="badge badge-dark" data-toggle="tooltip" title="Left side">',
                $lastTurtle["lefttag"],
                '</span>';
            } else {
                echo '<span class="badge badge-danger" data-toggle="tooltip" title="Left side">no tag</span>';
            }
            echo ' - ';
            if (!empty($lastTurtle["righttag"])) {

                echo '<span class="badge badge-dark" data-toggle="tooltip" title="Right side">',
                $lastTurtle["righttag"],
                '</span>';
            } else {
                echo '<span class="badge badge-danger" data-toggle="tooltip" title="Right side">no tag</span>';
            }
            echo " referenced the ", date("d/m/Y \a\\t H:i", strtotime($lastTurtle["referencingdate"])),
            '</a></p>';
        }
        echo '<p><a href="/', $language, '/entity/turtles">More...</a></p>';
        echo '</div>

            </div>

        </div>
        
        <!-- Modals -->';
        if (!empty($_SESSION["userId"])) {

            $modalTemplate = new ModalTemplate();
            $modalTemplate->generateModals($users, $stations);
        }
        ?>
        <script>
            function reloadPrivateContent() {

                location.reload();
            }

            google.charts.load('current', {'packages': ['bar']});
            google.charts.setOnLoadCallback(function () {
                var data = google.visualization.arrayToDataTable(
                    <?php echo json_encode($meetingTurtlesStat); ?>
                );

                var options = {
                    chart: {
                        title: 'Meeting turtles',
                        backgroundColor: {fill: 'transparent'}
                    },
                    bars: 'vertical'
                };

                var chart = new google.charts.Bar(document.getElementById('chart_div1'));
                chart.draw(data, google.charts.Bar.convertOptions(options));
            });
        </script>
        <?php
        new FooterTemplate();
    }
}
