<?php
namespace View;

use Framework\View;
use Framework\Head;
use Template\HeaderTemplate;
use Template\FooterTemplate;

class TurtleView extends View
{

    public function __construct($turtle, $activities, $stations, $users, $hatcheries, $nests, $language, $dic)
    {
        $this->head = new Head();
        $this->head->setStyle("activities.css");
        
        parent::__construct(ucfirst($dic["turtle"][$language]) . ' ' . $turtle["lefttag"] . ' - ' . $turtle["righttag"]);
        
        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();
        
        echo '<div class="container">';
        
            echo '<h1>',
                ucfirst($dic["turtle"][$language]) . ' <small>' . $turtle["id"], '</small>',
            '</h1>
            <p class="h4">
                Tags&nbsp;: ';
        		if (!empty($turtle["lefttag"])) {
        
                    echo '<span class="badge badge-dark" data-toggle="tooltip" title="Left side">',
                        $turtle["lefttag"],
                    '</span>';
        		} else {
        		    echo '<span class="badge badge-danger" data-toggle="tooltip" title="Left side">no tag</span>';
        		}
                echo ' - ';
                if (!empty($turtle["righttag"])) {
                    
                    echo '<span class="badge badge-dark" data-toggle="tooltip" title="Right side">',
                        $turtle["righttag"],
                    '</span>';
                } else {
                    echo '<span class="badge badge-danger" data-toggle="tooltip" title="Right side">no tag</span>';
                }
            echo '</p>';
            
            echo '<div class="row">

                <p class="col-sm">
                    Species&nbsp;:
                    <a href="https://', $language, '.wikipedia.org/wiki/', $turtle["englishname"], '">
                        <strong data-toggle="tooltip" title="Wikipedia">',
                            $turtle["englishname"],
                        '</strong>
                    </a>
                </p>

                <p class="col-sm">';

                    if (!empty($turtle["length"])) {
                        
                        echo 'Length&nbsp;: <strong>', $turtle["length"], ' cm</strong><br />';
                    }
                    if (!empty($turtle["width"])) {
                        
                        echo 'Width&nbsp;: <strong>', $turtle["width"], ' cm</strong>';
                    }
                echo '</p>

                <p>';
                	if (!empty($turtle["lefthurt"])) {
                	    
                	    echo '<strong class="text-capitalize">', $turtle["lefthurt"], '</strong> on the left side<br />';
                	}
                	if (!empty($turtle["righthurt"])) {
                	    
                		echo '<strong class="text-capitalize">', $turtle["righthurt"], '</strong> on the right side';
                	}
                echo '</p>
            </div>

            <h3>All activities for this turtle</h3>';
                
            foreach ($activities as $activity) {
                
                echo '<div class="card mb-3">
                    <div class="card-header">
                    	<h2>
                        	<a href="/', $language, '/patrol/activity?id=', $activity["id"], '" data-toggle="tooltip" title="Edit this activity">
                        		Activity<small> ', $activity["id"],'</small>
                        	</a>
                    	</h2>
                        <div class="row">
                        	<p class="col-sm m-0">Leader&nbsp;:
                                <span class="text-muted">',
                                $users[$activity["leader"]]["firstname"], ' ',
                                $users[$activity["leader"]]["lastname"], ' ',
                                '(', $users[$activity["leader"]]["registration"], ')',
                                '</span>
                            </p>
                        	<p class="col-sm m-0 text-right">
                                Station&nbsp;:
                                <strong data-toggle="tooltip" title="', $stations[$activity["stationid"]]["address"], '">',
                                $stations[$activity["stationid"]]["name"],
                                '</strong>
                            </p>
                    	</div>
                    </div>
                    <div class="card-body">
                    	<div class="row">

                            <div class="col-sm">
                                    
                            	<p>Step&nbsp;: <strong class="step">', $activity["step"], '/8</strong></p>';
                                
                                echo '<p>';
                                if (!empty($activity["startlaying"])) {
                                    
                                    echo 'Start laying&nbsp;: <strong>', date("G:i", strtotime($activity["startlaying"])), '</strong><br />';
                                }
                                if (!empty($activity["endlaying"])) {
                                    
                                    echo 'End laying&nbsp;: <strong>', date("G:i", strtotime($activity["endlaying"])), '</strong>';
                                }
                                echo '</p>';
                                
                                echo '<p>';
                                if (!empty($activity["beachlocation"])) {
                                    
                                    echo 'Beach location&nbsp;:
                                	    <strong>', $activity["beachlocation"];
                                    if (!empty($activity["hatcheryid"])) {
                                        
                                        echo '/', $hatcheries[$activity["hatcheryid"]]["numberoflocations"];
                                    }
                                    echo '</strong><br />';
                                }
                                if (!empty($activity["beachplace"])) {
                                    
                                    echo 'Height on the beach&nbsp;: <strong class="text-capitalize">', $activity["beachplace"], '</strong>';
                                }
                                echo '</p>';
                                
                                if(!empty($activity["oldtide"])) {
                                    
                                    echo '<p>Estimated tide&nbsp;: <strong class="text-capitalize">', $activity["oldtide"], '</strong></p>';
                                }
                                
                                echo '<p>';
                                if (!empty($activity["fire"])) {
                                    
                                    echo '<strong>Campfire</strong> near the turtle.<br />';
                                }
                                if (!empty($activity["residues"])) {
                                    
                                    echo '<strong>Residues</strong> near the nest.';
                                }
                                echo '</p>
                                
                            </div>
                                
                            <div class="col-sm">';
                                
                                if (!empty($activity["destination"])) {
                                    
                                    echo '<p>
                                        Destination&nbsp;: ';
                                    if(!empty($activity["hatcheryid"]) && $activity["destination"] == "hatchery") {
                                        echo '<strong data-toggle="tooltip" title="Lat: ', $hatcheries[$activity["hatcheryid"]]["latgps"], ' Lon: ', $hatcheries[$activity["hatcheryid"]]["longps"], '">';
                                        echo $hatcheries[$activity["hatcheryid"]]["name"];
                                        echo '</strong>';
                                    } else {
                                        echo '<strong class="text-capitalize">', $activity["destination"], '</strong>';
                                    }
                                    echo '</p>';
                                }
                                
                                if (!empty($activity["nestid"])) {
                                    
                                    echo '<p>';
                                    if (!empty($nests[$activity["nestid"]]["hatcheryleader"])) {
                                        
                                        $hatcheryleader = $users[$nests[$activity["nestid"]]["hatcheryleader"]];
                                        echo 'Hatchery leader&nbsp;: <strong>',
                                        $hatcheryleader["firstname"], ' ',
                                        $hatcheryleader["lastname"], ' ',
                                        '(', $hatcheryleader["registration"], ')',
                                        '</strong><br />';
                                    }
                                    if (!empty($nests[$activity["nestid"]]["builder"])) {
                                        
                                        $builder = $users[$nests[$activity["nestid"]]["builder"]];
                                        echo 'Nest builder&nbsp;: <strong>',
                                        $builder["firstname"], ' ',
                                        $builder["lastname"], ' ',
                                        '(', $builder["registration"], ')',
                                        '</strong>';
                                    }
                                    echo '</p>';
                                    
                                    if (!empty($nests[$activity["nestid"]]["code"])) {
                                        echo '<p>
                                            Nest code&nbsp;:
                                            <strong data-toggle="tooltip" title="Location in the hatchery">',
                                            $nests[$activity["nestid"]]["code"],
                                            '</strong>
                                		</p>';
                                    }
                                    
                                    if (!empty($nests[$activity["nestid"]]["buildingdate"])) {
                                        echo '<p>
                                    		Nest time&nbsp;:
                                            <strong>',
                                            date("G:i", strtotime($nests[$activity["nestid"]]["buildingdate"])),
                                            '</strong>
                                		</p>';
                                    }
                                    
                                    if (!empty($nests[$activity["nestid"]]["eggs"])) {
                                        echo '<div class="eggs" data-toggle="tooltip" title="Number of collected eggs">
                                		<p>
                                            <strong>',
                                            $nests[$activity["nestid"]]["eggs"],
                                            '</strong>
                                            <span>eggs</span>
                                        </p>
                                	</div>';
                                    }
                                }
                            echo '</div>
                        </div>
                    </div>
                	<p class="card-footer m-0">Meeting date&nbsp;: <span class="text-muted">', date("d/m/Y à G:i", strtotime($activity["date"])), '</span></p>
                </div>';
            }
        
        echo '</div>';
        
        new FooterTemplate();
    }
}

