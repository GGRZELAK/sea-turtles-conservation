<?php
namespace View\Activity;

use Template\HeaderTemplate;
use Framework\Head;
use Framework\View;
use Template\FooterTemplate;

class GetPatrolView extends View
{

    public function __construct($activities, $seasons, $year, $stations, $users, $turtles, $hatcheries, $nests, $language)
    {
        $this->head = new Head();
        $this->head->setStyle("activities.css");
        
        parent::__construct("Patrol activities");
        
        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();
        
        ?>
        
        <div class="container">
            <h1>Patrol activities</h1>
            <p class="lead">
                Some organizations are conducting scientific surveys to accurately establish the sea turtle population.
                Sometimes they can directly collecting the eggs and putting them in a safe place.
                This permit to increases the success rate of births and therefore increases the population.
            </p>
            <div class="row">
                <div class="col-md-auto">
                    <nav aria-label="Years">
                        <ul class="pagination">
                            <?php
                            foreach ($seasons as $season => $count) {
                                echo '<li class="page-item';
                                if ($season == $year) {
                                    echo " active";
                                }
                                echo '">
                                    <a class="page-link" title="', $count, ' activities" href="/', $language, '/activities/patrol/', $season, '">',
                                        $season,
                                    '</a>
                                </li>';
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
                <div class="col-md text-right mb-3">
                    <div class="btn-group" role="group" aria-label="Export/Import data">
                        <?php
                        echo '<a hre="/', $language, '/export" class="btn btn-secondary disabled"><i class="material-icons">cloud_download</i> Export to Excel file</a>';
                        if (!empty($_SESSION["userId"])) {
                        
                            echo '<a href="/import" class="btn btn-secondary"><i class="material-icons">backup</i> Import from Excel file</a>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            
            <div id="activities">
                <?php
                foreach ($activities as $activity) {
                    
                    echo '<div class="card mb-3">
                        <div class="card-header">
                        	<h2>
                            	<a href="/', $language, '/activity/patrol?id=', $activity["id"], '" data-toggle="tooltip" title="More data about this activity">
                            		Activity<small> ', $activity["id"],'</small>
                            	</a>
                        	</h2>
                            <div class="row">
                            	<p class="col-sm m-0">Leader&nbsp;:
                                    <span class="text-muted">',
                            	       $users[$activity["leader"]]["firstname"], ' ',
                            	       $users[$activity["leader"]]["lastname"], ' ',
                            	       '(', $users[$activity["leader"]]["registration"], ')',
                            	   '</span>
                                </p>
                            	<p class="col-sm m-0 text-right">
                                    Station&nbsp;:
                                    <strong data-toggle="tooltip" title="', $stations[$activity["stationid"]]["address"], '">',
                                        $stations[$activity["stationid"]]["name"],
                                    '</strong>
                                </p>
                        	</div>
                        </div>
                        <div class="card-body">
                        	<div class="row">
                                <div class="col-sm">';
                                    
                                    if (!empty($activity["turtleid"])) {
                                        
                                		echo '<h3 class="mb-0">';
                                    		if (!empty($turtles[$activity["turtleid"]]["lefttag"]) || !empty($turtles[$activity["turtleid"]]["righttag"])) {
                                                
                                        		echo '<a title="More data about this turtle" data-toggle="tooltip" href="/', $language, '/turtle/', $activity["turtleid"], '">';
                                            }
                                        	
                                            echo 'Turtle <small>', $activity["turtleid"], '</small>';
                                        			
                                			if (!empty($turtles[$activity["turtleid"]]["lefttag"]) || !empty($turtles[$activity["turtleid"]]["righttag"])) {
                                			    
                                			    echo '</a>';
                                			}
                                		echo '</h3>';                                		
                        
                                		echo '<p>
                                			Tags&nbsp;: ';
                                    		if (!empty($turtles[$activity["turtleid"]]["lefttag"])) {
                                    		    
                                                echo '<span class="badge badge-dark" data-toggle="tooltip" title="Left side">',
                                                    $turtles[$activity["turtleid"]]["lefttag"],
                                                '</span>';
                                    		} else {
                                    		    echo '<span class="badge badge-danger" data-toggle="tooltip" title="Left side">no tag</span>';
                                    		}
                                            echo ' - ';
                                            if (!empty($turtles[$activity["turtleid"]]["righttag"])) {
                                                
                                                echo '<span class="badge badge-dark" data-toggle="tooltip" title="Right side">',
                                                    $turtles[$activity["turtleid"]]["righttag"],
                                                '</span>';
                                            } else {
                                                echo '<span class="badge badge-danger" data-toggle="tooltip" title="Right side">no tag</span>';
                                            }
                                        echo '</p>';
                                            
                                        if (!empty($turtles[$activity["turtleid"]]["englishname"])) {
                                            
                                            echo '<p>
                                                Species&nbsp;: 
                                                <strong data-toggle="tooltip" title="Wikipedia">
                                                    <a href="https://', $language, '.wikipedia.org/wiki/', $turtles[$activity["turtleid"]]["englishname"], '">',
                                                        $turtles[$activity["turtleid"]]["englishname"],
                                                    '</a>
                                                </strong>
                                            </p>';
                                        }
                            			
                                    	echo '<p>';
                                            if (!empty($turtles[$activity["turtleid"]]["length"])) {
                                                
                                                echo 'Length&nbsp;: <strong>', $turtles[$activity["turtleid"]]["length"], ' cm</strong><br />';
                                            }
                                            if (!empty($turtles[$activity["turtleid"]]["width"])) {
                                                
                                                echo 'Width&nbsp;: <strong>', $turtles[$activity["turtleid"]]["width"], ' cm</strong>';
                                            }
                                    	echo '</p>';
                                    }
                                    
                                	if (!empty($activity["widthreturntrace"])) {
                                	    
                                	    echo '<p>Return trace&nbsp;: <strong>', $activity["widthreturntrace"], ' cm</strong></p>';
                                	}
                                	
                                	if (!empty($activity["turtleid"])) {
                                	    
                                    	echo '<p>';
                                        	if (!empty($turtles[$activity["turtleid"]]["lefthurt"])) {
                                        	    
                                        	    echo '<strong class="text-capitalize">', $turtles[$activity["turtleid"]]["lefthurt"], '</strong> on the left side<br />';
                                        	}
                                        	if (!empty($turtles[$activity["turtleid"]]["righthurt"])) {
                                        	    
                                        		echo '<strong class="text-capitalize">', $turtles[$activity["turtleid"]]["righthurt"], '</strong> on the right side';
                                        	}
                                    	echo '</p>';
                                    	
                                	} elseif (empty($activity["widthreturntrace"])) {
                                	    
                                	    echo "<p>No turtle data for this activity.</p>";
                                	}
                                	
                                echo '</div>';
                                
                                echo '<div class="col-sm">
                                    
                                	<p>Step&nbsp;: <strong class="step">', $activity["step"], '/8</strong></p>';

                                	echo '<p>';
                                        if (!empty($activity["startlaying"])) {
                                            
                                            echo 'Start laying&nbsp;: <strong>', date("H:i", strtotime($activity["startlaying"])), '</strong><br />';
                                        }
                                        if (!empty($activity["endlaying"])) {
                                            
                                            echo 'End laying&nbsp;: <strong>', date("H:i", strtotime($activity["endlaying"])), '</strong>';
                                        }                                    	
                                	echo '</p>';
                                	
                                	echo '<p>';
                                	   if (!empty($activity["beachlocation"])) {
                                    	    
                                    	    echo 'Beach location&nbsp;: 
                                    	    <strong>', $activity["beachlocation"];
                                    	       if (!empty($activity["hatcheryid"])) {
                                    	           
                                    	           echo '/', $hatcheries[$activity["hatcheryid"]]["numberoflocations"];
                                    	    }
                                    	    echo '</strong><br />';
                                    	}
                                    	if (!empty($activity["beachplace"])) {
                                    		
                                    	    echo 'Height on the beach&nbsp;: <strong class="text-capitalize">', $activity["beachplace"], '</strong>';
                                    	}
                            		echo '</p>';
                            		
                            		if(!empty($activity["oldtide"])) {
                            		    
                            		  echo '<p>Estimated tide&nbsp;: <strong class="text-capitalize">', $activity["oldtide"], '</strong></p>';
                            		}
                            		
                                	echo '<p>';
                                    	if (!empty($activity["fire"])) {
                                    	    
                                    		echo '<strong>Campfire</strong> near the turtle.<br />';
                                    	}
                                    	if (!empty($activity["residues"])) {
                                    	    
                                    	    echo '<strong>Residues</strong> near the nest.';
                                    	}
                                	echo '</p>';

                                echo '</div>';

                                echo '<div class="col-sm">';
                                
                                    if (!empty($activity["destination"])) {
                                     
                                        echo '<p>
                                            Destination&nbsp;: ';
                                            if(!empty($activity["hatcheryid"]) && $activity["destination"] == "hatchery") {
                                                echo '<strong data-toggle="tooltip" title="Lat: ', $hatcheries[$activity["hatcheryid"]]["latgps"], ' Lon: ', $hatcheries[$activity["hatcheryid"]]["longps"], '">';
                                                echo $hatcheries[$activity["hatcheryid"]]["name"];
                                                echo '</strong>';
                                            } else {
                                                echo '<strong class="text-capitalize">', $activity["destination"], '</strong>';
                                            }
                                        echo '</p>';
                                    }
                                    
                                    if (!empty($activity["nestid"])) {
                                        
                                    	echo '<p>';
                                        	if (!empty($nests[$activity["nestid"]]["hatcheryleader"])) {
                                        	    
                                        	    $hatcheryleader = $users[$nests[$activity["nestid"]]["hatcheryleader"]];
                                        	    echo 'Hatchery leader&nbsp;: <strong>',
                                            	    $hatcheryleader["firstname"], ' ',
                                            	    $hatcheryleader["lastname"], ' ',
                                            	    '(', $hatcheryleader["registration"], ')',
                                        		'</strong><br />';
                                        	}
                                        	if (!empty($nests[$activity["nestid"]]["builder"])) {
                                        	    
                                        	    $builder = $users[$nests[$activity["nestid"]]["builder"]];
                                        	    echo 'Nest builder&nbsp;: <strong>',
                                            	    $builder["firstname"], ' ',
                                            	    $builder["lastname"], ' ',
                                            	    '(', $builder["registration"], ')',
                                        		'</strong>';
                                        	}
                                    	echo '</p>';
                                    	
                                    	if (!empty($nests[$activity["nestid"]]["code"])) {
                                        	echo '<p>
                                                Nest code&nbsp;: 
                                                <strong data-toggle="tooltip" title="Location in the hatchery">',
                                                    $nests[$activity["nestid"]]["code"],
                                                '</strong>
                                    		</p>';
                                    	}
                                    	
                                    	if (!empty($nests[$activity["nestid"]]["buildingdate"])) {
                                        	echo '<p>
                                        		Nest time&nbsp;: 
                                                <strong>',
                                                date("H:i", strtotime($nests[$activity["nestid"]]["buildingdate"])),
                                                '</strong>
                                    		</p>';
                                    	}
                                    	
                                    	if (!empty($nests[$activity["nestid"]]["eggs"])) {
                                    	echo '<div class="eggs" data-toggle="tooltip" title="Number of collected eggs">
                                    		<p>
                                                <strong>',
                                                    $nests[$activity["nestid"]]["eggs"],
                                                '</strong> 
                                                <span>eggs</span>
                                            </p>
                                    	</div>';
                                        }
                                    }
                                echo '</div>
                            </div>
                        </div>
                    	<p class="card-footer m-0">Meeting date&nbsp;: <span class="text-muted">', date("d/m/Y \a\\t H:i", strtotime($activity["date"])), '</span></p>
                    </div>';
                }
                ?>
            </div>
		</div>

        <div id="step-content" style="display: none;">
            <ol class="m-0 pl-3">
                <li>Come from the sea</li>
                <li>Looking for a place</li>
                <li>Start to dig</li>
                <li>Start to lay</li>
                <li>Close the nest</li>
                <li>Start covering</li>
                <li>Finish covering</li>
                <li>Returned to the sea</li>
            </ol>
        </div>
        
        <script>
            $(".step").popover({
                
            	  trigger: "hover",
            	  placement: "top",
            	  content: $("#step-content").html(),
            	  html: true
            });

            function reloadPrivateContent() {
            	
            	location.reload();
            }
        </script>
        
        <?php
        
        new FooterTemplate();
    }
}

