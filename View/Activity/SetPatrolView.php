<?php

namespace View\Activity;

use Template\HeaderTemplate;
use Framework\View;
use Template\ModalTemplate;
use Framework\Head;
use Template\FooterTemplate;

class SetPatrolView extends View
{
    /**
     * Generates the page to set or get a patrol activity.
     * For setting a new patrol activity. Last fields don't need to be used.
     *
     * @param array $stations
     * @param array $users
     * @param array $species
     * @param $language
     * @param null $activity
     * @param null $turtles
     * @param null $hatcheries
     * @param null $nests
     */
    public function __construct($stations, $users, $species, $language, $activity = null, $turtles = null, $hatcheries = null, $nests = null)
    {
        $this->head = new Head();

        $this->head->setScript("patrolActivity.js");
        $this->head->setScript("location.js");
        $this->head->setScript("station.js");
        $this->head->setScript("hatchery.js");
        $this->head->setScript("user.js");
        $this->head->setScript("turtle.js");

        if (!empty($activity["id"])) {

            $title = 'Patrol activity <small>' . $activity["id"] . '</small>';

        } else {

            $title = 'New patrol activity';
        }

        parent::__construct($title);

        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();

        ?>
        <style>
            label {
                font-weight: bold;
            }
        </style>

        <div class="container">

            <?php echo '<h1>', $title, '</h1>'; ?>

            <form id="patrol" method="post" action="">

                <?php
                if (!empty($_SESSION["userId"]) && !empty($activity["id"])) {

                    echo '<input type="hidden" name="id" value="', $activity["id"], '" />';
                }
                ?>

                <div class="row">
                    <div class="col-lg">
                        <div class="form-group">
                            <label class="control-label" for="date">Meeting date <strong class="text-danger">*</strong></label>
                            <?php
                            if (!empty($_SESSION["userId"])) {

                                echo '<div class="input-group date datetimepicker" id="date" data-target-input="nearest">
                            <input name="date" type="text" class="form-control datetimepicker-input" data-target="#date" aria-describedby="dateHelpBlock" ';
                                if (!empty($activity["date"])) {

                                    echo 'value="', date("d/m/Y H:i", strtotime($activity["date"])), '" ';
                                }
                                echo 'required />
                            <div class="input-group-append" data-target="#date" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="material-icons">date_range</i></div>
                            </div>
                        </div>
                        <p class="small form-text text-muted" id="dateHelpBlock">
                        	Please enter the true date and not the working date after midnight.
                        </p>';

                            } else {

                                echo '<p id="date">', date("d/m/Y à G:i", strtotime($activity["date"])), '</p>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="form-group">
                            <label class="control-label" for="station">Station <strong
                                        class="text-danger">*</strong></label>
                            <?php
                            if (!empty($_SESSION["userId"])) {

                                echo '<div class="input-group">
                            <select name="station" id="station" class="custom-select" required>';

                                foreach ($stations as $station) {

                                    echo '<option value ="', $station["id"], '"';
                                    if (!empty($activity["stationid"]) && $station["id"] == $activity["stationid"] || $station["id"] == 1) echo ' selected';
                                    echo '>', $station["name"], '</option>';
                                }

                                echo '</select>
                            <div class="input-group-append">
                            	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#addstation">Add station</button>
                            </div>
				        </div>';

                            } else {

                                echo '<div id="station">
                            <p>', $stations[$activity["stationid"]]["name"], '</p>
                            <p>Open in&nbsp;: ', date("d/m/Y", strtotime($stations[$activity["stationid"]]["date"])), '</p>';
                                if (!empty($stations[$activity["stationid"]]["address"])) {

                                    echo '<p>Address&nbsp;: ', $stations[$activity["stationid"]]["address"], '</p>';
                                }
                                if (!empty($users[$stations[$activity["stationid"]]["contact"]])) {

                                    $user = $users[$stations[$activity["stationid"]]["contact"]];
                                    echo '<p>Contact&nbsp;: ',
                                    $user["firstname"], ' ',
                                    $user["lastname"], ' (',
                                    $user["registration"], ')
                                </p>';
                                }
                                if (!empty($stations[$activity["stationid"]]["latgps"]) && !empty($stations[$activity["stationid"]]["longps"])) {

                                    echo '<p>Coordonnées GPS&nbsp;:<br />
                                Latitude&nbsp;: ', $stations[$activity["stationid"]]["latgps"], '<br />
                                Longitude&nbsp;: ', $stations[$activity["stationid"]]["longps"],
                                    '</p>';
                                }
                                if (!empty($stations[$activity["stationid"]]["comments"])) {

                                    echo '<p>Comments&nbsp;: ', $stations[$activity["stationid"]]["comments"], '</p>';
                                }
                                echo '</div>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="form-group">
                            <label class="control-label" for="leader">Leader <strong
                                        class="text-danger">*</strong></label>
                            <?php
                            if (!empty($_SESSION["userId"])) {

                                echo '<div class="input-group">
                            <select name="leader" id="leader" class="custom-select" required>';

                                $selected = (!empty($activity["leader"])) ? $activity["leader"] : 1;
                                foreach ($users as $user) {

                                    echo '<option value ="', $user["id"], '"';
                                    if ($selected == $user["id"]) {

                                        echo " selected";
                                    }
                                    echo '>', $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')</option>';
                                }
                                echo '</select>
                            <div class="input-group-append">
                            	<button class="btn btn-secondary" type="button" data-toggle="modal" data-id="#leader" data-target="#adduser">Add user</button>
                            </div>
                        </div>';

                            } else {

                                echo '<p id="leader">',
                                $users[$activity["leader"]]["firstname"], ' ',
                                $users[$activity["leader"]]["lastname"], ' (',
                                $users[$activity["leader"]]["registration"], ')
                        </p>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-lg">
                        <div class="form-group">
                            <label class="control-label" for="assistant">Assistant</label>
                            <?php
                            if (!empty($_SESSION["userId"])) {

                                echo '<div class="input-group">
                        <select name="assistant" id="assistant" class="custom-select">
                        	<option value="">Nobody</option>';
                                foreach ($users as $user) {

                                    echo '<option value ="', $user["id"], '"';
                                    if (!empty($activity["assistant"]) && $activity["assistant"] == $user["id"]) {
                                        echo ' selected';
                                    }
                                    echo '>', $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')
                                </option>';
                                }
                                echo '</select>
                        <div class="input-group-append">
                        	<button class="btn btn-secondary" type="button" data-toggle="modal" data-id="#assistant" data-target="#adduser">Add user</button>
                        </div>
                    </div>';

                            } elseif ($activity["assistant"]) {

                                echo '<p id="leader">',
                                $users[$activity["assistant"]]["firstname"], ' ',
                                $users[$activity["assistant"]]["lastname"], ' (',
                                $users[$activity["assistant"]]["registration"], ')
                        </p>';
                            } else {

                                echo '<p id="leader">Nobody</p>';
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm">

                        <div class="row">
                            <h2 class="col-lg">Activity data / sighting</h2>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="oldtide">Estimated tide</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<select name="oldtide" id="oldtide" class="custom-select">
                                    <option  value="" disabled';
                                        if (empty($activity["oldtide"])) {
                                            echo ' selected';
                                        }
                                        echo '>Choose...</option>
                                    <option value="up"';
                                        if (!empty($activity["oldtide"]) && $activity["oldtide"] == "up") {
                                            echo ' selected';
                                        }
                                        echo '>Up</option>
                                    <option value="high"';
                                        if (!empty($activity["oldtide"]) && $activity["oldtide"] == "high") {
                                            echo ' selected';
                                        }
                                        echo '>High</option>
                                    <option value="down"';
                                        if (!empty($activity["oldtide"]) && $activity["oldtide"] == "down") {
                                            echo ' selected';
                                        }
                                        echo '>Down</option>
                                    <option value="low"';
                                        if (!empty($activity["oldtide"]) && $activity["oldtide"] == "low") {
                                            echo ' selected';
                                        }
                                        echo '>Low</option>
                                </select>';
                                    } elseif (!empty($activity["oldtide"])) {

                                        echo '<p id="oldtide" class="text-capitalize">', $activity["oldtide"], '</p>';

                                    } else {

                                        echo '<p id="oldtide">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="beachlocation">Beach location <strong
                                                class="text-danger">*</strong></label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<input id="beachlocation" name="beachlocation" type="number" min="1" class="form-control" ';
                                        if (!empty($activity["beachlocation"])) {

                                            echo 'value="', $activity["beachlocation"], '" ';
                                        }
                                        echo 'required />';

                                    } else {

                                        echo '<p id="beachlocation">', $activity["beachlocation"], '</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">

                                <div class="row">
                                    <div class="col-lg">
                                        <div class="form-group">
                                            <label class="control-label" for="startlaying">Start laying</label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<div class="input-group date timepicker" id="startlaying" data-target-input="nearest">
                                            <input name="startlaying" type="text" class="form-control datetimepicker-input" data-target="#startlaying" ';
                                                if (!empty($activity["startlaying"])) {

                                                    echo 'value="', date("G:i", strtotime($activity["startlaying"])), '" ';
                                                }
                                                echo '/>
                                            <div class="input-group-append" data-target="#startlaying" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="material-icons">access_time</i></div>
                                            </div>
                                        </div>';

                                            } elseif (!empty($activity["startlaying"])) {

                                                echo '<p id="startlaying">', date("G:i", strtotime($activity["startlaying"])), '</p>';
                                            } else {

                                                echo '<p id="startlaying">No data</p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg">
                                        <div class="form-group">
                                            <label class="control-label" for="endlaying">End laying</label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<div class="input-group date timepicker" id="endlaying" data-target-input="nearest">
                                            <input name="endlaying" type="text" class="form-control datetimepicker-input" data-target="#endlaying" ';
                                                if (!empty($activity["endlaying"])) {

                                                    echo 'value="', date("G:i", strtotime($activity["endlaying"])), '" ';
                                                }
                                                echo '/>
                                            <div class="input-group-append" data-target="#endlaying" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="material-icons">access_time</i></div>
                                            </div>
                                        </div>';
                                            } elseif (!empty($activity["endlaying"])) {

                                                echo '<p id="endlaying">', date("G:i", strtotime($activity["endlaying"])), '</p>';

                                            } else {

                                                echo '<p id="endlaying">No data</p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg">
                                        <div class="form-group">
                                            <label class="control-label" for="nestdate">Nest time</label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<div class="form-group">
                                            <div class="input-group date timepicker" id="nestdate" data-target-input="nearest">
                                                <input name="nestdate" type="text" class="form-control datetimepicker-input" data-target="#nestdate" ';
                                                if (!empty($nests[$activity["nestid"]]["buildingdate"])) {

                                                    echo 'value="', date("G:i", strtotime($nests[$activity["nestid"]]["buildingdate"])), '" ';
                                                }
                                                echo '/>
                                               	<div class="input-group-append" data-target="#nestdate" data-toggle="datetimepicker">
                                                   	<div class="input-group-text"><i class="material-icons">access_time</i></div>
                                                </div>
                                            </div>
                                        </div>';
                                            } elseif (!empty($nests[$activity["nestid"]]["buildingdate"])) {

                                                echo '<p id="nestdate">', date("G:i", strtotime($nests[$activity["nestid"]]["buildingdate"])), '</p>';

                                            } else {

                                                echo '<p id="nestdate">No data</p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="step">Step <strong class="text-danger">*</strong></label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<input name="step" id="step" type="number" value="2" min="1" max="8" class="form-control" aria-describedby="stepHelpBlock" required ';
                                        if (!empty($activity["step"])) {

                                            echo 'value="', $activity["step"], '" ';
                                        }
                                        echo '/>';

                                    } else {

                                        echo '<p id="step">', $activity["step"], '</p>';
                                    }
                                    ?>
                                    <ol class="small form-text text-muted pl-4" id="stepHelpBlock">
                                        <li>Come from the sea</li>
                                        <li>Looking for a place</li>
                                        <li>Start to dig</li>
                                        <li>Start to lay</li>
                                        <li>Close the nest</li>
                                        <li>Start covering</li>
                                        <li>Finish covering</li>
                                        <li>Returned to the sea</li>
                                    </ol>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="species">Species</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <select name="species" id="species" class="custom-select">
                                    	<option value=""';
                                        if (empty($turtles[$activity["turtleid"]]["speciesid"])) {

                                            echo ' selected';
                                        }
                                        echo '></option>';
                                        foreach ($species as $specie) {

                                            echo '<option value ="', $specie["id"], '" ';
                                            if (!empty($turtles[$activity["turtleid"]]["speciesid"]) && $turtles[$activity["turtleid"]]["speciesid"] == $specie["id"] || $specie["id"] == 1) echo "selected";
                                            echo '>', $specie["englishname"], '</option>';
                                        }
                                        echo '</select>
                                    <div class="input-group-append">
                                    	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#addspecies">Add species</button>
                                    </div>
                                </div>';

                                    } elseif (!empty($turtles[$activity["turtleid"]]["speciesid"])) {

                                        echo '<p id="species">
                                    English name&nbsp;: ',
                                        $turtles[$activity["turtleid"]]["englishname"], '<br />
                                    Scientific name&nbsp;: ',
                                        $turtles[$activity["turtleid"]]["scientificname"], '<br />
                                    <a href="https://', $language, '.wikipedia.org/wiki/', $turtles[$activity["turtleid"]]["englishname"], '">
                                        Wikipédia
                                    </a>
                                </p>';

                                    } else {

                                        echo '<p id="species">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm">

                        <div class="row">
                            <h2 class="col-sm">Nesting data</h2>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="laid">Laid <strong class="text-danger">*</strong></label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<select name="laid" id="laid" class="custom-select" required>
                                    <option value="yes"';
                                        if (!empty($activity["laid"]) && $activity["laid"] == "yes") {

                                            echo " selected";
                                        }
                                        echo '>Yes</option>
                                    <option value="no"';
                                        if (!empty($activity["laid"]) && $activity["laid"] == "no") {

                                            echo " selected";
                                        }
                                        echo '>No</option>
                                    <option value="dontknow"';
                                        if (empty($activity["laid"]) || !empty($activity["laid"]) && $activity["laid"] == "dontknow") {

                                            echo " selected";
                                        }
                                        echo '>Don\'t know</option>
                                </select>';

                                    } else {

                                        echo '<p id="laid" class="text-capitalize">', $activity["laid"], '</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="beachplace">Height on the beach</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<select name="beachplace" id="beachplace" class="custom-select">
                                	<option value=""';
                                        if (empty($activity["beachplace"])) {

                                            echo " selected";
                                        }
                                        echo '></option>
                                    <option value="vegetation"';
                                        if (!empty($activity["beachplace"]) && $activity["beachplace"] == "vegetation") {

                                            echo " selected";
                                        }
                                        echo '>Vegetation</option>
                                    <option value="berm"';
                                        if (!empty($activity["beachplace"]) && $activity["beachplace"] == "berm") {

                                            echo " selected";
                                        }
                                        echo '>Berm</option>
                                    <option value="walkingarea"';
                                        if (!empty($activity["beachplace"]) && $activity["beachplace"] == "walkingarea") {

                                            echo " selected";
                                        }
                                        echo '>Walking Area</option>
                                </select>';

                                    } elseif (!empty($activity["beachplace"])) {

                                        echo '<p id="beachplace" class="text-capitalize">', $activity["beachplace"], '</p>';

                                    } else {

                                        echo '<p id="beachplace">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="destination">Destination</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <select name="destination" id="destination" class="custom-select">
                                    	<option value=""';
                                        if (empty($activity["destination"])) {

                                            echo " selected";
                                        }
                                        echo '></option>
                                        <option value="relocated"';
                                        if (!empty($activity["destination"]) && $activity["destination"] == "relocated") {

                                            echo " selected";
                                        }
                                        echo '>Relocated</option>
                                        <option value="natural"';
                                        if (!empty($activity["destination"]) && $activity["destination"] == "natural") {

                                            echo " selected";
                                        }
                                        echo '>Natural</option>
                                    </select>
                                    <div class="input-group-append">
                                    	<button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#addhatchery">Add hatchery</button>
                                    </div>
                                </div>';

                                    } elseif (!empty($activity["destination"])) {

                                        if (!empty($activity["hatcheryid"])) {

                                            echo '<div id="destination">';

                                            echo '<p>',
                                            $hatcheries[$activity["hatcheryid"]]["name"],
                                            '</p>
                                        <p>Open in&nbsp;: ', date("d/m/Y", strtotime($hatcheries[$activity["hatcheryid"]]["date"])), '</p>
                                        <p>
                                            Size of the grid&nbsp;: ',
                                            $hatcheries[$activity["hatcheryid"]]["width"],
                                            $hatcheries[$activity["hatcheryid"]]["height"],
                                            '</p>
                                        <p>
                                            Number of locations&nbsp;: ',
                                            $hatcheries[$activity["hatcheryid"]]["numberoflocations"],
                                            '</p>';
                                            if ($hatcheries[$activity["hatcheryid"]]["latgps"] && $hatcheries[$activity["hatcheryid"]]["longps"]) {
                                                echo '<p>
                                                Coordonnées GPS&nbsp;: <br />
                                                Latitude&nbsp;: ', $hatcheries[$activity["hatcheryid"]]["latgps"], '<br />
                                                Longitude&nbsp;: ', $hatcheries[$activity["hatcheryid"]]["longps"],
                                                '</p>';
                                            }
                                            if (!empty($hatcheries[$activity["hatcheryid"]]["comments"])) {

                                                echo '<p>
                                                Comments<br />',
                                                $hatcheries[$activity["hatcheryid"]]["comments"],
                                                '</p>';
                                            }

                                            echo '</div>';

                                        } else {

                                            echo '<p id="destination">', $activity["destination"], '</p>';
                                        }

                                    } else {

                                        echo '<p id="destination">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="code">Nest code</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                	<input name="code" id="code" type="text" class="form-control" ';
                                        if (!empty($nests[$activity["nestid"]]["code"])) {

                                            echo 'value="', $nests[$activity["nestid"]]["code"], '" ';
                                        }
                                        echo '/>
                                </div>';

                                    } elseif (!empty($nests[$activity["nestid"]]["code"])) {

                                        echo '<p id="code">', $nests[$activity["nestid"]]["code"], '</p>';

                                    } else {

                                        echo '<p id="code">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="hatcheryleader">Hatchery leader</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <select name="hatcheryleader" id="hatcheryleader" class="custom-select">
                                    	<option value=""';
                                        if (empty($nests[$activity["nestid"]]["hatcheryleader"])) {

                                            echo " selected";
                                        }
                                        echo '>Nobody</option>';
                                        foreach ($users as $user) {

                                            echo '<option value ="', $user["id"], '"';
                                            if (!empty($nests[$activity["nestid"]]["hatcheryleader"]) && $nests[$activity["nestid"]]["hatcheryleader"] == $user["id"]) {

                                                echo " selected";
                                            }
                                            echo '>', $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')</option>';
                                        }
                                        echo '</select>
                                    <div class="input-group-append">
                                    	<button class="btn btn-secondary" type="button" data-toggle="modal" data-id="#hatcheryleader" data-target="#adduser">Add user</button>
                                    </div>
                                </div>';

                                    } elseif (!empty($activity["nestid"]) && !empty($users[$nests[$activity["nestid"]]["hatcheryleader"]])) {

                                        $user = $users[$nests[$activity["nestid"]]["hatcheryleader"]];

                                        echo '<p id="hatcheryleader">',
                                        $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')',
                                        '</p>';

                                    } else {

                                        echo '<p id="hatcheryleader">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="builder">Builder</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <select name="builder" id="builder" class="custom-select">
                                    	<option value=""';
                                        if (empty($nests[$activity["nestid"]]["builder"])) {

                                            echo " selected";
                                        }
                                        echo '>Nobody</option>';
                                        foreach ($users as $user) {

                                            echo '<option value ="', $user["id"], '"';
                                            if (!empty($nests[$activity["nestid"]]["builder"]) && $nests[$activity["nestid"]]["builder"] == $user["id"]) {

                                                echo " selected";
                                            }
                                            echo '>', $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')</option>';
                                        }
                                        echo '</select>
                                    <div class="input-group-append">
                                    	<button class="btn btn-secondary" type="button" data-toggle="modal" data-id="#builder" data-target="#adduser">Add user</button>
                                    </div>
                                </div>';

                                    } elseif (!empty($activity["nestid"]) && !empty($users[$nests[$activity["nestid"]]["builder"]])) {

                                        $user = $users[$nests[$activity["nestid"]]["builder"]];

                                        echo '<p id="hatcheryleader">',
                                        $user["firstname"], ' ', $user["lastname"], ' (', $user["registration"], ')',
                                        '</p>';

                                    } else {

                                        echo '<p id="hatcheryleader">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="eggs">Number of eggs</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<input id="eggs" name="eggs" type="number" min="1" class="form-control" ';

                                        if (!empty($nests[$activity["nestid"]]["eggs"])) {

                                            echo 'value="', $nests[$activity["nestid"]]["eggs"], '" ';
                                        }
                                        echo '/>';

                                    } elseif (!empty($nests[$activity["nestid"]]["eggs"])) {

                                        echo '<p id="eggs">', $nests[$activity["nestid"]]["eggs"], '</p>';

                                    } else {

                                        echo '<p id="eggs">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <h3 class="col-lg">Anthropogenic activities present on the beach</h3>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="custom-control custom-checkbox">
                                	<input id="fire" name="fire" type="checkbox" class="custom-control-input" ';
                                        if (!empty($activity["fire"])) {

                                            echo " checked";
                                        }
                                        echo '/>
                                	<label class="custom-control-label" for="fire">Fire</label>
                                </div>';

                                    } elseif (!empty($activity["fire"])) {

                                        echo '<p id="fire">Campfire near the turtle.</p>';

                                    } else {

                                        echo '<p id="fire">No campfire on the beach.</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="custom-control custom-checkbox">
                                	<input id="residues" name="residues" type="checkbox" class="custom-control-input" ';
                                        if (!empty($activity["residues"])) {

                                            echo " checked";
                                        }
                                        echo '/>
                                	<label class="custom-control-label" for="residues">Residues</label>
                                </div>';
                                    } elseif (!empty($activity["residues"])) {

                                        echo '<p id="residues">Residues near the nest.</p>';

                                    } else {

                                        echo '<p id="residues">No residues.</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-sm">

                        <div class="row">
                            <h2 class="col-sm">Turtle data</h2>
                        </div>

                        <div class="row">
                            <div class="col-sm">
                                <div class="form-group">
                                    <label class="control-label" for="length">Turtle length</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <input name="length" id="length" type="text" class="form-control" ';
                                        if (!empty($turtles[$activity["turtleid"]]["length"])) {

                                            echo 'value="', $turtles[$activity["turtleid"]]["length"], '" ';
                                        }
                                        echo '/>
                                    <div class="input-group-append">
                                    	<span class="input-group-text" >cm</span>
                                    </div>
                                </div>';

                                    } elseif (!empty($turtles[$activity["turtleid"]]["length"])) {

                                        echo '<p id="length">', $turtles[$activity["turtleid"]]["length"], ' cm</p>';

                                    } else {

                                        echo '<p id="length">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm">
                                <div class="form-group">
                                    <label class="control-label" for="width">Turtle width</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <input name="width" id="width" type="text" class="form-control" ';
                                        if (!empty($turtles[$activity["turtleid"]]["width"])) {

                                            echo 'value="', $turtles[$activity["turtleid"]]["width"], '" ';
                                        }
                                        echo '/>
                                    <div class="input-group-append">
                                    	<span class="input-group-text" >cm</span>
                                    </div>
                                </div>';

                                    } elseif (!empty($turtles[$activity["turtleid"]]["width"])) {

                                        echo '<p id="width">', $turtles[$activity["turtleid"]]["width"], ' cm</p>';

                                    } else {

                                        echo '<p id="width=">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-lg">
                                <div class="form-group">
                                    <label class="control-label" for="widthreturntrace">Width of the return
                                        trace</label>
                                    <?php
                                    if (!empty($_SESSION["userId"])) {

                                        echo '<div class="input-group">
                                    <input name="widthreturntrace" id="widthreturntrace" type="text" class="form-control" ';
                                        if (!empty($activity["widthreturntrace"])) {

                                            echo 'value="', $activity["widthreturntrace"], '" ';
                                        }
                                        echo '/>
                                    <div class="input-group-append">
                                    	<span class="input-group-text" >cm</span>
                                    </div>
                                </div>';

                                    } elseif (!empty($activity["widthreturntrace"])) {

                                        echo '<p id="widthreturntrace">', $activity["widthreturntrace"], ' cm</p>';

                                    } else {

                                        echo '<p id="widthreturntrace">No data</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <h3 class="col-sm">Front marking process</h3>
                        </div>

                        <div class="row">
                            <div class="col-sm">

                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label class="control-label" for="lefttag">Left tag <span
                                                        class="badge badge-danger">important</span></label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<div class="input-group">
                                        	<input name="lefttag" id="lefttag" type="text" class="form-control" ';
                                                if (!empty($turtles[$activity["turtleid"]]["lefttag"])) {

                                                    echo 'value="', $turtles[$activity["turtleid"]]["lefttag"], '" ';
                                                }
                                                echo '/>
                                        </div>';

                                            } elseif (!empty($turtles[$activity["turtleid"]]["lefttag"])) {

                                                echo '<p id="lefttag">', $turtles[$activity["turtleid"]]["lefttag"], '</p>';

                                            } else {

                                                echo '<p id="lefttag">No data</a>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label class="control-label" for="righttag">Right tag <span
                                                        class="badge badge-danger">important</span></label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<div class="input-group">
                                        	<input name="righttag" id="righttag" type="text" class="form-control" ';
                                                if (!empty($turtles[$activity["turtleid"]]["righttag"])) {

                                                    echo 'value="', $turtles[$activity["turtleid"]]["righttag"], '" ';
                                                }
                                                echo '/>
                                        </div>';

                                            } elseif (!empty($turtles[$activity["turtleid"]]["righttag"])) {

                                                echo '<p id="righttag">', $turtles[$activity["turtleid"]]["righttag"], '</p>';

                                            } else {

                                                echo '<p id="righttag">No data</a>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm">

                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label class="control-label" for="lefthurt">Left injury</label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<select name="lefthurt" id="lefthurt" class="custom-select">
                                            <option value=""';
                                                if (empty($turtles[$activity["turtleid"]]["lefthurt"])) {

                                                    echo ' selected';
                                                }
                                                echo '></option>
                                            <option value="scar"';
                                                if (empty($turtles[$activity["turtleid"]]["lefthurt"]) && $turtles[$activity["turtleid"]]["lefthurt"] == "scar") {

                                                    echo ' selected';
                                                }
                                                echo '>Scar</option>
                                            <option value="tear"';
                                                if (empty($turtles[$activity["turtleid"]]["lefthurt"]) && $turtles[$activity["turtleid"]]["lefthurt"] == "tear") {

                                                    echo ' selected';
                                                }
                                                echo '>Tear</option>
                                            <option value="hole"';
                                                if (empty($turtles[$activity["turtleid"]]["lefthurt"]) && $turtles[$activity["turtleid"]]["lefthurt"] == "hole") {

                                                    echo ' selected';
                                                }
                                                echo '>Hole</option>
                                        </select>';

                                            } elseif (!empty($turtles[$activity["turtleid"]]["lefthurt"])) {

                                                echo '<p id="lefthurt" class="text-capitalize">', $turtles[$activity["turtleid"]]["lefthurt"], '</p>';

                                            } else {

                                                echo '<p id="lefthurt">No data</p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm">
                                        <div class="form-group">
                                            <label class="control-label" for="righthurt">Right injury</label>
                                            <?php
                                            if (!empty($_SESSION["userId"])) {

                                                echo '<select name="righthurt" id="righthurt" class="custom-select">
                                            <option value=""';
                                                if (empty($turtles[$activity["turtleid"]]["righthurt"])) {

                                                    echo ' selected';
                                                }
                                                echo '></option>
                                            <option value="scar"';
                                                if (empty($turtles[$activity["turtleid"]]["righthurt"]) && $turtles[$activity["turtleid"]]["righthurt"] == "scar") {

                                                    echo ' selected';
                                                }
                                                echo '>Scar</option>
                                            <option value="tear"';
                                                if (empty($turtles[$activity["turtleid"]]["righthurt"]) && $turtles[$activity["turtleid"]]["righthurt"] == "tear") {

                                                    echo ' selected';
                                                }
                                                echo '>Tear</option>
                                            <option value="hole"';
                                                if (empty($turtles[$activity["turtleid"]]["righthurt"]) && $turtles[$activity["turtleid"]]["righthurt"] == "hole") {

                                                    echo ' selected';
                                                }
                                                echo '>Hole</option>
                                        </select>';

                                            } elseif (!empty($turtles[$activity["turtleid"]]["righthurt"])) {

                                                echo '<p id="lefthurt" class="text-capitalize">', $turtles[$activity["turtleid"]]["righthurt"], '</p>';

                                            } else {

                                                echo '<p id="lefthurt">No data</p>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm">

                        <div class="row">
                            <h2 class="col-sm">Comments</h2>
                        </div>

                        <div class="row">
                            <div class="col-sm">
                                <?php
                                if (!empty($_SESSION["userId"])) {

                                    echo '<div class="form-group">
                            <p class="small form-text text-muted" id="commentsHelpBlock">
                            	Please enter the number of eggs in the appropriate field.
                            </p>
                            <textarea name="comments" id="comments" class="form-control" rows="3" aria-describedby="commentsHelpBlock">';
                                    if (!empty($activity["comments"])) {

                                        echo $activity["comments"];
                                    }
                                    echo '</textarea>
                            </div>';

                                } elseif (!empty($activity["comments"])) {

                                    echo '<p id="comments">', $activity["comments"], '</p>';

                                } else {

                                    echo '<p id="comments">No data</p>';
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <?php
                if (!empty($_SESSION["userId"])) {

                    ?>
                    <hr/>

                    <div class="row">
                        <p class="col-sm">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                        </p>
                    </div>
                    <?php
                }
                ?>
            </form>
        </div>
        <!-- Modals -->
        <?php
        if (!empty($_SESSION["userId"])) {

            $modalTemplate = new ModalTemplate();
            $modalTemplate->generateModals($users, $stations);
        }
        new FooterTemplate();
    }
}

