<?php
namespace View;

use Framework\View;
use Template\HeaderTemplate;
use Framework\App;
use Template\FooterTemplate;

class LanguageView extends View
{
    
    public function __construct()
    {
        $conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);
    
        parent::__construct($conf["APP"]["name"]);
        
        $headerTemplate = new HeaderTemplate();
        $headerTemplate->generateHtml();
        
        $app = new App();
        
        ?>
        <style>
            #language {
                height: 70vh;
                display: flex;
            }
            
            .container_p {
                margin: auto;
            }
            
            .button_group {
                display: flex;
                flex-direction: column;
            }
            
            .button_group > * {
                margin: auto;
                margin: 2vw;
            }
            
            .button_group > * > img {
                height: 15vw;
            }
            
            @media ( min-width : 800px ) {
            
                .button_group {
                    flex-direction: row;
                }
            
                .button_group > * > img {
                    height: 5vw;
                }
            }
        </style>
        <div id="language">
        	<div class="container_p">
            	<h1 class="text-center">Please select your language</h1>
            	<p class="button_group">
            		<a href="/en/" class="btn btn-secondary btn-lg">
            			<?php echo '<img alt="English flag" src="/', $app->asset, $app->image, 'flags/en.svg" /><br />'; ?>
            			English
            		</a>
            		<a href="/es/" class="btn btn-secondary btn-lg disabled">
            			<?php echo '<img alt="Spain flag" src="/', $app->asset, $app->image, 'flags/es.svg" /><br />'; ?>
            			Español
            		</a>
            		<a href="/fr/" class="btn btn-secondary btn-lg disabled">
            			<?php echo '<img alt="French flag" src="/', $app->asset, $app->image, 'flags/fr.svg" /><br />'; ?>
            			Français
            		</a>
            		<a href="/de/" class="btn btn-secondary btn-lg disabled">
            			<?php echo '<img alt="German flag" src="/', $app->asset, $app->image, 'flags/de.svg" /><br />'; ?>
            			Deutsch
            		</a>
            	</p>
        	</div>
        </div>
        <?php
        
        new FooterTemplate();
    }
}
