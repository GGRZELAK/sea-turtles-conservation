<?php
namespace Router;

use Framework\App;

require "vendor/autoload.php";

$conf = (file_exists("Framework/prod.ini")) ?  parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

$httpHost = explode('.', $_SERVER["HTTP_HOST"]);
$numbSubDns = count($httpHost);
$dns = array();
$subDns = array();
for($count=0; $count<$numbSubDns; $count++)
{
    if ($count < $numbSubDns-$conf["APP"]["dnsrange"])
    {
        array_push($subDns, $httpHost[$count]);
    } else {
        array_push($dns, $httpHost[$count]);
    }
}

$GLOBALS["subDns"] = (!empty($subDns)) ? implode('.', $subDns) : null;
$GLOBALS["dns"] = implode('.', $dns);

$app = new App();

session_set_cookie_params(0, '/', '.' . $app->dns);
session_start();

// Langage
$GLOBALS["language"] = (!empty($_SESSION["language"])) ? $_SESSION["language"] : "en";

// Activation du cache sauf pour l'API
if ($GLOBALS["subDns"] != "api" && $GLOBALS["subDns"] != "cron")
    header("Cache-Control: max-age=31536000");

$url = (! empty($_GET["url"])) ? strip_tags($_GET["url"]) : null;

$router = new Router($url);

$languageRegex = "[a-z]{2}";
$lowAlphabRegex = "[a-z]+";
$turtleIdRegex = "\S{0,50}";

$router->add(null, "/script", "Script#index");

$router->add(null, "/:language/login", "Api\Authenticate#login")->with("language", $languageRegex);
$router->add(null, "/:language/register", "Api\Authenticate#register")->with("language", $languageRegex);
$router->add(null, "/login", "Api\Authenticate#login");
$router->add(null, "/register", "Api\Authenticate#register");

$router->add(null, '/', "Home#language");
$router->add(null, "/:language/", "Home#index")->with("language", $languageRegex);

$router->add(null, "/:language/activity/patrol", "Activity#setPatrol")->with("language", $languageRegex);
$router->add(null, "/:language/activities/patrol/", "Activity#addYear")->with("language", $languageRegex);
$router->add(null, "/:language/activities/patrol/:year", "Activity#getPatrol")->with("language", $languageRegex)->with("year", "[0-9]{4}");
$router->add(null, "/:language/turtle/:id", "Turtle#get")->with("language", $languageRegex)->with("id", $turtleIdRegex);
$router->add(null, "/:language/entity/:table", "Entity#get")->with("language", $languageRegex)->with("table", $lowAlphabRegex);

$router->add(null, "/import", "Data#import");

$router->add("api", "/set/activity/patrol", "Api\Activity\Patrol#set");

$router->add("api", "/set/station", "Api\Station#set");
$router->add("api", "/set/hatchery", "Api\Hatchery#set");
$router->add("api", "/set/species", "Api\Turtle#setSpecies");
$router->add("api", "/set/user", "Api\Authenticate#register");

$router->add("api", "/update/:entity/:pk", "Api\Entity#update")->with("entity", $lowAlphabRegex)->with("pk", $turtleIdRegex);
$router->add("api", "/delete/:entity/:pk", "Api\Entity#delete")->with("entity", $lowAlphabRegex)->with("pk", $turtleIdRegex);

$router->add("api", "/activities/patrol", "Api\Activity\Patrol#get");

$router->add("api", "/stations", "Api\Station#get");
$router->add("api", "/hatcheries", "Api\Hatchery#get");
$router->add("api", "/species", "Api\Turtle#getSpecies");
$router->add("api", "/turtles", "Api\Turtle#get");
$router->add("api", "/users", "Api\Authenticate#getUsers");
$router->add("api", "/nests", "Api\Hatchery#getNests");

$router->add("api", "/login", "Api\Authenticate#login");
$router->add("api", "/logout", "Api\Authenticate#logout");
$router->add("api", "/password/forgot", "Api\Authenticate#forgot");
$router->add("api", "/user/update", "Api\Authenticate#update");


try {
    $router->run();
} catch (RouterException $e) {
}
