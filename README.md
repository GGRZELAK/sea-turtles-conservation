# Sea turtles conservation

## Contribute

As an open source project, Sea turtles conservation benefits greatly from both the volunteer work of helpful developers and good bug reports made by users.

### Bug Reports & Feature Requests

If you've noticed a bug or simply have an idea that you'd like to see become real, why not work on it? Bug reports and feature requests are typically submitted to the issue tracker on our GitLab repository.

### Issue Tracker

The Sea turtles conservation is a build environment that includes all the tools necessary for developers who want to contribute by writing code.

Please look at the [technical overview](https://gitlab.com/GGRZELAK/sea-turtles-conservation/wikis/Examples-of-development-environment) of the packaging and how to include your changes in your own custom installer.

To contribute/update this web page, see its Repository.