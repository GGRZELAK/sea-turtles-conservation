<?php
namespace Template;

class HatcheryTemplate
{

    public function hatcheryTools($stations, $stationId = 1)
    {
                        
$return = '<div class="modal fade" id="addhatchery" tabindex="-1" role="dialog"
                 aria-labelledby="addhatcheryModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <form class="modal-content" method="post" action="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addhatcheryModalLabel">
                                <i class="material-icons">add_circle</i> New hatchery
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="stationid">Station <strong class="text-danger">*</strong></label>
                                        <div class="input-group">
                                                <select name="station" id="stationid" class="custom-select" required>';

                                                foreach($stations as $station) {

                                                    $return .= '<option value ="' . $station["id"] . '"';
                                                    if ($station["id"] == $stationId) $return .= ' selected';
                                                    $return .= '>' . $station["name"] . '</option>';
                                                }

                                                $return .= '</select>
                                                <div class="input-group-append">
                                                    <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#addstation">Add station</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="hatcheryname">Hatchery name <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <input name="hatcheryname" id="hatcheryName" type="text" class="form-control" required autofocus />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="width">Grid width <strong class="text-danger">*</strong></label>
                                        <div class="input-group">
                                            <p class="container-fluid mb-0 px-0"><input name="width" id="width" type="text" class="form-control" min="1" aria-describedby="widthHelpBlock" required /></p>
                                            <p class="small form-text text-muted m-0" id="widthHelpBlock">
                                                Please enter the largest letter of the grid.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="height">Grid height <strong class="text-danger">*</strong></label>
                                        <div class="input-group">
                                            <p class="container-fluid mb-0 px-0"><input name="height" id="height" type="number" min="1" class="form-control" aria-describedby="heightHelpBlock" required /></p>
                                            <p class="small form-text text-muted m-0" id="heightHelpBlock">
                                                Please enter the largest number of the grid.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="numberoflocations">Number of locations <strong class="text-danger">*</strong></label>
                                        <div class="input-group">
                                            <p class="container-fluid mb-0 px-0"><input name="numberoflocations" id="numberoflocations" type="number" min="1" class="form-control" aria-describedby="numberoflocationsHelpBlock" required /></p>
                                            <p class="small form-text text-muted m-0" id="numberoflocationsHelpBlock">
                                                Please enter the last place on the beach (last mojón).
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label">GPS coordinates</label>
                                        <div class="input-group">
                                            <input name="hatcherylatgps" type="text" placeholder="Latitude" class="form-control latgps" aria-describedby="hatcheryLocationHelpBlock" />
                                            <input name="hatcherylongps" type="text" placeholder="Longitude" class="form-control longps" aria-describedby="hatcheryLocationHelpBlock" />
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary get-position" type="button"><i class="material-icons ilittle">my_location</i></button>
                                            </div>
                                        </div>
                                        <p class="small form-text text-muted" id="hatcheryLocationHelpBlock">
                                            Please enter coordinates GPS in Decimal Degrees.
                                            See this page : <a href="https://www.coordonnees-gps.fr/" target="_blank">Get GPS coordinates <i class="material-icons">open_in_new</i></a>.<br />
                                            Used to <a href="https://www.worldtides.info/" target="_blank">automatically detect the tide <i class="material-icons">open_in_new</i></a>.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add hatchery</button>
                        </div>
                    </form>
                </div>
            </div>';

return $return;
    }
}

