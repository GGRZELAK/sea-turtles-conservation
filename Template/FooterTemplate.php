<?php
namespace Template;

class FooterTemplate
{

    public function __construct()
    {
        
        ?>
        <footer>
        	<p>Made by <a href="https://communication.gaelmedias.fr/#informations"><img src="https://communication.gaelmedias.fr/Design/Gblanc.svg" alt="G" />aëlMedias</a></p>
        </footer>
        <?php
    }
}

