<?php

namespace Template;

use Framework\App;

class StationTemplate
{

    public $app;

    public function __construct()
    {
        $this->app = new App();
    }

    public function stationTools($users)
    {

        $return = '<div class="modal fade" id="addstation" tabindex="-1" role="dialog"
                 aria-labelledby="addstationModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form class="modal-content" method="post" action="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addstationModalLabel">
                                <i class="material-icons">add_location</i> New station
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="name">Name  <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <input name="name" id="name" type="text" class="form-control" required autofocus />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="address">Address</label>
                                        <div class="input-group">
                                            <input name="address" id="address" type="text" class="form-control" />
                                            <div class="input-group-append">
                                                <span class="input-group-text" ><i class="material-icons">location_city</i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="contact">Contact</label>
                                        <div class="input-group">
                                            <select name="contact" id="contact" class="custom-select">
                                                <option value="">Nobody</option>';
                                    foreach ($users as $user) {

                                        $return .= '<option value ="' . $user["id"] . '">' . $user["firstname"] . ' ' . $user["lastname"] . ' (' . $user["registration"] . ')</option>';
                                    }
                                    $return .= '</select>
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary" type="button" data-toggle="modal" data-id="#leader" data-target="#adduser">Add user</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label">GPS coordinates</label>
                                        <div class="input-group">
                                            <input name="latgps" type="text" placeholder="Latitude" class="latgps form-control"  aria-describedby="locationHelpBlock" />
                                            <input name="longps" type="text" placeholder="Longitude" class="longps form-control"  aria-describedby="locationHelpBlock" />
                                            <div class="input-group-append">
                                                <button class="btn btn-secondary get-position" type="button"><i class="material-icons ilittle">my_location</i></button>
                                            </div>
                                        </div>
                                        <p class="small form-text text-muted" id="locationHelpBlock">
                                            Please enter coordinates GPS in Decimal Degrees.
                                            See this page : <a href="https://www.coordonnees-gps.fr/" target="_blank">Get GPS coordinates <i class="material-icons">open_in_new</i></a>.<br />
                                            Used to <a href="https://www.worldtides.info/" target="_blank">automatically detect the tide <i class="material-icons">open_in_new</i></a>.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add station</button>
                        </div>
                    </form>
                </div>
            </div>';

        return $return;
    }
}

