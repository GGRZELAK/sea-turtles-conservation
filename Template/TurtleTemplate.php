<?php
namespace Template;

use Framework\App;

class TurtleTemplate
{
    
    public $app;
    
    public function __construct()
    {
        $this->app = new App();
    }
    
    public function speciesTools()
    {
        return <<<EOT
<div class="modal fade" id="addspecies" tabindex="-1" role="dialog"
                 aria-labelledby="addspeciesModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form class="modal-content" method="post" action="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addspeciesModalLabel">
                                <i class="material-icons">add_circle</i> New species
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="scientificname">Scientific name <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <input name="scientificname" id="scientificname" type="text" class="form-control" required autofocus />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="englishname">English name <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <input name="englishname" id="englishname" type="text" class="form-control" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add species</button>
                        </div>
                    </form>
                </div>
            </div>
EOT;
    }
}

