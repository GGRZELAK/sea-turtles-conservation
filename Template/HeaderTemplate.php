<?php
namespace Template;

use Framework\App;

class HeaderTemplate
{

    public $app;

    public function __construct()
    {
        $this->app = new App();
    }
    
    public function generateHtml($others = array())
    {
        echo '<header>';
        
        echo $this->logo();
        
        // echo $this->navigation();
        
        if (! empty($_SESSION["userId"])) {
            
            echo '<!-- Bouton Profil -->
            <p id="profil">
            <a role="button" data-toggle="popover-profil" data-placement="bottom" href="#">';
            
            if (! empty($_SESSION["picture"])) {
                
                echo '<img src="', $_SESSION["picture"], '" alt="Photo de profil ', $_SESSION["firstName"], ' ', $_SESSION["lastName"], '" />';
            } else {
                
                echo '<i class="material-icons">account_circle</i>';
            }
            
            echo '<span>', $_SESSION["firstName"], "<br />";
            if (!empty($_SESSION["lastName"])) {
                echo $_SESSION["lastName"];
            } else {
                echo '(', $_SESSION["registration"], ')';
            }
            echo '</span>
            </a>
            </p>';
        } else {
            
            echo '<!-- Bouton Login -->
            <p id="login">
            <a role="button" data-toggle="popover-login" data-placement="bottom" href="#" class="btn btn-primary">
            Sign in
            </a>
            </p>';
        }
        
        echo '</header>';
        
        // echo $this->navigationTools();
        
        echo '<div id="headerTools">';
        
        if (! empty($_SESSION["userId"])) {

            echo $this->profilTools($others);
        } else {
            
            echo $this->loginTools();
        }
        
        echo '</div>';
    }

    public function loginTools() {
        
$return = '<!-- Popover Login -->
<div class="container-popover-login">
    <div id="popover-login">
    	<form action="" method="post">
    		<h2>Sign in</h2>
    		<div class="input-group input-group-lg mb-3">
    			<div class="input-group-prepend">
    				<span class="input-group-text">
    				<i class="material-icons">person</i>
    				</span>
    			</div>
    			<input name="email" type="email" class="form-control" placeholder="Email" required autofocus>
    		</div>
    		<div class="input-group input-group-lg mb-3">
    			<div class="input-group-prepend">
    				<span class="input-group-text">
    					<i class="material-icons">lock</i>
    				</span>
    			</div>
    			<input name="password" type="password" class="form-control" placeholder="Password" required>
    		</div>
    		<p class="text-right">
    			<a class="btn btn-light btn-sm" href="#" data-toggle="modal" data-target="#forgotPassword">Forgot password ?</a>
    		</p>
    		<p class="text-center">
    			<button type="submit" class="btn btn-primary text-center">Sign in</button>
    		</p>
    		<div class="custom-control custom-checkbox text-center">
    			<input name="autologin" type="checkbox" class="custom-control-input" id="autologin">
    			<label class="custom-control-label" for="autologin" data-toggle="tooltip" data-placement="bottom"
    				title="For your security, check this box only on trusted devices.">
    				Remember me
    			</label>
    		</div>
        	<div class="dropdown-divider"></div>
        	<p class="text-right">
        		<a class="btn btn-light btn-sm" href="/' . $GLOBALS["language"] . '/register">Sign up</a>
        	</p>
    	</form>
    </div>
</div>

<!-- Modale Mot de passe oublié -->
<div class="modal fade" id="forgotPassword">
	<form class="modal-dialog" role="document" action="" method="post">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Forgot password</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="input-group input-group-lg mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text">
							<i class="material-icons">person</i>
						</span>
					</div>
					<input name="forgot" type="email" class="form-control" placeholder="Email" required>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Send my password</button>
			</div>
		</div>
	</form>
</div>';

        return $return;

    }

    public function profilTools($others = null)
    {
        $return = '<!-- Popover Profil -->
        <div style="display: none;">
            <div id="popover-profil">
            	<div class="my">
                	<p class="picture">';
                        if (!empty($_SESSION["picture"])) {
                		  $return .= '<img src="' . $_SESSION["picture"] . '" alt="Photo de profil ' . $_SESSION["firstName"] . ' ' . $_SESSION["lastName"] . '" />';
                        } else {
                            $return .= '<i style="color: white; font-size: 6em;" class="material-icons">person</i>';
                        }
                        $return .= '<!-- <span>Update</span> -->
                	</p>
                	<div class="informations">
                    	<p>
                        	<strong>' . $_SESSION["firstName"] . ' ';
                        $return .= (!empty($_SESSION["lastName"])) ? $_SESSION["lastName"] : '(' . $_SESSION["registration"] . ')';
                        $return .= '</strong>
                        	<br />
                        	<em>' . $_SESSION["email"] . '</em>
                    	</p>
                    	<p class="m-0">
                            <a data-toggle="modal" data-target="#updateProfil" href="#" class="btn btn-primary" title="Edit your account information">
                                Profil
                            </a>
                        </p>
                	</div>
            	</div>
            	<!--
            	<div class="others">
                	<p class="picture">
                		<img src="' . '" alt="Photo de profil ' . '" />
                	</p>
                	<p class="informations">
                    	Firstname LASTNAME<br />
                    	<em>mail@mail.fr</em>
                	</p>
            	</div>
            	-->
                <p class="buttons mb-0 text-center">
                	<button type="button" class="btn btn-secondary btn-sm" disabled>Add account</button>
                	<button type="button" class="btn btn-secondary btn-sm" id="logout">Sign out</button>
            	</p>
            </div>
        </div>
        
        <!-- Modale Modifier profil -->
        <div class="modal fade" id="updateProfil">
        	<form class="modal-dialog" role="document" action="" method="post">
        		<div class="modal-content">
        			<div class="modal-header">
        				<h5 class="modal-title">Edit profil</h5>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        					<span aria-hidden="true">&times;</span>
        				</button>
        			</div>
        			<div class="modal-body">
                		<div class="input-group input-group-lg mb-3">
                			<div class="input-group-prepend">
                				<span class="input-group-text">
                				<i class="material-icons">person</i>
                				</span>
                			</div>
                			<input name="email" type="email" class="form-control" placeholder="' . $_SESSION["email"] . '">
                		</div>
                		<div class="input-group input-group-lg mb-3">
                			<div class="input-group-prepend">
                				<span class="input-group-text">
                					<i class="material-icons">lock</i>
                				</span>
                			</div>
                			<input name="password" type="password" class="form-control" placeholder="Password">
                		</div>
        			</div>
        			<div class="modal-footer">
        				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        				<button type="submit" class="btn btn-primary">Update</button>
        			</div>
        		</div>
        	</form>
        </div>';
        
        return $return;
    }

    public function logo()
    {
        $conf = (file_exists("Framework/prod.ini")) ? parse_ini_file("Framework/prod.ini", true) : parse_ini_file("Framework/dev.ini", true);

        return '<!-- Logo -->
    <p id="logo">
        <a href="/"><img src="/' . $this->app->asset . $this->app->image . 'logo.jpg" alt="Logo" /></a>
        <span>' . $conf["APP"]["name"] . '</span>
    </p>';
    }

    public function navigation()
    {
        return '<!-- Bouton Navigation -->
    <p id="navigation">
        <a role="button" data-toggle="popover-navigation" data-placement="bottom" href="#"><i class="material-icons">apps</i></a>
    </p>';
    }

    public function navigationTools()
    {
        return '<!-- Popover Navigation -->
    <div style="display: none;">
        <div id="popover-navigation">
<!--
            <a href="#" class="grid">
                <img src="path/to/logo" alt="Logo navbar 1" />
                <span>Navbar 1</span>
            </a>
            <a href="#" class="grid">
                <img src="path/to/logo" alt="Logo navbar 2" />
                <span>Navbar 2</span>
            </a>
-->
        </div>
    </div>';
    }
}
