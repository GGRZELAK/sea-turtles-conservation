<?php
namespace Template;

class ModalTemplate
{
    public function generateModals($users, $stations) {

        $stationTemplate = new StationTemplate();
        $registerTemplate = new RegisterTemplate();
        $hatcheryTemplate = new HatcheryTemplate();
        $turtleTemplate = new TurtleTemplate();

        echo $stationTemplate->stationTools($users);
        echo $registerTemplate->registerTools();
        echo $hatcheryTemplate->hatcheryTools($stations);
        echo $turtleTemplate->speciesTools();
    }
}