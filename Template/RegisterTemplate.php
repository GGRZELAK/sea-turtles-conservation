<?php

namespace Template;


class RegisterTemplate
{

    public function registerTools()
    {
        return <<<EOT
<div class="modal fade" id="adduser" tabindex="-1" role="dialog"
                 aria-labelledby="adduserModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form class="modal-content" method="post" action="">
                        <div class="modal-header">
                            <h5 class="modal-title" id="adduserModalLabel">
                                <i class="material-icons">person_add</i> New user
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="firstName">Firstname  <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <input name="firstName" id="firstName" type="text" class="form-control" required autofocus />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="lastName">Lastname</label>
                                        <div class="input-group input-group-lg">
                                            <input name="lastName" id="lastName" type="text" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Email</label>
                                        <div class="input-group input-group-lg">
                                            <input name="email" id="email" type="email" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="password">Password</label>
                                        <div class="input-group input-group-lg">
                                            <input name="password" id="password" type="password" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="picture">Picture</label>
                                        <div class="input-group input-group-lg">
                                            <input name="picture" id="picture" type="text" placeholder="URL address of a picture" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <div class="form-group">
                                        <label class="control-label" for="language">Nationality <strong class="text-danger">*</strong></label>
                                        <div class="input-group input-group-lg">
                                            <select name="language" id="language" class="custom-select" required>
                                                <option value ="en">English</option>
                                                <option value ="es">Spanish</option>
                                                <option value ="fr">French</option>
                                                <option value ="de">German</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg">
                                    <div class="form-group">
                                        <label class="control-label" for="birthdate">Birthdate</label>
                                        <div class="input-group input-group-lg date datepicker" id="birthdate" data-target-input="nearest">
                                            <input name="birthdate" type="text" class="form-control datetimepicker-input" data-target="#birthdate" />
                                            <div class="input-group-append" data-target="#birthdate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="material-icons">date_range</i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm">
                                    <p class="form-group small form-text text-muted">
                                        We will never share your information with anyone.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add user</button>
                        </div>
                    </form>
                </div>
            </div>

EOT;

    }
}

