<?php
function decrypt($encrypted_string, $encryption_key) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
    return $decrypted_string;
}

if (!empty($_POST["hash"]) && !empty($_POST["key"])) {
    
    echo encrypt(strip_tags($_POST["hash"]), strip_tags($_POST["key"]));
    
} else {
    
    ?>
    <form method="post" action="">
        <p>
        	<input placeholder="Clé" type="text" name="key">
        	<input placeholder="Hash" type="text" name="hash">
        </p>
        <p>
        	<button type="submit">Crypter</button>
        </p>
    </form>
    <?php
}
