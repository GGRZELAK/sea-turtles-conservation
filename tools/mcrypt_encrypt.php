<?php
function encrypt($pure_string, $encryption_key) {
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
}

if (!empty($_POST["password"]) && !empty($_POST["key"])) {
    
    echo encrypt(strip_tags($_POST["password"]), strip_tags($_POST["key"]));
    
} else {
    
    ?>
    <form method="post" action="">
        <p>
        	<input placeholder="Clé" type="text" name="key">
        	<input placeholder="Mot de passe" type="password" name="password">
        </p>
        <p>
        	<button type="submit">Crypter</button>
        </p>
    </form>
    <?php
}
