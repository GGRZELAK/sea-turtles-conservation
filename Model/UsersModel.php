<?php
namespace Model;

use Framework\Model;
use PDO;

class UsersModel extends Model
{

    function searchUsers($data)
    {
        $sql = "SELECT * FROM users
            WHERE ";
        $keys = array_keys($data);
        for ($i = 0; $i < count($keys); $i ++) {
            $keys[$i] = $keys[$i] . " = :" . $keys[$i];
        }
        $sql .= implode(" AND ", $keys);
        $req = $this->executerRequete($sql, $data);
        $output = $req->fetchAll(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function getUsers()
    {
        $sql = "SELECT * FROM users ORDER BY registration DESC, firstname ASC";
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    function getUser($id)
    {
        $sql = "SELECT * FROM users WHERE id = ?";
        $req = $this->executerRequete($sql, array(
            $id
        ));
        $output = $req->fetchAll(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function updateUserEmail($data)
    {
        $sql = "UPDATE users SET email = :email WHERE id = :id";
        return $this->executerRequete($sql, $data);
    }

    function updateUserPassword($data)
    {
        $sql = "UPDATE users SET password = :password WHERE id = :id";
        return $this->executerRequete($sql, $data);
    }

    function getUserByFirstnameAndPassword($firstName, $password)
    {
        $sql = "SELECT * FROM users WHERE firstname = ? AND password = ?";
        $req = $this->executerRequete($sql, array(
            $firstName,
            $password
        ));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    function setUser($data)
    {
        $sql = "INSERT INTO users(email, password, firstname, lastname, birthdate, registration, language, picture)
        VALUES(:email, :password, :firstname, :lastname, :birthdate, :registration, :language, :picture)";
        return $this->executerRequete($sql, $data);
    }

    function getLeaders()
    {
        $sql = "SELECT * FROM leaders as l
        LEFT JOIN users as u ON u.id = l.userid
        LEFT JOIN stations as s ON s.id = l.stationid";
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
}

