<?php
namespace Model;

use Framework\Model;
use PDO;

class StationModel extends Model
{

    function getStation($id)
    {
        $sql = "SELECT * FROM stations WHERE id = ? ORDER BY date DESC";

        $req = $this->executerRequete($sql, array($id));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    function getStations()
    {
        $sql = "SELECT * FROM stations ORDER BY date DESC";

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    function setStation($data)
    {
        $sql = "INSERT INTO stations(name, address, contact, latgps, longps, comments)
        VALUES(:name, :address, :contact, :latgps, :longps, :comments)";
        return $this->executerRequete($sql, $data);
    }

    function getStationByName($name)
    {
        $sql = "SELECT * FROM stations WHERE name = ?";

        $req = $this->executerRequete($sql, array(
            $name
        ));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }
}

