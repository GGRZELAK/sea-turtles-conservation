<?php

namespace Model;

use Framework\Model;
use PDO;
use PDOStatement;

class TurtleModel extends Model
{
    /**
     * @param $arg
     * @return string
     */
    private function isNull($arg) {
        if (!empty($arg)) {
            return "= ?";
        }
        return "IS NULL";
    }
    /**
     * Get a species and every species if id is null
     *
     * @param string [$id]
     * @return array
     */
    function getSpecies($id = null)
    {
        $sql = "SELECT * FROM species";
        if ($id) {
            $sql .= " WHERE id = " . $id;
        }

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Get a specific species with these names
     *
     * @param string $scientificName
     * @param string $englishName
     * @return mixed
     */
    function getSpeciesByNames($scientificName, $englishName)
    {
        $sql = "SELECT * FROM species WHERE scientificname = ? AND englishname = ?";

        $req = $this->executerRequete($sql, array($scientificName, $englishName));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }

    /**
     * Set a new species
     *
     * @param array $data
     * @return PDOStatement
     */
    function setSpecies($data)
    {
        $sql = "INSERT INTO species(scientificname, englishname)
        VALUES(:scientificname, :englishname)";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Set a new turtle
     *
     * @param array $data
     * @return PDOStatement
     */
    function setTurtle($data)
    {
        $sql = "INSERT INTO turtles(id, speciesid, length, width, lefttag, righttag, lefthurt, righthurt, referencingdate, motherid, fatherid, birthdate, manager, comments)
        VALUES(:id, :speciesid, :length, :width, :lefttag, :righttag, :lefthurt, :righthurt, :referencingdate, :motherid, :fatherid, :birthdate, :manager, :comments)";
        return $this->executerRequete($sql, $data);
    }

    /**
     * Get every registered turtles
     *
     * @param null $limit
     * @return array
     */
    function getTurtles($limit = null)
    {
        $sql = "SELECT t.id, speciesid, length, width, lefttag, righttag, lefthurt, righthurt, referencingdate, motherid, fatherid, birthdate, manager, comments,
                scientificname, englishname, s.date as referencingspeciesdate
                FROM turtles as t
                LEFT JOIN species as s ON t.speciesid = s.id
                ORDER BY referencingspeciesdate DESC";

        if ($limit) {
            $sql .= " LIMIT " . $limit;
        }

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    function getLastTurtles($limit = 10)
    {
        $sql = "SELECT t.id, speciesid, length, width, lefttag, righttag, lefthurt, righthurt, referencingdate, motherid, fatherid, birthdate, manager, comments,
                scientificname, englishname, s.date as referencingspeciesdate
                FROM turtles as t
                LEFT JOIN species as s ON t.speciesid = s.id
                WHERE lefttag IS NOT NULL OR righttag IS NOT NULL
                ORDER BY referencingdate DESC";

        if ($limit) {
            $sql .= " LIMIT " . $limit;
        }

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Get a turtle with her uniqid
     *
     * @param string $id
     * @return mixed
     */
    function getTurtle($id)
    {
        $sql = "SELECT t.id, speciesid, length, width, lefttag, righttag, lefthurt, righthurt, referencingdate, motherid, fatherid, birthdate, manager, comments,
                scientificname, englishname, s.date as referencingspeciesdate
                FROM turtles as t
                LEFT JOIN species as s ON t.speciesid = s.id
                WHERE t.id = ?";

        $req = $this->executerRequete($sql, array($id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Get a turtle with her uniqid
     *
     * @param int $speciesId
     * @param string $leftTag
     * @param string $rightTag
     * @return mixed
     */
    function getTurtleByTag($speciesId, $leftTag, $rightTag)
    {

        $sql = "SELECT * FROM turtles WHERE speciesid ";

        $sql .= $this->isNull($speciesId);

        $sql .= " AND lefttag ";

        $sql .= $this->isNull($leftTag);

        $sql .= " AND righttag ";

        $sql .= $this->isNull($rightTag);

        $data = array();

        foreach (func_get_args() as $arg) {
            if (!empty($arg)) {
                array_push($data, $arg);
            }
        }

        $req = $this->executerRequete($sql, $data);
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * Update a turtle
     *
     * @param array $data
     * @return int
     */
    function updTurtle($data)
    {
        $sql = "UPDATE turtles SET
                speciesid = :speciesid,
                length = :length,
                width = :width,
                lefttag = :lefttag,
                righttag = :righttag,
                lefthurt = :lefthurt,
                righthurt = :righthurt,
                referencingdate = :referencingdate,
                motherid = :motherid,
                fatherid = :fatherid,
                birthdate = :birthdate,
                manager = :manager,
                comments = :comments
                WHERE id = :id";

        return $this->executerRequete($sql, $data)->rowCount();
    }

    /**
     * Delete a turtle
     *
     * @param array $id
     * @return number
     */
    function delTurtle($id)
    {
        $sql = "DELETE FROM turtles WHERE id = ?";
        return $this->executerRequete($sql, array($id))->rowCount();
    }
}

