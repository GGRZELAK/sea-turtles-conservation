<?php
namespace Model;

use Framework\Model;
use PDO;

class AuthenticateModel extends Model
{

    function getSession($userId)
    {
        $sql = "SELECT * FROM sessions WHERE userid = ?";
        $req = $this->executerRequete($sql, array(
            $userId
        ));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    function setSession($id, $userId)
    {
        $sql = "INSERT INTO sessions(id, userid) VALUES(?, ?)";
        return $this->executerRequete($sql, array(
            $id,
            $userId
        ));
    }

    /**
     * Supprime la référence des sessions qui ne sont plus disponibles
     * 
     * @param int $second Durée de vie des sessions
     * @return int Nombre de sessions supprimées
     */
    function deleteSessions($second)
    {
        $sql = "DELETE FROM sessions WHERE lastaccess < (NOW() - INTERVAL " . $second . " SECOND)";
        $req = $this->executerRequete($sql);
        return $req->rowCount();
    }

    function deleteSession($id)
    {
        $sql = "DELETE FROM sessions WHERE id = ?";
        $req = $this->executerRequete($sql, array(
            $id
        ));
        return $req->rowCount();
    }

    function setAutologin($userId, $deviceId)
    {
        $sql = 'UPDATE devices SET autologin = ? WHERE hash = ?';
        $req = $this->executerRequete($sql, array(
            $userId,
            $deviceId
        ));
        return $req->rowCount();
    }

    function getAutologin($deviceId)
    {
        $sql = "SELECT *
            FROM devices LEFT JOIN users
            ON devices.autologin = users.id
            WHERE devices.id = ?";
        $req = $this->executerRequete($sql, array(
            $deviceId
        ));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    function deleteAutologin($deviceHash)
    {
        $sql = "UPDATE devices SET autologin = NULL WHERE hash = ?";
        $req = $this->executerRequete($sql, array(
            $deviceHash
        ));
        return $req->rowCount();
    }
}

