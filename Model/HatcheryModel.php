<?php
namespace Model;

use Framework\Model;
use PDO;
use PDOStatement;

class HatcheryModel extends Model
{
    /**
     * Set an hatchery
     * 
     * @param array $data
     * @return PDOStatement
     */
    function setHatchery($data)
    {
        $sql = "INSERT INTO hatcheries(stationid, name, width, height, numberoflocations, latgps, longps, comments)
        VALUES(:stationid, :name, :width, :height, :numberoflocations, :latgps, :longps, :comments)";
        return $this->executerRequete($sql, $data);
    }
    
    /**
     * Get hatcheries of a station
     * 
     * @param number $stationId
     * @return array
     */
    function getHatcheries($stationId = 1)
    {
        $sql = "SELECT * FROM hatcheries WHERE stationid = ? ORDER BY date DESC";
        
        $req = $this->executerRequete($sql, array($stationId));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Get hatcheries of every stations
     * 
     * @return array
     */
    function getAllHatcheries()
    {
        $sql = "SELECT * FROM hatcheries ORDER BY date DESC";
        
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Get an hatchery with his name
     * 
     * @param string $name
     * @param int $stationId
     * @return mixed
     */
    function getHatcheryByName($name, $stationId)
    {
        $sql = "SELECT * FROM hatcheries WHERE name = ? AND stationid = ? ORDER BY date DESC";
        
        $req = $this->executerRequete($sql, array($name, $stationId));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Set a nest
     * 
     * @param array $data
     * @return PDOStatement
     */
    function setNest($data)
    {
        $sql = "INSERT INTO nests(code, hatcheryid, builder, hatcheryleader, buildingdate, eggs)
        VALUES(:code, :hatcheryid, :builder, :hatcheryleader, :buildingdate, :eggs)";
        return $this->executerRequete($sql, $data);
    }
    
    /**
     * Get the last created nest
     * 
     * @param int $hatcheryid
     * @return mixed
     */
    function getLastNest($hatcheryid)
    {
        $sql = "SELECT * FROM nests WHERE hatcheryid = ? ORDER BY buildingdate DESC LIMIT 1";
        
        $req = $this->executerRequete($sql, array($hatcheryid));
        $output = $req->fetch(PDO::FETCH_NAMED);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Get nests of every hatcheries
     * 
     * @return array
     */
    function getAllNests()
    {
        $sql = "SELECT * FROM nests ORDER BY buildingdate DESC";
        
        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Get a nest by id
     * 
     * @param int $id
     * @return mixed
     */
    function getNest($id)
    {
        $sql = "SELECT * FROM nests WHERE id = ?";
        
        $req = $this->executerRequete($sql, array($id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
    
    /**
     * Update a nest
     * 
     * @param array $data
     * @return int Number of updated row
     */
    function updNest($data)
    {
        $sql = "UPDATE nests SET
                code = :code,
                hatcheryid = :hatcheryid,
                builder = :builder,
                hatcheryleader = :hatcheryleader,
                buildingdate = :buildingdate,
                eggs = :eggs
                WHERE id = :id";
        
        return $this->executerRequete($sql, $data)->rowCount();
    }
    
    /**
     * Delete a nest
     * 
     * @param int $id
     * @return int Number of deleted row
     */
    function delNest($id)
    {
        $sql = "DELETE FROM nests WHERE id = ?";
        return $this->executerRequete($sql, array($id))->rowCount();
    }
}

