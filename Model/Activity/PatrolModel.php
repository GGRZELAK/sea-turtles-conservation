<?php

namespace Model\Activity;

use Framework\Model;
use PDO;
use PDOStatement;

class PatrolModel extends Model
{

    /**
     * @param $data
     * @return PDOStatement
     */
    public function setPatrol($data)
    {
        $sql = "INSERT INTO patrolactivities(stationid, turtleid, date, startlaying, endlaying, oldtide, tide, leader, assistant, beachlocation, beachplace, step, widthreturntrace, fire, residues, laid, destination, hatcheryid, nestid, comments)
            VALUES(:stationid, :turtleid, :date, :startlaying, :endlaying, :oldtide, :tide, :leader, :assistant, :beachlocation, :beachplace, :step, :widthreturntrace, :fire, :residues, :laid, :destination, :hatcheryid, :nestid, :comments)";
        return $this->executerRequete($sql, $data);
    }

    /**
     * @param $year
     * @param null $limit
     * @return array
     */
    public function getPatrols($year, $limit = null)
    {
        $sql = "SELECT * FROM patrolactivities WHERE YEAR(date) = ? ORDER BY date DESC";

        if ($limit) {
            $sql .= " LIMIT " . $limit;
        }

        $req = $this->executerRequete($sql, array($year));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getLastPatrols($limit = 10)
    {
        $sql = "SELECT * FROM patrolactivities
        WHERE nestid IS NOT NULL
        ORDER BY date DESC";

        if ($limit) {
            $sql .= " LIMIT " . $limit;
        }

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @param $stationid
     * @param $date
     * @return array
     */
    public function getPatrolActivityByDate($stationid, $date)
    {
        $sql = "SELECT * FROM patrolactivities WHERE stationid = ? AND date = ?";

        $req = $this->executerRequete($sql, array($stationid, $date));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @return array
     */
    public function getSeasons()
    {
        $sql = "SELECT count(id) as count, YEAR(date) as year FROM patrolactivities GROUP BY YEAR(date) ORDER BY YEAR(date)";

        $req = $this->executerRequete($sql);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @param $id
     * @return array
     */
    public function getPatrolsByTurtle($id)
    {
        $sql = "SELECT * FROM patrolactivities WHERE turtleid = ? ORDER BY date DESC";

        $req = $this->executerRequete($sql, array($id));
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPatrol($id)
    {
        $sql = "SELECT * FROM patrolactivities WHERE id = ?";

        $req = $this->executerRequete($sql, array($id));
        $output = $req->fetch(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }

    /**
     * @param $data
     * @return int
     */
    public function updPatrol($data)
    {
        $sql = "UPDATE patrolactivities SET
                stationid = :stationid,
                turtleid = :turtleid,
                date = :date,
                startlaying = :startlaying,
                endlaying = :endlaying,
                oldtide = :oldtide,
                tide = :tide,
                leader = :leader,
                assistant = :assistant,
                beachlocation = :beachlocation,
                beachplace = :beachplace,
                step = :step,
                widthreturntrace = :widthreturntrace,
                fire = :fire,
                residues = :residues,
                laid = :laid,
                destination = :destination,
                hatcheryid = :hatcheryid,
                nestid = :nestid,
                comments = :comments
                WHERE id = :id";

        return $this->executerRequete($sql, $data)->rowCount();
    }

    /**
     * @param null $years
     * @return array
     */
    public function getPatrolsByYear($years = null)
    {
        if (!$years) {
            $years = array(date('Y'));
        }

        $sql = "SELECT * FROM patrolactivities WHERE YEAR(date) = ?";

        while (next($years) !== false) {
            $sql .= " OR YEAR(date) = ?";
        }

        $req = $this->executerRequete($sql, $years);
        $output = $req->fetchAll(PDO::FETCH_ASSOC);
        $req->closeCursor();
        return $output;
    }
}

